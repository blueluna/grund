#include <grund/file_descriptor.hpp>
#include <grund/memory_map.hpp>
#include <vector>

#ifndef PLOGIK_UIO_DEVICE_HPP
#define PLOGIK_UIO_DEVICE_HPP

namespace plogik {

class UIODescription;

/// UIO mapped region description
class UIOMapDescription {
public:
  /// Create a invalid UIO map description
  explicit UIOMapDescription();
  /// Create a UIO map description
  ///
  /// \param uio UIO device number
  /// \param map UIO device map number
  explicit UIOMapDescription(uint16_t uio, uint16_t map);
  /// Create a UIO map description
  ///
  /// \param name UIO device name
  /// \param map UIO device map number
  explicit UIOMapDescription(const std::string& name, uint16_t map);
  /// Create a UIO map description
  ///
  /// \param description UIO device description
  /// \param map UIO device map number
  explicit UIOMapDescription(const UIODescription& description, uint16_t map);

  /// Get the UIO device number
  uint16_t uio() const { return _uio_number; };
  /// Geth the UIO device map number
  uint16_t map() const { return _map_number; };
  /// Get the physical address of the UIO device map
  size_t address() const { return _address; }
  /// Get the size of the UIO device map
  size_t size() const { return _size; }
  /// Get the offset of the UIO device map
  size_t offset() const { return _offset; }
  /// Get the UIO map name
  std::string name() const;

  /// Memory map the UIO memory region
  ///
  /// \return MemoryMap of the memory region
  grund::MemoryMap to_memory_map() const;

protected:
  uint16_t _uio_number;
  uint16_t _map_number;
  size_t _address;
  size_t _size;
  size_t _offset;
};

/// UIO device description
class UIODescription {
public:
  /// Invalid device identifier
  static const uint16_t INVALID = std::numeric_limits<uint16_t>::max();
  /// Create a invalid UIO device description
  explicit UIODescription();
  /// Create a UIO device description
  ///
  /// \param uio UIO device number
  explicit UIODescription(uint16_t uio);
  /// Create a UIO device description
  ///
  /// \param name UIO device name
  explicit UIODescription(const std::string& name);

  /// Get the UIO device number
  uint16_t uio() const { return _uio_number; };
  /// Get the UIO device name
  std::string name() const;
  /// Get the maps associated with the UIO device
  ///
  /// \return Vector of UIO map descriptions
  std::vector<UIOMapDescription> maps() const;
  /// Enumerate all UIO devices
  ///
  /// \return Vector of UIO device descriptions
  static std::vector<UIODescription> enumerate();

protected:
  uint16_t _uio_number;
};

class UIODeviceFile : public grund::FileDescriptor {
public:
  explicit UIODeviceFile(uint16_t uio);

  /// Arm interrupt for this UIO device
  void interrupt_arm();

  /// Read the interrupt counter
  ///
  /// \return Read interrupt counter or none if the read call would block
  std::optional<uint32_t> interrupt_counter();

  /// Wait for the interrupt
  ///
  /// \return Read interrupt counter
  uint32_t interrupt_wait();
};

}

#endif // PLOGIK_UIO_DEVICE_HPP
