#include <grund/memory_map.hpp>

#ifndef PLOGIK_ZDMA_HPP
#define PLOGIK_ZDMA_HPP

namespace plogik {

class Zdma {
public:
  /// ZDMA block state
  enum class State : uint32_t {
    /// Transaction is done, the block is idle
    Done = 0,
    /// Transaction is done, the block is paused
    Paused = 1,
    /// Transaction underway
    Busy = 2,
    /// Transaction is done, there where errors
    DoneError = 3
  };

  /// ZDMA operation mode
  enum class Mode : uint32_t {
    /// Normal mode, read from source then write to destination
    Normal = 0,
    /// Read only mode, read from source and discard the data
    ReadOnly = 1,
    /// Write only mode, write register value (REG_WR_ONLY_WORD) to destination. 128-bit for GDMA, 64-bit for ADMA
    WriteOnly = 2,
  };

  /// Enable FPD DMA clock
  ///
  /// Use /dev/mem to enable clock for FPD DMA (GDMA)
  /// Might be required for GDMA to function.
  ///
  /// Throws std::exception on error
  static void activate_fpd_dma_clk();

  /// Create Zdma instance from memory mapped region
  Zdma(grund::MemoryMap&& mm);

  void print();

  /// Reset Zdma block
  void reset();

  /// Get the current state of the Zdma block
  ///
  /// \return The current state
  State get_state();

  /// Not used yet
  void set_mode(const Mode mode);

  /// Start a simple DMA transfer
  ///
  /// \param source Physical address to the location to read from
  /// \param destination Physical address to the location to write to
  /// \param length Number of bytes to transfer
  /// \return 0 on done, EIO on error, EAGAIN on busy
  int start_simple_transfer(size_t source, size_t destination, size_t length);

  /// Get the transfer status
  ///
  /// \return 0 on done, EIO on error, EAGAIN on busy
  int transfer_status();

  /// Get the interrupt flags
  ///
  /// \return The raw interrupt register
  uint32_t get_interrupts();

protected:
  grund::MemoryMap _mm;

  struct LinearDescription {
    uint64_t Address;
    uint32_t Length;
    uint32_t Control;
  };

  /// Enable or disable the DMA block
  void enable_dma(bool enable);

  /// Check if the block is ready for a new transfer
  ///
  /// \return 0 on done, EIO on error, EAGAIN on busy
  int check_ready();

  static constexpr uint32_t DESC_CTR_COHERENT { 0x0000000001 }; /// Coherent transfer for ADMA
  static constexpr uint32_t DESC_CTR_DESC_TYPE { 0x0000000002 }; /// 0 = Linear, 1 = Linked list
  static constexpr uint32_t DESC_CTR_INT { 0x0000000004 }; /// Interrupt on completion

  static constexpr uint32_t INT_INV_APB { 1 << 0 }; /// Interrupt,
  static constexpr uint32_t INT_SRC_DONE { 1 << 1 }; /// Interrupt, source operation done
  static constexpr uint32_t INT_DST_DONE { 1 << 2 }; /// Interrupt, destination operation done
  static constexpr uint32_t INT_COUNTER_OVERFLOW { 1 << 3 }; /// Interrupt, byte counter overflow
  static constexpr uint32_t INT_SRC_ACCOUNT_COUNTER_ERROR { 1 << 4 }; /// Interrupt, source account counter error
  static constexpr uint32_t INT_DST_ACCOUNT_COUNTER_ERROR { 1 << 5 }; /// Interrupt, destination account counter error
  static constexpr uint32_t INT_AXI_ERROR_SRC_DESC { 1 << 6 }; /// Interrupt, AXI source descriptor error
  static constexpr uint32_t INT_AXI_ERROR_DST_DESC { 1 << 7 }; /// Interrupt, AXI destination descriptor error
  static constexpr uint32_t INT_AXI_ERROR_RD_DATA { 1 << 8 }; /// Interrupt, AXI read data error
  static constexpr uint32_t INT_AXI_ERROR_WR_DATA { 1 << 9 }; /// Interrupt, AXI write data error
  static constexpr uint32_t INT_DONE { 1 << 10 }; /// Interrupt, operation done
  static constexpr uint32_t INT_PAUSE { 1 << 11 }; /// Interrupt, operation paused
  static constexpr uint32_t INT_MASK { 0x00000fff }; /// Interrupt mask
  /// Interrupts indicating errors
  static constexpr uint32_t INT_ERROR { INT_INV_APB | INT_AXI_ERROR_SRC_DESC | INT_AXI_ERROR_DST_DESC | INT_AXI_ERROR_RD_DATA | INT_AXI_ERROR_WR_DATA };

  static constexpr size_t REG_ERR_CTRL { 0x0000000000 }; /// Enable/Disable a error response
  static constexpr size_t REG_ISR { 0x0000000100 }; /// Interrupt Status Register for intrN. This is a sticky register that holds the value of the interrupt until cleared by a value of 1.
  static constexpr size_t REG_IMR { 0x0000000104 }; /// Interrupt Mask Register for intrN. This is a read-only location and can be atomically altered by either the IDR or the IER.
  static constexpr size_t REG_IEN { 0x0000000108 }; /// Interrupt Enable Register. A write of to this location will unmask the interrupt. (IMR: 0)
  static constexpr size_t REG_IDS { 0x000000010C }; /// Interrupt Disable Register. A write of one to this location will mask the interrupt. (IMR: 1)
  static constexpr size_t REG_CTRL0 { 0x0000000110 }; /// Channel Control Register 0
  static constexpr size_t REG_CTRL1 { 0x0000000114 }; /// Channel Control Register 1
  static constexpr size_t REG_FCI { 0x0000000118 }; /// Channel Flow Control Register
  static constexpr size_t REG_STATUS { 0x000000011C }; /// Channel Status Register
  static constexpr size_t REG_DATA_ATTR { 0x0000000120 }; /// Channel DATA AXI parameter Register
  static constexpr size_t REG_DSCR_ATTR { 0x0000000124 }; /// Channel DSCR AXI parameter Register
  static constexpr size_t REG_SRC_DSCR_WORD0 { 0x0000000128 }; /// SRC DSCR Word 0
  static constexpr size_t REG_SRC_DSCR_WORD1 { 0x000000012C }; /// SRC DSCR Word 1
  static constexpr size_t REG_SRC_DSCR_WORD2 { 0x0000000130 }; /// SRC DSCR Word 2
  static constexpr size_t REG_SRC_DSCR_WORD3 { 0x0000000134 }; /// SRC DSCR Word 3
  static constexpr size_t REG_DST_DSCR_WORD0 { 0x0000000138 }; /// DST DSCR Word 0
  static constexpr size_t REG_DST_DSCR_WORD1 { 0x000000013C }; /// DST DSCR Word 1
  static constexpr size_t REG_DST_DSCR_WORD2 { 0x0000000140 }; /// DST DSCR Word 2
  static constexpr size_t REG_DST_DSCR_WORD3 { 0x0000000144 }; /// DST DSCR Word 3
  static constexpr size_t REG_WR_ONLY_WORD0 { 0x0000000148 }; /// Write Only Data Word 0
  static constexpr size_t REG_WR_ONLY_WORD1 { 0x000000014C }; /// Write Only Data Word 1
  static constexpr size_t REG_WR_ONLY_WORD2 { 0x0000000150 }; /// Write Only Data Word 2
  static constexpr size_t REG_WR_ONLY_WORD3 { 0x0000000154 }; /// Write Only Data Word 3
  static constexpr size_t REG_SRC_START_LSB { 0x0000000158 }; /// SRC DSCR Start Address LSB Register
  static constexpr size_t REG_SRC_START_MSB { 0x000000015C }; /// SRC DSCR Start Address MSB Register
  static constexpr size_t REG_DST_START_LSB { 0x0000000160 }; /// DST DSCR Start Address LSB Register
  static constexpr size_t REG_DST_START_MSB { 0x0000000164 }; /// DST DSCR Start Address MSB Register
  static constexpr size_t REG_TOTAL_BYTE { 0x0000000188 }; /// Total Bytes Transferred Register
  static constexpr size_t REG_RATE_CTRL { 0x000000018C }; /// Rate Control Count Register
  static constexpr size_t REG_IRQ_SRC_ACCT { 0x0000000190 }; /// SRC Interrupt Account Count Register
  static constexpr size_t REG_IRQ_DST_ACCT { 0x0000000194 }; /// DST Interrupt Account Count Register
  static constexpr size_t REG_CTRL2 { 0x0000000200 }; /// zDMA Control Register 2

public:
  enum class Instance {
    ADMA0,
    ADMA1,
    ADMA2,
    ADMA3,
    ADMA4,
    ADMA5,
    ADMA6,
    ADMA7,
    GDMA0,
    GDMA1,
    GDMA2,
    GDMA3,
    GDMA4,
    GDMA5,
    GDMA6,
    GDMA7,
  };

protected:
  static constexpr size_t base_address(const Instance instance);

  static constexpr size_t ADMA0_BASE_ADDRESS { 0x00FFA80000 }; // (ADMA_CH0)
  static constexpr size_t ADMA1_BASE_ADDRESS { 0x00FFA90000 }; // (ADMA_CH1)
  static constexpr size_t ADMA2_BASE_ADDRESS { 0x00FFAA0000 }; // (ADMA_CH2)
  static constexpr size_t ADMA3_BASE_ADDRESS { 0x00FFAB0000 }; // (ADMA_CH3)
  static constexpr size_t ADMA4_BASE_ADDRESS { 0x00FFAC0000 }; // (ADMA_CH4)
  static constexpr size_t ADMA5_BASE_ADDRESS { 0x00FFAD0000 }; // (ADMA_CH5)
  static constexpr size_t ADMA6_BASE_ADDRESS { 0x00FFAE0000 }; // (ADMA_CH6)
  static constexpr size_t ADMA7_BASE_ADDRESS { 0x00FFAF0000 }; // (ADMA_CH7)
  static constexpr size_t GDMA0_BASE_ADDRESS { 0x00FD500000 }; // (GDMA_CH0)
  static constexpr size_t GDMA1_BASE_ADDRESS { 0x00FD510000 }; // (GDMA_CH1)
  static constexpr size_t GDMA2_BASE_ADDRESS { 0x00FD520000 }; // (GDMA_CH2)
  static constexpr size_t GDMA3_BASE_ADDRESS { 0x00FD530000 }; // (GDMA_CH3)
  static constexpr size_t GDMA4_BASE_ADDRESS { 0x00FD540000 }; // (GDMA_CH4)
  static constexpr size_t GDMA5_BASE_ADDRESS { 0x00FD550000 }; // (GDMA_CH5)
  static constexpr size_t GDMA6_BASE_ADDRESS { 0x00FD560000 }; // (GDMA_CH6)
  static constexpr size_t GDMA7_BASE_ADDRESS { 0x00FD570000 }; // (GDMA_CH7)
};

}

#endif // PLOGIK_ZDMA_HPP
