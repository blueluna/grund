#include <plogik/uio_device.hpp>
#include <span>

#ifndef PLOGIK_AXI_FIFO_UIO_HPP
#define PLOGIK_AXI_FIFO_UIO_HPP

namespace plogik {

/// AXI Stream FIFO memory mapped interface
class AxiStreamFifoMm {
public:
  /// Create AxiStreamFifoMm from UIO device
  AxiStreamFifoMm(const UIODescription& description, int data_width = 32);
  /// Create AxiStreamFifoMm from physical address
  AxiStreamFifoMm(const size_t axi4_lite_physical_address);
  /// Create AxiStreamFifoMm from physical address
  AxiStreamFifoMm(const size_t axi4_lite_physical_address, const size_t axi4_physical_address, int data_width = 32);

  /// Reset the FIFO
  void reset();

  size_t write(uint8_t destination, const std::span<uint32_t>& values);
  size_t write(uint8_t destination, const std::span<uint64_t>& values);
  size_t write(const std::span<uint32_t>& values);
  size_t read(uint8_t& destination, std::span<uint32_t>& values);
  size_t read(uint8_t& destination, std::span<uint64_t>& values);
  size_t read(std::span<uint32_t>& values);

  size_t read_ready();

  /// Enable the read complete interrupt
  void read_interrupt_enable();

protected:
  grund::MemoryMap _mmap;
  grund::MemoryMap _mmap_full;
  size_t _data_width;

  void interrupts_clear();
  void interrupts_clear_tx();
  void interrupts_clear_rx();

  void write_setup(uint8_t destination);
  void write_finish(uint32_t bytes);
  size_t write(uint8_t destination, const uint32_t* words, size_t words_length);

  void read_full(uint32_t* data, size_t length);
  size_t read(uint8_t& destination, uint32_t* words, size_t words_length);

  void write_lite(const uint32_t* data, size_t length);
  void write_full(const uint32_t* data, size_t length);

  static constexpr uint32_t RESET_MAGIC = 0x000000A5;

  static constexpr uint32_t REG_INT_STATUS = 0x00;
  static constexpr uint32_t REG_INT_ENABLE = 0x04;
  static constexpr uint32_t REG_TX_RESET = 0x08;
  static constexpr uint32_t REG_TX_VACANCY = 0x0c;
  static constexpr uint32_t REG_TX_DATA = 0x10;
  static constexpr uint32_t REG_TX_LENGTH = 0x14;
  static constexpr uint32_t REG_RX_RESET = 0x18;
  static constexpr uint32_t REG_RX_OCCUPANCY = 0x1c;
  static constexpr uint32_t REG_RX_DATA = 0x20;
  static constexpr uint32_t REG_RX_LENGTH = 0x24;
  static constexpr uint32_t REG_AXI4_STREAM_RESET = 0x28;
  static constexpr uint32_t REG_TRANSMIT_DST = 0x2c;
  static constexpr uint32_t REG_RECEIVE_DST = 0x30;

  static constexpr uint32_t FULL_REG_WRITE = 0x00000000;
  static constexpr uint32_t FULL_REG_READ = 0x00001000;

  static constexpr uint32_t INT_RX_URE = 0x80000000; /* Receive under-read */
  static constexpr uint32_t INT_RX_ORE = 0x40000000; /* Receive over-read */
  static constexpr uint32_t INT_RX_UR = 0x20000000; /* Receive under run (empty) */
  static constexpr uint32_t INT_TX_ORE = 0x10000000; /* Transmit overrun */
  static constexpr uint32_t INT_TXC = 0x08000000; /* Transmit complete */
  static constexpr uint32_t INT_RXC = 0x04000000; /* Receive complete */
  static constexpr uint32_t INT_TXLE = 0x02000000; /* Transmit length mismatch */
  static constexpr uint32_t INT_TXRC = 0x01000000; /* Transmit reset complete */
  static constexpr uint32_t INT_RXRC = 0x00800000; /* Receive reset complete */
  static constexpr uint32_t INT_TXPF = 0x00400000; /* Tx FIFO Programmable Full */
  static constexpr uint32_t INT_TXPE = 0x00200000; /* Tx FIFO Programmable Empty */
  static constexpr uint32_t INT_RXPF = 0x00100000; /* Rx FIFO Programmable Full */
  static constexpr uint32_t INT_RXPE = 0x00080000; /* Rx FIFO Programmable Empty */
  static constexpr uint32_t INT_ALL = 0xfff80000; /* All the ints */
  static constexpr uint32_t INT_ERROR = 0xf2000000; /* Error status ints */
  static constexpr uint32_t INT_RXERROR = 0xe0000000; /* Receive Error status ints */
  static constexpr uint32_t INT_TXERROR = 0x12000000; /* Transmit Error status ints */
};

}

#endif // PLOGIK_AXI_FIFO_UIO_HPP
