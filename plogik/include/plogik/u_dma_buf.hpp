#include <grund/file_descriptor.hpp>
#include <grund/memory_map.hpp>
#include <string>
#include <sys/types.h>

#ifndef PLOGIK_U_DMA_BUF_HPP
#define PLOGIK_U_DMA_BUF_HPP

namespace plogik {

class UDmaBuf {
public:
  explicit UDmaBuf(const std::string& name);

  const std::string& name() const { return _name; }
  size_t address() const { return _address; }
  size_t size() const { return _size; }

  grund::MemoryMap to_memory_map(bool sync) const;

protected:
  std::string _name;
  size_t _address;
  size_t _size;
};

class UDmaBufManager {
public:
  static void add_buffer(const std::string name, size_t size);
  static void remove_buffer(const std::string name);

protected:
  static grund::FileDescriptor open_manager();

  static constexpr std::string_view DEVICE_FILE_PATH { "/dev/u-dma-buf-mgr" };
};

}

#endif // PLOGIK_U_DMA_BUF_HPP
