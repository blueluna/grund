#include <cstdint>
#include <grund/file_descriptor.hpp>
#include <span>
#include <string>

#ifndef PLOGIK_AXI_FIFO_KERNEL_HPP
#define PLOGIK_AXI_FIFO_KERNEL_HPP

namespace plogik {

class AxiFifoKernel {
public:
  AxiFifoKernel();
  AxiFifoKernel(const std::filesystem::path& path, const grund::FileDescriptor::OpenMode direction);

  void reset();

  size_t write(uint8_t destination, const std::span<uint32_t>& values);
  size_t write(const std::span<uint32_t>& values);
  size_t read(uint8_t& destination, std::span<uint32_t>& values);
  size_t read(std::span<uint32_t>& values);

  size_t read_ready();

protected:
  grund::FileDescriptor _fd;
};

}

#endif // PLOGIK_AXI_FIFO_KERNEL_HPP
