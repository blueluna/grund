#include <fcntl.h>
#include <plogik/axi_fifo_kernel.hpp>
#include <sys/ioctl.h>

#define AXIS_FIFO_IOCTL_MAGIC 'Q'
#define AXIS_FIFO_NUM_IOCTLS 14

#define AXIS_FIFO_GET_REG _IOR(AXIS_FIFO_IOCTL_MAGIC, 0, struct axis_fifo_kern_regInfo)
#define AXIS_FIFO_SET_REG _IOW(AXIS_FIFO_IOCTL_MAGIC, 1, struct axis_fifo_kern_regInfo)
#define AXIS_FIFO_GET_TX_MAX_PKT _IOR(AXIS_FIFO_IOCTL_MAGIC, 2, uint32_t)
#define AXIS_FIFO_SET_TX_MAX_PKT _IOW(AXIS_FIFO_IOCTL_MAGIC, 3, uint32_t)
#define AXIS_FIFO_GET_RX_MIN_PKT _IOR(AXIS_FIFO_IOCTL_MAGIC, 4, uint32_t)
#define AXIS_FIFO_SET_RX_MIN_PKT _IOW(AXIS_FIFO_IOCTL_MAGIC, 5, uint32_t)
#define AXIS_FIFO_RESET_IP _IO(AXIS_FIFO_IOCTL_MAGIC, 6)
#define AXIS_FIFO_GET_FPGA_ADDR _IOR(AXIS_FIFO_IOCTL_MAGIC, 7, uint32_t)
#define AXIS_FIFO_GET_TX_VACANCY _IOR(AXIS_FIFO_IOCTL_MAGIC, 8, uint32_t)
#define AXIS_FIFO_GET_RX_OCCUPANCY _IOR(AXIS_FIFO_IOCTL_MAGIC, 9, uint32_t)
#define AXIS_FIFO_GET_TX_PKTS_SENT _IOR(AXIS_FIFO_IOCTL_MAGIC, 10, uint32_t)
#define AXIS_FIFO_GET_TX_BYTES_SENT _IOR(AXIS_FIFO_IOCTL_MAGIC, 11, uint32_t)
#define AXIS_FIFO_GET_RX_PKTS_READ _IOR(AXIS_FIFO_IOCTL_MAGIC, 12, uint32_t)
#define AXIS_FIFO_GET_RX_BYTES_READ _IOR(AXIS_FIFO_IOCTL_MAGIC, 13, uint32_t)

struct axis_fifo_kern_regInfo {
  uint32_t regNo;
  uint32_t regVal;
};

namespace plogik {

AxiFifoKernel::AxiFifoKernel()
{
}

AxiFifoKernel::AxiFifoKernel(const std::filesystem::path& path, const grund::FileDescriptor::OpenMode direction)
  : _fd(grund::FileDescriptor::open(path, static_cast<grund::FileDescriptor::OpenMode>(direction | grund::FileDescriptor::NON_BLOCKING)))
{
}

void AxiFifoKernel::reset()
{
  _fd.ioctl(AXIS_FIFO_RESET_IP);
}

size_t AxiFifoKernel::write(uint8_t destination, const std::span<uint32_t>& values)
{
  (void)destination;
  return _fd.write(values) / sizeof(uint32_t);
}

size_t AxiFifoKernel::write(const std::span<uint32_t>& values)
{
  return write(0, values);
}

size_t AxiFifoKernel::read(uint8_t& destination, std::span<uint32_t>& values)
{
  destination = 0;
  return _fd.read(values) / sizeof(uint32_t);
}

size_t AxiFifoKernel::read(std::span<uint32_t>& values)
{
  return _fd.read(values) / sizeof(uint32_t);
}

size_t AxiFifoKernel::read_ready()
{
  return 0;
}

}