#include <filesystem>
#include <fmt/core.h>
#include <grund/convert.hpp>
#include <plogik/u_dma_buf.hpp>

namespace plogik {
/*
/dev/<device-name>
/sys/class/u-dma-buf/<device-name>/phys_addr
/sys/class/u-dma-buf/<device-name>/size
/sys/class/u-dma-buf/<device-name>/sync_mode
/sys/class/u-dma-buf/<device-name>/sync_offset
/sys/class/u-dma-buf/<device-name>/sync_size
/sys/class/u-dma-buf/<device-name>/sync_direction
/sys/class/u-dma-buf/<device-name>/sync_owner
/sys/class/u-dma-buf/<device-name>/sync_for_cpu
/sys/class/u-dma-buf/<device-name>/sync_for_device
/sys/class/u-dma-buf/<device-name>/dma_coherent
*/

std::filesystem::path u_dma_buf_device_path(const std::string& name)
{
  return std::filesystem::path { fmt::format("/dev/{}", name) };
}

std::filesystem::path u_dma_buf_sysfs_path(const std::string& name)
{
  return std::filesystem::path { fmt::format("/sys/class/u-dma-buf/{}", name) };
}

UDmaBuf::UDmaBuf(const std::string& name)
{
  size_t address;
  size_t size;
  auto path = u_dma_buf_sysfs_path(name);
  if (!std::filesystem::exists(path)) {
    throw std::invalid_argument("Device does not exist");
  }
  try {
    address = grund::from_string<size_t>(grund::read_string_rtrim(path.append("phys_addr"), 64));
    size = grund::from_string<size_t>(grund::read_string_rtrim(path.replace_filename("size"), 64));
  }
  catch (const std::exception&) {
    throw std::invalid_argument("Invalid device configuration");
  }
  _name = name;
  _address = address;
  _size = size;
}

grund::MemoryMap UDmaBuf::to_memory_map(bool sync) const
{
  auto device_path = u_dma_buf_device_path(_name);
  int mode = grund::FileDescriptor::READ | grund::FileDescriptor::WRITE;
  if (sync) {
    mode |= grund::FileDescriptor::SYNC;
  }
  auto fd = grund::FileDescriptor::open(device_path, static_cast<grund::FileDescriptor::OpenMode>(mode));
  return grund::MemoryMap::map_read_write(fd.fd(), _size, 0);
}

void UDmaBufManager::add_buffer(const std::string name, size_t size)
{
  grund::FileDescriptor fd = open_manager();
  std::string command(128, '\0');
  sprintf(command.data(), "create %s 0x%zx", name.c_str(), size);
  fd.write_string(command);
}

void UDmaBufManager::remove_buffer(const std::string name)
{
  grund::FileDescriptor fd = open_manager();
  std::string command(128, '\0');
  sprintf(command.data(), "delete %s", name.c_str());
  fd.write_string(command);
}

grund::FileDescriptor UDmaBufManager::open_manager()
{
  std::filesystem::path device_file_path(DEVICE_FILE_PATH);
  if (!std::filesystem::exists(device_file_path)) {
    throw std::runtime_error("No u-dma-buf-mgr available");
  }
  return grund::FileDescriptor::open(device_file_path, grund::FileDescriptor::OpenMode::WRITE);
}

}