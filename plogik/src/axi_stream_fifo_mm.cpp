#include <fmt/core.h>
#include <grund/dev_mem.hpp>
#include <plogik/axi_stream_fifo_mm.hpp>

namespace plogik {

AxiStreamFifoMm::AxiStreamFifoMm(const UIODescription& description, const int data_width)
{
  std::vector<UIOMapDescription> maps = description.maps();
  if (maps.size() >= 1) {
    _mmap = maps[0].to_memory_map();
  }
  else {
    throw std::invalid_argument("UIO without mappings");
  }
  if (maps.size() >= 2) {
    _mmap_full = maps[1].to_memory_map();
    if (data_width == 32 || data_width == 64 || data_width == 128 || data_width == 256 || data_width == 512) {
      _data_width = data_width / 8;
    }
    else {
      throw std::invalid_argument("Invalid data width");
    }
  }
  else {
    _data_width = sizeof(uint32_t);
  }
}

AxiStreamFifoMm::AxiStreamFifoMm(const size_t axi4_lite_physical_address)
  : _mmap(grund::DevMem::to_memory_map(axi4_lite_physical_address, 0x80))
{
}

AxiStreamFifoMm::AxiStreamFifoMm(const size_t axi4_lite_physical_address, const size_t axi4_physical_address, int data_width)
  : _mmap(grund::DevMem::to_memory_map(axi4_lite_physical_address, 0x80))
  , _mmap_full(grund::DevMem::to_memory_map(axi4_physical_address, 0x2000))
{
  if (data_width == 32 || data_width == 64 || data_width == 128 || data_width == 256 || data_width == 512) {
    _data_width = data_width / 8;
  }
  else {
    throw std::invalid_argument("Invalid data width");
  }
}

void AxiStreamFifoMm::reset()
{
  _mmap.set(REG_AXI4_STREAM_RESET, RESET_MAGIC);
  _mmap.set(REG_TX_RESET, RESET_MAGIC);
  _mmap.set(REG_RX_RESET, RESET_MAGIC);
  _mmap.set(REG_INT_ENABLE, INT_TXC | INT_RXC | INT_RX_URE | INT_RX_ORE | INT_RX_UR | INT_TX_ORE | INT_TXLE);
  interrupts_clear();
  // Can REG_TX_VACANCY and REG_RX_OCCUPANCY be used to detect read-only, write-only FIFOs?
  uint32_t vacancy = _mmap.get<uint32_t>(REG_TX_VACANCY);
  fprintf(stdout, "AxiStreamFifoMm TX vacancy %u\n", vacancy);
  uint32_t occupancy = _mmap.get<uint32_t>(REG_RX_OCCUPANCY);
  fprintf(stdout, "AxiStreamFifoMm RX occupancy %u\n", occupancy);
}

void AxiStreamFifoMm::interrupts_clear()
{
  _mmap.set(REG_INT_STATUS, INT_ALL);
}

void AxiStreamFifoMm::interrupts_clear_tx()
{
  _mmap.set(REG_INT_STATUS, INT_TXERROR | INT_TXC | INT_TXLE);
}

void AxiStreamFifoMm::interrupts_clear_rx()
{
  _mmap.set(REG_INT_STATUS, INT_RXERROR | INT_RXC);
}

inline void AxiStreamFifoMm::write_setup(uint8_t destination)
{
  interrupts_clear_tx();
  _mmap.set(REG_TRANSMIT_DST, static_cast<uint32_t>(destination & 0x0f));
}

inline void AxiStreamFifoMm::write_finish(uint32_t bytes)
{
  uint32_t interrupts;
  _mmap.set(REG_TX_LENGTH, bytes);
  do {
    interrupts = _mmap.get<uint32_t>(REG_INT_STATUS);
    if ((interrupts & INT_TXERROR) != 0) [[unlikely]] {
      break;
    }
  } while ((interrupts & INT_TXC) == 0);
  if ((interrupts & INT_TXERROR) != 0) [[unlikely]] {
    fprintf(stdout, "AxiStreamFifoMm TX failed %08x\n", interrupts);
    reset();
  }
  interrupts_clear_tx();
}

void AxiStreamFifoMm::write_lite(const uint32_t* data, size_t length)
{
  for (const uint32_t* ptr = data; ptr < data + length; ptr++) {
    _mmap.set(REG_TX_DATA, *ptr);
  }
}

void AxiStreamFifoMm::write_full(const uint32_t* data, size_t length)
{
  switch (_data_width) {
  case sizeof(uint32_t):
    {
      for (const uint32_t* ptr = data; ptr < data + length; ptr++) {
        _mmap_full.set(FULL_REG_WRITE, *ptr);
      }
    }
    break;
  case sizeof(uint64_t):
    {
      const uint64_t* data_u64 = reinterpret_cast<const uint64_t*>(data);
      size_t length_u64 = length / 2;
      for (const uint64_t* ptr = data_u64; ptr < (data_u64 + length_u64); ptr++) {
        _mmap_full.set(FULL_REG_WRITE, *ptr);
      }
    }
    break;
  case 16:
  case 32:
  case 64:
    throw std::runtime_error("Not implemented");
  }
}

size_t AxiStreamFifoMm::write(uint8_t destination, const uint32_t* words, size_t words_length)
{
  write_setup(destination);
  if (_mmap_full) {
    write_full(words, words_length);
  }
  else {
    write_lite(words, words_length);
  }
  size_t bytes = words_length * sizeof(uint32_t);
  write_finish(bytes);
  return bytes;
}

size_t AxiStreamFifoMm::write(uint8_t destination, const std::span<uint32_t>& values)
{
  return write(destination, values.data(), values.size());
}

size_t AxiStreamFifoMm::write(const std::span<uint32_t>& values)
{
  return write(0, values);
}

size_t AxiStreamFifoMm::write(uint8_t destination, const std::span<uint64_t>& values)
{
  return write(destination, reinterpret_cast<const uint32_t*>(values.data()), values.size() * 2);
}

void AxiStreamFifoMm::read_full(uint32_t* data, size_t length)
{
  switch (_data_width) {
  case sizeof(uint32_t):
    {
      for (uint32_t* ptr = data; ptr < data + length; ptr++) {
        *ptr = _mmap_full.get<uint32_t>(FULL_REG_READ);
      }
    }
    break;
  case sizeof(uint64_t):
    {
      uint64_t* data_u64 = reinterpret_cast<uint64_t*>(data);
      size_t length_u64 = length / 2;
      for (uint64_t* ptr = data_u64; ptr < data_u64 + length_u64; ptr++) {
        *ptr = _mmap_full.get<uint64_t>(FULL_REG_READ);
      }
    }
    break;
  case 16:
  case 32:
  case 64:
    throw std::runtime_error("Not implemented");
    break;
  }
}

size_t AxiStreamFifoMm::read(uint8_t& destination, uint32_t* words, size_t words_length)
{
  uint32_t available = _mmap.get<uint32_t>(REG_RX_OCCUPANCY);
  if (available == 0) {
    fmt::print(stderr, "AxiStreamFifoMm RX no data available, {}\n", available);
    return 0;
  }
  // REG_RECEIVE_FIFO_DATA_READ and REG_RECEIVE_FIFO_LENGTH seems to fail
  // with bus error if there has been no transfer.
  interrupts_clear_rx();
  size_t fifo_length = static_cast<size_t>(_mmap.get<uint32_t>(REG_RX_LENGTH) / sizeof(uint32_t));
  size_t length = std::min(fifo_length, words_length);
  uint8_t dst = static_cast<uint8_t>(_mmap.get<uint32_t>(REG_RECEIVE_DST));

  if (_mmap_full) {
    read_full(words, length);
  }
  else {
    for (uint32_t* ptr = words; ptr < words + length; ptr++) {
      *ptr = _mmap.get<uint32_t>(REG_RX_DATA);
    }
  }
  uint32_t interrupts = _mmap.get<uint32_t>(REG_INT_STATUS);
  if ((interrupts & INT_RXERROR) != 0) [[unlikely]] {
    fprintf(stderr, "AxiStreamFifoMm RX failed %08x\n", interrupts);
    reset();
    return 0;
  }
  destination = dst;
  return length * sizeof(uint32_t);
}

size_t AxiStreamFifoMm::read(uint8_t& destination, std::span<uint32_t>& values)
{
  return read(destination, values.data(), values.size());
}

size_t AxiStreamFifoMm::read(std::span<uint32_t>& values)
{
  uint8_t dst;
  return read(dst, values);
}

size_t AxiStreamFifoMm::read(uint8_t& destination, std::span<uint64_t>& values)
{
  return read(destination, reinterpret_cast<uint32_t*>(values.data()), values.size() * 2);
}

size_t AxiStreamFifoMm::read_ready()
{
  uint32_t interrupts = _mmap.get<uint32_t>(REG_INT_STATUS);
  if ((interrupts & INT_RXC) != INT_RXC) {
    return 0;
  }
  return static_cast<size_t>(_mmap.get<uint32_t>(REG_RX_LENGTH) / sizeof(uint32_t));
}

void AxiStreamFifoMm::read_interrupt_enable()
{
  uint32_t interrupts = _mmap.get<uint32_t>(REG_INT_ENABLE);
  interrupts |= INT_RXC;
  _mmap.set(REG_INT_ENABLE, interrupts);
}

}