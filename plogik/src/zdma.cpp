#include <cstdio>
#include <cstdlib>
#include <grund/file_descriptor.hpp>
#include <grund/memory_map.hpp>
#include <grund/system.hpp>
#include <plogik/zdma.hpp>

namespace plogik {

Zdma::Zdma(grund::MemoryMap&& mm)
  : _mm(std::move(mm))
{
}

void Zdma::print()
{
  fprintf(stdout, "%04zx: %08x\n", REG_ERR_CTRL, _mm.get<uint32_t>(REG_ERR_CTRL));
  fprintf(stdout, " ISR: %08x\n", _mm.get<uint32_t>(REG_ISR));
  fprintf(stdout, " IMR: %08x\n", _mm.get<uint32_t>(REG_IMR));
  fprintf(stdout, "%04zx: %08x\n", REG_CTRL0, _mm.get<uint32_t>(REG_CTRL0));
  fprintf(stdout, "%04zx: %08x\n", REG_CTRL1, _mm.get<uint32_t>(REG_CTRL1));
  fprintf(stdout, "%04zx: %08x\n", REG_FCI, _mm.get<uint32_t>(REG_FCI));
  fprintf(stdout, "%04zx: %08x\n", REG_STATUS, _mm.get<uint32_t>(REG_STATUS));
  fprintf(stdout, "%04zx: %08x\n", REG_DATA_ATTR, _mm.get<uint32_t>(REG_DATA_ATTR));
  fprintf(stdout, "%04zx: %08x\n", REG_DSCR_ATTR, _mm.get<uint32_t>(REG_DSCR_ATTR));
  fprintf(stdout, "%04zx: %08x\n", REG_SRC_DSCR_WORD0, _mm.get<uint32_t>(REG_SRC_DSCR_WORD0));
  fprintf(stdout, "%04zx: %08x\n", REG_SRC_DSCR_WORD1, _mm.get<uint32_t>(REG_SRC_DSCR_WORD1));
  fprintf(stdout, "%04zx: %08x\n", REG_SRC_DSCR_WORD2, _mm.get<uint32_t>(REG_SRC_DSCR_WORD2));
  fprintf(stdout, "%04zx: %08x\n", REG_SRC_DSCR_WORD3, _mm.get<uint32_t>(REG_SRC_DSCR_WORD3));
  fprintf(stdout, "%04zx: %08x\n", REG_DST_DSCR_WORD0, _mm.get<uint32_t>(REG_DST_DSCR_WORD0));
  fprintf(stdout, "%04zx: %08x\n", REG_DST_DSCR_WORD1, _mm.get<uint32_t>(REG_DST_DSCR_WORD1));
  fprintf(stdout, "%04zx: %08x\n", REG_DST_DSCR_WORD2, _mm.get<uint32_t>(REG_DST_DSCR_WORD2));
  fprintf(stdout, "%04zx: %08x\n", REG_DST_DSCR_WORD3, _mm.get<uint32_t>(REG_DST_DSCR_WORD3));
}

void Zdma::reset()
{
  // disable DMA
  _mm.set(REG_CTRL2, UINT32_C(0));
  // clear error
  _mm.set(REG_ERR_CTRL, 0);
  // disable all interrupts
  _mm.set(REG_IDS, INT_MASK);
  // clear all interrupts
  _mm.set(REG_ISR, INT_MASK);
  // reset to default
  _mm.set(REG_CTRL0, UINT32_C(0x00000080));
  // reset to default
  _mm.set(REG_CTRL1, UINT32_C(0x000003FF));
  // reset to default
  _mm.set(REG_DATA_ATTR, UINT32_C(0x0483D20F));
  // reset to default
  _mm.set(REG_DSCR_ATTR, UINT32_C(0));
  // write to clear counter
  _mm.set(REG_TOTAL_BYTE, UINT32_C(0xffffffff));
  // read to clear
  _mm.get<uint32_t>(REG_IRQ_DST_ACCT);
  // read to clear
  _mm.get<uint32_t>(REG_IRQ_SRC_ACCT);
}

Zdma::State Zdma::get_state()
{
  return static_cast<Zdma::State>(_mm.get<uint32_t>(REG_STATUS) & 0x0003);
}

void Zdma::set_mode(const Zdma::Mode mode)
{
  _mm.modify(REG_CTRL0, (static_cast<uint32_t>(mode) << 4), 0x00000030u);
}

uint32_t Zdma::get_interrupts()
{
  return _mm.get<uint32_t>(REG_ISR);
}

int Zdma::check_ready()
{
  State state = get_state();
  switch (state) {
  case State::Busy:
    return EAGAIN;
  case State::Paused:
    {
      // enter disabled state
      enable_dma(false);
      _mm.modify(REG_CTRL0, 2, 2);
    }
  case State::Done:
  case State::DoneError:
    break;
  }
  return 0;
}

int Zdma::start_simple_transfer(const size_t source, const size_t destination, const size_t length)
{
  if (length > 0x3fffffff) {
    return EINVAL;
  }
  int result = check_ready();
  if (result != 0) {
    return result;
  }
  enable_dma(false);
  // clear all interrupts
  _mm.set(REG_ISR, INT_MASK);
  // enable interrupts
  _mm.set(REG_IEN, INT_MASK);
  // select simple mode
  _mm.modify(REG_CTRL0, UINT32_C(0x00000000), UINT32_C(0x00000040));
  // Source address lower
  _mm.set(REG_SRC_DSCR_WORD0, static_cast<uint32_t>(source));
  // Source address upper
  _mm.set(REG_SRC_DSCR_WORD1, static_cast<uint32_t>(source >> 32));
  // Destination address lower
  _mm.set(REG_DST_DSCR_WORD0, static_cast<uint32_t>(destination));
  // Destination address upper
  _mm.set(REG_DST_DSCR_WORD1, static_cast<uint32_t>(destination >> 32));
  // Source length
  _mm.set(REG_SRC_DSCR_WORD2, static_cast<uint32_t>(length));
  // Destination length
  _mm.set(REG_DST_DSCR_WORD2, static_cast<uint32_t>(length));
  uint32_t control = DESC_CTR_INT; // Enable interrupt for finished transaction
  // Source control word
  _mm.set(REG_SRC_DSCR_WORD3, control);
  // Destination control word
  _mm.set(REG_DST_DSCR_WORD3, control);
  // Enable transaction
  enable_dma(true);
  return 0;
}

int Zdma::transfer_status()
{
  uint32_t interrupts = get_interrupts();
  if ((interrupts & INT_ERROR) != 0) {
    return EIO;
  }
  if ((interrupts & INT_DONE) == INT_DONE) {
    return 0;
  }
  State state = get_state();
  switch (state) {
  case State::Busy:
    return EAGAIN;
  case State::DoneError:
    return EIO;
  case State::Paused:
  case State::Done:
    break;
  }
  return 0;
}

void Zdma::enable_dma(bool enable)
{
  _mm.set(REG_CTRL2, enable ? 1 : 0); // enable DMA
}

constexpr size_t Zdma::base_address(const Zdma::Instance instance)
{
  switch (instance) {
  case Zdma::Instance::ADMA0:
    return ADMA0_BASE_ADDRESS;
  case Zdma::Instance::ADMA1:
    return ADMA1_BASE_ADDRESS;
  case Zdma::Instance::ADMA2:
    return ADMA2_BASE_ADDRESS;
  case Zdma::Instance::ADMA3:
    return ADMA3_BASE_ADDRESS;
  case Zdma::Instance::ADMA4:
    return ADMA4_BASE_ADDRESS;
  case Zdma::Instance::ADMA5:
    return ADMA5_BASE_ADDRESS;
  case Zdma::Instance::ADMA6:
    return ADMA6_BASE_ADDRESS;
  case Zdma::Instance::ADMA7:
    return ADMA7_BASE_ADDRESS;
  case Zdma::Instance::GDMA0:
    return GDMA0_BASE_ADDRESS;
  case Zdma::Instance::GDMA1:
    return GDMA1_BASE_ADDRESS;
  case Zdma::Instance::GDMA2:
    return GDMA2_BASE_ADDRESS;
  case Zdma::Instance::GDMA3:
    return GDMA3_BASE_ADDRESS;
  case Zdma::Instance::GDMA4:
    return GDMA4_BASE_ADDRESS;
  case Zdma::Instance::GDMA5:
    return GDMA5_BASE_ADDRESS;
  case Zdma::Instance::GDMA6:
    return GDMA6_BASE_ADDRESS;
  case Zdma::Instance::GDMA7:
    return GDMA7_BASE_ADDRESS;
  }
  return 0;
}

void Zdma::activate_fpd_dma_clk()
{
  const unsigned int FPD_DMA_REF_CTRL_ADDR = 0xFD1A00B8;
  const unsigned int ACTIVATE_CLOCK_MASK = 0x01000000;
  grund::FileDescriptor fd = grund::FileDescriptor::open(std::string_view { "/dev/mem" }, grund::FileDescriptor::READ | grund::FileDescriptor::WRITE);
  grund::MemoryMap mm = grund::MemoryMap::map_read_write(fd.fd(), grund::System::get_page_size(), FPD_DMA_REF_CTRL_ADDR);
  uint32_t value = mm.get<uint32_t>(0);
  mm.set(0, value | ACTIVATE_CLOCK_MASK);
}

}