#include <cstdio>
#include <fmt/core.h>
#include <grund/convert.hpp>
#include <grund/memory_map.hpp>
#include <grund/string.hpp>
#include <grund/system.hpp>
#include <grund/system_error.hpp>
#include <plogik/uio_device.hpp>
extern "C" {
#include <fcntl.h>
#include <poll.h>
#include <sys/mman.h>
}

using namespace plogik;

std::filesystem::path uio_device_path(uint16_t uio_number)
{
  return std::filesystem::path { fmt::format("/dev/uio{}", uio_number) };
}

std::filesystem::path uio_sysfs_path(uint16_t uio_number)
{
  return std::filesystem::path { fmt::format("/sys/class/uio/uio{}", uio_number) };
}

std::filesystem::path uio_maps_path(uint16_t uio_number)
{
  return uio_sysfs_path(uio_number).append("maps");
}

std::filesystem::path uio_map_path(uint16_t uio_number, uint16_t map_number)
{
  return uio_maps_path(uio_number).append(fmt::format("map{}", map_number));
}

UIOMapDescription::UIOMapDescription()
  : _uio_number(UIODescription::INVALID)
  , _map_number(UIODescription::INVALID)
{
}

UIOMapDescription::UIOMapDescription(const UIODescription& description, uint16_t map)
  : _uio_number(description.uio())
  , _map_number(map)
{
  size_t address;
  size_t size;
  size_t offset;
  auto map_path = uio_map_path(_uio_number, _map_number);
  try {
    address = grund::from_string<size_t>(grund::read_string_rtrim(map_path.append("addr"), 64));
    size = grund::from_string<size_t>(grund::read_string_rtrim(map_path.replace_filename("size"), 64));
    offset = grund::from_string<size_t>(grund::read_string_rtrim(map_path.replace_filename("offset"), 64));
  }
  catch (const std::exception&) {
    throw std::invalid_argument("Invalid uio map");
  }
  _address = address;
  _size = size;
  _offset = offset;
}

UIOMapDescription::UIOMapDescription(uint16_t uio, uint16_t map)
  : UIOMapDescription(UIODescription(uio), map)
{
}

UIOMapDescription::UIOMapDescription(const std::string& name, uint16_t map)
  : UIOMapDescription(UIODescription(name), map)
{
}

grund::MemoryMap UIOMapDescription::to_memory_map() const
{
  UIODeviceFile dev(uio());
  // UIO devices with multiple maps are differentiated with map number
  // times page size as offset.
  off_t offset = _map_number * grund::System::get_page_size();
  return grund::MemoryMap::map_read_write(dev.fd(), _size, offset);
}

std::string UIOMapDescription::name() const
{
  return grund::read_string_rtrim(uio_map_path(_uio_number, _map_number).append("name"), 256);
}

UIODescription::UIODescription()
  : _uio_number(INVALID)
{
}

UIODescription::UIODescription(uint16_t number)
  : _uio_number(number)
{
  std::filesystem::path uio_path = uio_sysfs_path(_uio_number);
  if (!std::filesystem::exists(uio_path)) {
    throw std::invalid_argument("UIO do not exist");
  }
}

UIODescription::UIODescription(const std::string& name)
  : _uio_number(INVALID)
{
  for (const UIODescription& description : enumerate()) {
    if (description.name() == name) {
      _uio_number = description.uio();
    }
  }
  if (INVALID == _uio_number) {
    throw std::invalid_argument("UIO do not exist");
  }
}

std::string UIODescription::name() const
{
  return grund::read_string_rtrim(uio_sysfs_path(_uio_number).append("name"), 256);
}

std::vector<UIOMapDescription> UIODescription::maps() const
{
  std::vector<UIOMapDescription> maps;
  for (uint16_t n = 0; n < 128; n++) {
    try {
      UIOMapDescription description(_uio_number, n);
      maps.push_back(description);
    }
    catch (const std::invalid_argument&) {
      break;
    }
  }
  return maps;
}
std::vector<UIODescription> UIODescription::enumerate()
{
  std::vector<UIODescription> uios;
  for (uint16_t n = 0; n < 128; n++) {
    std::filesystem::path uio_path = uio_sysfs_path(n);
    if (std::filesystem::exists(uio_path)) {
      uios.push_back(UIODescription(n));
    }
    else {
      break;
    }
  }
  return uios;
}

UIODeviceFile::UIODeviceFile(uint16_t uio)
  : grund::FileDescriptor(uio_device_path(uio), grund::FileDescriptor::READ | grund::FileDescriptor::WRITE | grund::FileDescriptor::NON_BLOCKING)
{
}

uint32_t UIODeviceFile::interrupt_wait()
{
  struct pollfd poll_data;

  poll_data.fd = fd();
  poll_data.events = POLLIN;
  poll_data.revents = 0;
  int poll_count = 1;

  int result = poll(&poll_data, poll_count, -1);
  if (-1 == result) {
    throw grund::SystemError();
  }
  else if (1 == result) {
    auto value = interrupt_counter();
    return value.value();
  }
  return std::numeric_limits<uint32_t>::max();
}

void UIODeviceFile::interrupt_arm()
{
  uint32_t mask = 1;
  write_object(mask);
}

std::optional<uint32_t> UIODeviceFile::interrupt_counter()
{
  return read_object_non_blocking<uint32_t>();
}
