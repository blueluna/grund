#include <algorithm>
#include <cinttypes>
#include <cstdlib>
#include <limits>
extern "C" {
#include <getopt.h>
}
#include <grund/convert.hpp>
#include <grund/time.hpp>
#include <plogik/axi_stream_fifo_mm.hpp>
#include <plogik/u_dma_buf.hpp>
#include <plogik/uio_device.hpp>
#include <plogik/zdma.hpp>

static constexpr size_t WRITE_FIFO_ADDRESS { 0xa0020000 };
static constexpr size_t READ_FIFO_ADDRESS { 0xa0030000 };
static constexpr size_t FIFO_SIZE { 0x00001000 };
static constexpr size_t FIFO_WRITE_OFFSET { 0x00000000 };
static constexpr size_t FIFO_READ_OFFSET { 0x00001000 };

int fifo_read(plogik::Zdma& dma, plogik::UDmaBuf& buffer, int argc, char* const* argv)
{
  using namespace std::literals::chrono_literals;
  if (argc < 1) {
    fprintf(stderr, "No address to read\n");
    return EXIT_FAILURE;
  }
  size_t size;
  try {
    size = grund::from_string<size_t>(argv[0]);
  }
  catch (const std::invalid_argument&) {
    size = std::numeric_limits<size_t>::max();
  }
  size_t bytes = size * sizeof(uint32_t);
  if (FIFO_SIZE < bytes) {
    fprintf(stderr, "Invalid size to read\n");
    return EXIT_FAILURE;
  }
  auto buffer_mm = buffer.to_memory_map(true);
  uint32_t* values = reinterpret_cast<uint32_t*>(buffer_mm.pointer_mut());
  uint32_t* ptr = values;
  for (size_t n = 0; n < size; n++) {
    *ptr = n;
    ptr++;
  }
  auto start_time = std::chrono::steady_clock::now();
  int result = dma.start_simple_transfer(READ_FIFO_ADDRESS + FIFO_READ_OFFSET, buffer.address(), bytes);
  if (result != 0) {
    fprintf(stderr, "failed to start transfer, %s\n", strerror(result));
    return EXIT_FAILURE;
  }
  do {
    result = dma.transfer_status();
  } while (result == EAGAIN);
  auto end_time = std::chrono::steady_clock::now();
  if (result != 0) {
    uint32_t flags = dma.get_interrupts();
    fprintf(stderr, "failed to finish transfer, %s. %08x\n", strerror(result), flags);
    return EXIT_FAILURE;
  }
  for (uint16_t n = 0; n < size; n++) {
    if ((n % 8) == 0) {
      fprintf(stdout, "%04x: ", n);
    }
    fprintf(stdout, "%04x ", *values);
    values++;
    if (((n + 1) % 8) == 0) {
      fprintf(stdout, "\n");
    }
  }
  fprintf(stdout, "\n");
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  double bytes_per_seconds = static_cast<double>(std::chrono::nanoseconds(1s).count() * size) / static_cast<double>(elapsed.count());
  fprintf(stdout, "Size %zu Read took: %" PRIi64 " ns %9.5f Mb/s\n", size, elapsed.count(), bytes_per_seconds / (1024.0 * 1024.0));
  return EXIT_SUCCESS;
}

int fifo_write(plogik::Zdma& dma, plogik::UDmaBuf& buffer, int argc, char* const* argv)
{
  using namespace std::literals::chrono_literals;
  if (argc < 2) {
    fprintf(stderr, "No address to read\n");
    return EXIT_FAILURE;
  }
  size_t size;
  uint32_t value;
  try {
    size = grund::from_string<size_t>(argv[0]);
    value = grund::from_string<uint32_t>(argv[1]);
  }
  catch (const std::invalid_argument&) {
    size = std::numeric_limits<size_t>::max();
  }
  size_t bytes = size * sizeof(uint32_t);
  if (FIFO_SIZE < bytes) {
    fprintf(stderr, "Invalid size to write\n");
    return EXIT_FAILURE;
  }
  bool counter = false;
  {
    auto buffer_mm = buffer.to_memory_map(true);
    uint32_t* values = reinterpret_cast<uint32_t*>(buffer_mm.pointer_mut());
    for (size_t n = 0; n < size; n++) {
      if (counter) {
        *values = (0x4000 - n);
      }
      else {
        *values = value;
      }
      values++;
    }
  }
  auto start_time = std::chrono::steady_clock::now();
  int result = dma.start_simple_transfer(buffer.address(), WRITE_FIFO_ADDRESS + FIFO_WRITE_OFFSET, bytes);
  if (result != 0) {
    fprintf(stderr, "failed to start transfer, %s\n", strerror(result));
    return EXIT_FAILURE;
  }
  do {
    result = dma.transfer_status();
  } while (result == EAGAIN);
  auto end_time = std::chrono::steady_clock::now();
  if (result != 0) {
    uint32_t flags = dma.get_interrupts();
    fprintf(stderr, "failed to finish transfer, %s. %08x\n", strerror(result), flags);
    return EXIT_FAILURE;
  }
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  double bytes_per_seconds = static_cast<double>(std::chrono::nanoseconds(1s).count() * size) / static_cast<double>(elapsed.count());
  fprintf(stdout, "Size %zu Write took: %" PRIi64 " ns %9.5f Mb/s\n", size, elapsed.count(), bytes_per_seconds / (1024.0 * 1024.0));
  return EXIT_SUCCESS;
}

enum class Command {
  READ,
  WRITE,
};

extern int optind;
extern char* optarg;

int main(int argc, char* argv[])
{
  static struct option long_options[] = {
    { 0, 0, 0, 0 }
  };
  int long_option_index = 0;
  int option_index = 0;

  while (1) {
    int c = getopt_long(argc, argv, "",
      long_options, &long_option_index);
    option_index = optind;
    if (c == -1) {
      break;
    }
    switch (c) {
    default:
      break;
    }
  }

  size_t arguments_length = 0;
  char* const* arguments = nullptr;
  Command command = Command::READ;
  if ((argc - option_index) > 0) {
    std::string command_str { argv[option_index] };
    arguments_length = argc - option_index - 1;
    arguments = &argv[option_index + 1];
    if (command_str == "read") {
      command = Command::READ;
    }
    else if (command_str == "write") {
      command = Command::WRITE;
    }
    else {
      fprintf(stderr, "Unknown command %s\n", command_str.c_str());
      return EXIT_FAILURE;
    }
  }

  plogik::Zdma::activate_fpd_dma_clk();
  int result = EXIT_SUCCESS;
  std::string buffer_name { "fifo_dma_buffer" };
  plogik::UDmaBufManager::add_buffer(buffer_name, 8 * 1024);
  {
    auto user_buffer = plogik::UDmaBuf(buffer_name);
    auto uio_description = plogik::UIOMapDescription("gdma1", 0);
    auto dma = plogik::Zdma(uio_description.to_memory_map());

    switch (command) {
    case Command::READ:
      result = fifo_read(dma, user_buffer, arguments_length, arguments);
      break;
    case Command::WRITE:
      result = fifo_write(dma, user_buffer, arguments_length, arguments);
      break;
    }
  }
  plogik::UDmaBufManager::remove_buffer(buffer_name);
  return result;
}