#include <algorithm>
#include <cinttypes>
#include <cstdlib>
#include <limits>
extern "C" {
#include <getopt.h>
}
#include <grund/convert.hpp>
#include <grund/time.hpp>
#include <memory>
#include <plogik/uio_device.hpp>

int bram_read(grund::MemoryMap& bram, int argc, char* const* argv)
{
  using namespace std::literals::chrono_literals;
  if (argc < 1) {
    fprintf(stderr, "No address to read\n");
    return EXIT_FAILURE;
  }
  size_t size;
  try {
    size = grund::from_string<size_t>(argv[0]);
  }
  catch (const std::invalid_argument&) {
    size = std::numeric_limits<size_t>::max();
  }
  if (std::numeric_limits<size_t>::max() == size) {
    fprintf(stderr, "Invalid size to read\n");
    return EXIT_FAILURE;
  }
  std::vector<uint32_t> data(size, 0);
  auto slice = std::span { data };
  const uint32_t* ptr = bram.offset<uint32_t>(0);
  auto start_time = std::chrono::steady_clock::now();
  std::copy(ptr, ptr + size, slice.data());
  auto end_time = std::chrono::steady_clock::now();
  size_t part = std::min(size, 16ul);
  for (uint32_t value : std::span { data.begin(), part }) {
    fprintf(stdout, "%08x\n", value);
  }
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  double bytes_per_seconds = static_cast<double>(std::chrono::nanoseconds(1s).count() * size) / static_cast<double>(elapsed.count());
  fprintf(stdout, "Size %zu Read took: %" PRIi64 " ns %9.5f Mb/s\n", size, elapsed.count(), bytes_per_seconds / (1024.0 * 1024.0));
  return EXIT_SUCCESS;
}

int bram_write(grund::MemoryMap& bram, int argc, char* const* argv)
{
  using namespace std::literals::chrono_literals;
  if (argc < 2) {
    fprintf(stderr, "No address to read\n");
    return EXIT_FAILURE;
  }
  size_t size;
  uint64_t value;
  try {
    size = grund::from_string<size_t>(argv[0]);
    value = grund::from_string<uint64_t>(argv[1]);
  }
  catch (const std::invalid_argument&) {
    size = std::numeric_limits<size_t>::max();
  }
  if (std::numeric_limits<size_t>::max() == size) {
    fprintf(stderr, "Invalid address or value to write\n");
    return EXIT_FAILURE;
  }
  std::vector<uint64_t> data(size, value);
  for (size_t n = 0; n < size; n++) {
    data[n] = value + static_cast<uint64_t>(n);
  }
  auto start_time = std::chrono::steady_clock::now();
  for (size_t n = 0; n < size; n++) {
    bram.set_stride<uint32_t>(n, data[n]);
  }
  auto end_time = std::chrono::steady_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  double bytes_per_seconds = static_cast<double>(std::chrono::nanoseconds(1s).count() * size) / static_cast<double>(elapsed.count());
  fprintf(stdout, "Size %zu Write took: %" PRIi64 " ns %9.5f Mb/s\n", size, elapsed.count(), bytes_per_seconds / (1024.0 * 1024.0));
  return EXIT_SUCCESS;
}

enum class Command {
  READ,
  WRITE,
};

extern int optind;
extern char* optarg;

int main(int argc, char* argv[])
{
  static struct option long_options[] = {
    { "uio", required_argument, 0, 'u' },
    { 0, 0, 0, 0 }
  };
  int long_option_index = 0;
  int option_index = 0;
  uint16_t uio_number = std::numeric_limits<uint16_t>::max();
  std::string uio_name;

  while (1) {
    int c = getopt_long(argc, argv, "u:",
      long_options, &long_option_index);
    option_index = optind;
    if (c == -1) {
      break;
    }
    switch (c) {
    case 'u':
      try {
        uio_number = grund::from_string<uint16_t>(optarg);
      }
      catch (const std::exception&) {
        uio_name.assign(optarg);
      }
      break;
    default:
      break;
    }
  }

  size_t arguments_length = 0;
  char* const* arguments = nullptr;
  Command command = Command::READ;
  if ((argc - option_index) > 0) {
    std::string command_str { argv[option_index] };
    arguments_length = argc - option_index - 1;
    arguments = &argv[option_index + 1];
    if (command_str == "read") {
      command = Command::READ;
    }
    else if (command_str == "write") {
      command = Command::WRITE;
    }
    else {
      fprintf(stderr, "Unknown command %s\n", command_str.c_str());
      return EXIT_FAILURE;
    }
  }

  plogik::UIODescription uio_description;
  try {
    if (uio_number < std::numeric_limits<uint16_t>::max()) {
      uio_description = plogik::UIODescription(uio_number);
    }
    else if (!uio_name.empty()) {
      uio_description = plogik::UIODescription(uio_name);
    }
    else {
      fprintf(stderr, "No UIO device provided\n");
      return EXIT_FAILURE;
    }
  }
  catch (const std::exception& e) {
    fprintf(stderr, "Failed to enumerate uio, %s\n", e.what());
    return EXIT_FAILURE;
  }
  auto bram_mm = plogik::UIOMapDescription(uio_description, 0).to_memory_map();

  int result = EXIT_SUCCESS;
  switch (command) {
  case Command::READ:
    result = bram_read(bram_mm, arguments_length, arguments);
    break;
  case Command::WRITE:
    result = bram_write(bram_mm, arguments_length, arguments);
    break;
  }

  return result;
}