#include <algorithm>
#include <cinttypes>
#include <cstdlib>
extern "C" {
#include <getopt.h>
}
#include <grund/convert.hpp>
#include <grund/time.hpp>
#include <plogik/u_dma_buf.hpp>
#include <plogik/uio_device.hpp>
#include <plogik/zdma.hpp>

int dma_read(plogik::Zdma& dma, plogik::UDmaBuf& buffer, size_t address, size_t size, int argc, char* const* argv)
{
  using namespace std::literals::chrono_literals;
  (void)argc;
  (void)argv;
  uint32_t length = static_cast<uint32_t>(size / sizeof(uint32_t));
  auto buffer_mm = buffer.to_memory_map(true);
  uint32_t* values = reinterpret_cast<uint32_t*>(buffer_mm.pointer_mut());
  uint32_t* ptr = values;
  for (uint32_t n = 0; n < length; n++) {
    *ptr = n;
    ptr++;
  }
  auto start_time = std::chrono::steady_clock::now();
  int result = dma.start_simple_transfer(address, buffer.address(), size);
  if (result != 0) {
    fprintf(stderr, "failed to start transfer, %s\n", strerror(result));
    return EXIT_FAILURE;
  }
  do {
    result = dma.transfer_status();
  } while (result == EAGAIN);
  auto end_time = std::chrono::steady_clock::now();
  if (result != 0) {
    uint32_t flags = dma.get_interrupts();
    fprintf(stderr, "failed to finish transfer, %s. %08x\n", strerror(result), flags);
    return EXIT_FAILURE;
  }
  for (uint32_t n = 0; n < length; n++) {
    if ((n % 8) == 0) {
      fprintf(stdout, "%08x: ", n);
    }
    fprintf(stdout, "%08x ", *values);
    values++;
    if (((n + 1) % 8) == 0) {
      fprintf(stdout, "\n");
    }
  }
  fprintf(stdout, "\n");
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  double bytes_per_seconds = static_cast<double>(std::chrono::nanoseconds(1s).count() * size) / static_cast<double>(elapsed.count());
  fprintf(stdout, "Size %zu Read took: %" PRIi64 " ns %9.5f Mb/s\n", size, elapsed.count(), bytes_per_seconds / (1024.0 * 1024.0));
  return EXIT_SUCCESS;
}

int dma_reset(plogik::Zdma& dma)
{
  dma.reset();
  plogik::Zdma::State state = dma.get_state();
  fprintf(stdout, "DMA state %u\n", static_cast<uint32_t>(state));
  return EXIT_SUCCESS;
}

int dma_write(plogik::Zdma& dma, plogik::UDmaBuf& buffer, size_t address, size_t size, int argc, char* const* argv)
{
  using namespace std::literals::chrono_literals;
  bool counter = true;
  uint32_t value;
  if (argc >= 1) {
    try {
      value = grund::from_string<uint32_t>(argv[0]);
      counter = false;
    }
    catch (const std::invalid_argument&) {
      counter = true;
    }
  }
  {
    auto buffer_mm = buffer.to_memory_map(true);
    uint32_t* values = reinterpret_cast<uint32_t*>(buffer_mm.pointer_mut());
    uint32_t length = static_cast<uint32_t>(size / sizeof(uint32_t));
    for (uint32_t n = 0; n < length; n++) {
      if (counter) {
        *values = (0x4000 - n);
      }
      else {
        *values = value;
      }
      values++;
    }
  }
  auto start_time = std::chrono::steady_clock::now();
  int result = dma.start_simple_transfer(buffer.address(), address, size);
  if (result != 0) {
    fprintf(stderr, "failed to start transfer, %s\n", strerror(result));
    return EXIT_FAILURE;
  }
  do {
    result = dma.transfer_status();
  } while (result == EAGAIN);
  auto end_time = std::chrono::steady_clock::now();
  if (result != 0) {
    uint32_t flags = dma.get_interrupts();
    fprintf(stderr, "failed to finish transfer, %s. %08x\n", strerror(result), flags);
    return EXIT_FAILURE;
  }
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  double bytes_per_seconds = static_cast<double>(std::chrono::nanoseconds(1s).count() * size) / static_cast<double>(elapsed.count());
  fprintf(stdout, "Size %zu Write took: %" PRIi64 " ns %9.5f Mb/s\n", size, elapsed.count(), bytes_per_seconds / (1024.0 * 1024.0));
  return EXIT_SUCCESS;
}

enum class Command {
  RESET,
  READ,
  WRITE,
};

extern int optind;
extern char* optarg;

int main(int argc, char* argv[])
{
  static struct option long_options[] = {
    { "address", required_argument, 0, 'a' },
    { "size", required_argument, 0, 's' },
    { 0, 0, 0, 0 }
  };
  int long_option_index = 0;
  int option_index = 0;
  size_t address = 0;
  size_t size = 0;

  while (1) {
    int c = getopt_long(argc, argv, "a:s:",
      long_options, &long_option_index);
    option_index = optind;
    if (c == -1) {
      break;
    }
    switch (c) {
    case 'a':
      try {
        address = grund::from_string<size_t>(optarg);
      }
      catch (const std::exception&) {
        fprintf(stderr, "Invalid address value\n");
      }
      break;
    case 's':
      try {
        size = grund::from_string<size_t>(optarg);
      }
      catch (const std::exception&) {
        fprintf(stderr, "Invalid size value\n");
      }
      break;
    default:
      break;
    }
  }
  if (address == 0) {
    fprintf(stderr, "No address provided\n");
    return EXIT_FAILURE;
  }
  if (size == 0) {
    fprintf(stderr, "No size provided\n");
    return EXIT_FAILURE;
  }

  int result = EXIT_SUCCESS;
  std::string buffer_name { "dma_buffer" };
  plogik::UDmaBufManager::add_buffer(buffer_name, 64 * 1024);
  {
    auto user_buffer = plogik::UDmaBuf(buffer_name);
    auto uio_description = plogik::UIOMapDescription("gdma0", 0);
    auto dma = plogik::Zdma(uio_description.to_memory_map());

    size_t arguments_length = 0;
    char* const* arguments = nullptr;
    Command command = Command::READ;
    if ((argc - option_index) > 0) {
      std::string command_str { argv[option_index] };
      arguments_length = argc - option_index - 1;
      arguments = &argv[option_index + 1];
      if (command_str == "read") {
        command = Command::READ;
      }
      else if (command_str == "reset") {
        command = Command::RESET;
      }
      else if (command_str == "write") {
        command = Command::WRITE;
      }
      else {
        fprintf(stderr, "Unknown command %s\n", command_str.c_str());
        return EXIT_FAILURE;
      }
    }

    plogik::Zdma::activate_fpd_dma_clk();

    switch (command) {
    case Command::READ:
      result = dma_read(dma, user_buffer, address, size, arguments_length, arguments);
      break;
    case Command::RESET:
      result = dma_reset(dma);
      break;
    case Command::WRITE:
      result = dma_write(dma, user_buffer, address, size, arguments_length, arguments);
      break;
    }
  }

  plogik::UDmaBufManager::remove_buffer(buffer_name);

  return result;
}