#include <algorithm>
#include <cinttypes>
#include <cstdlib>
#include <limits>
extern "C" {
#include <getopt.h>
}
#include <grund/convert.hpp>
#include <grund/time.hpp>
#include <plogik/axi_stream_fifo_mm.hpp>

int fifo_read(plogik::AxiStreamFifoMm& fifo, int argc, char* const* argv)
{
  using namespace std::literals::chrono_literals;
  if (argc < 1) {
    fprintf(stderr, "No address to read\n");
    return EXIT_FAILURE;
  }
  size_t size;
  try {
    size = grund::from_string<size_t>(argv[0]);
  }
  catch (const std::invalid_argument&) {
    size = std::numeric_limits<size_t>::max();
  }
  if (std::numeric_limits<size_t>::max() == size) {
    fprintf(stderr, "Invalid size to read\n");
    return EXIT_FAILURE;
  }
  std::vector<uint64_t> data(size, 0);
  auto slice = std::span { data };
  uint8_t destination;
  auto start_time = std::chrono::steady_clock::now();
  size_t length = fifo.read(destination, slice);
  auto end_time = std::chrono::steady_clock::now();
  if (length == 0) {
    fprintf(stderr, "Failed to read FIFO\n");
    return EXIT_FAILURE;
  }
  fprintf(stdout, "Destination: %01x Size %zu\n", destination, length);
  size_t part = std::min(length, 16ul);
  for (uint64_t value : std::span { data.begin(), part }) {
    fprintf(stdout, "%016lx\n", value);
  }
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  double bytes_per_seconds = static_cast<double>(std::chrono::nanoseconds(1s).count() * size) / static_cast<double>(elapsed.count());
  fprintf(stdout, "Size %zu Read took: %" PRIi64 " ns %9.5f Mb/s\n", size, elapsed.count(), bytes_per_seconds / (1024.0 * 1024.0));
  return EXIT_SUCCESS;
}

int fifo_write(plogik::AxiStreamFifoMm& fifo, int argc, char* const* argv)
{
  using namespace std::literals::chrono_literals;
  if (argc < 2) {
    fprintf(stderr, "No address to write\n");
    return EXIT_FAILURE;
  }
  size_t size;
  uint64_t value;
  try {
    size = grund::from_string<size_t>(argv[0]);
    value = grund::from_string<uint64_t>(argv[1]);
  }
  catch (const std::invalid_argument&) {
    size = std::numeric_limits<size_t>::max();
  }
  if (std::numeric_limits<size_t>::max() == size) {
    fprintf(stderr, "Invalid address or value to write\n");
    return EXIT_FAILURE;
  }
  std::vector<uint64_t> data(size, value);
  for (size_t n = 0; n < size; n++) {
    data[n] = value + static_cast<uint64_t>(n);
  }
  auto start_time = std::chrono::steady_clock::now();
  size_t length = fifo.write(2, std::span { data });
  auto end_time = std::chrono::steady_clock::now();
  if (length == 0) {
    fprintf(stderr, "Failed to write FIFO\n");
    return EXIT_FAILURE;
  }
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  double bytes_per_seconds = static_cast<double>(std::chrono::nanoseconds(1s).count() * size) / static_cast<double>(elapsed.count());
  fprintf(stdout, "Size %zu Write took: %" PRIi64 " ns %9.5f Mb/s\n", size, elapsed.count(), bytes_per_seconds / (1024.0 * 1024.0));
  return EXIT_SUCCESS;
}

enum class Command {
  READ,
  RESET,
  WRITE,
};

extern int optind;
extern char* optarg;

int main(int argc, char* argv[])
{
  static struct option long_options[] = {
    { 0, 0, 0, 0 }
  };
  int long_option_index = 0;
  int option_index = 0;

  while (1) {
    int c = getopt_long(argc, argv, "",
      long_options, &long_option_index);
    option_index = optind;
    if (c == -1) {
      break;
    }
    switch (c) {
    default:
      break;
    }
  }

  size_t arguments_length = 0;
  char* const* arguments = nullptr;
  Command command = Command::READ;
  if ((argc - option_index) > 0) {
    std::string command_str { argv[option_index] };
    arguments_length = argc - option_index - 1;
    arguments = &argv[option_index + 1];
    if (command_str == "read") {
      command = Command::READ;
    }
    else if (command_str == "write") {
      command = Command::WRITE;
    }
    else if (command_str == "reset") {
      command = Command::RESET;
    }
    else {
      fprintf(stderr, "Unknown command %s\n", command_str.c_str());
      return EXIT_FAILURE;
    }
  }

  int result = EXIT_SUCCESS;
  switch (command) {
  case Command::READ:
    {
      auto fifo = plogik::AxiStreamFifoMm(0xa0010000, 0xa0030000, 64);
      result = fifo_read(fifo, arguments_length, arguments);
    }
    break;
  case Command::RESET:
    {
      auto rd_fifo = plogik::AxiStreamFifoMm(0xa0010000, 0xa0030000, 64);
      auto wr_fifo = plogik::AxiStreamFifoMm(0xa0000000, 0xa0020000, 64);
      rd_fifo.reset();
      wr_fifo.reset();
    }
    break;
  case Command::WRITE:
    {
      auto fifo = plogik::AxiStreamFifoMm(0xa0000000, 0xa0020000, 64);
      result = fifo_write(fifo, arguments_length, arguments);
    }
    break;
  }

  return result;
}