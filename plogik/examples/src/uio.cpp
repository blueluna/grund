#include <cstdlib>
#include <limits>
extern "C" {
#include <getopt.h>
}
#include <grund/convert.hpp>
#include <plogik/uio_device.hpp>

void list_maps_uio(plogik::UIODescription& uio)
{
  fprintf(stdout, "uio%d %s\n", uio.uio(), uio.name().c_str());
  for (plogik::UIOMapDescription& map : uio.maps()) {
    fprintf(stdout, "\tmap%d 0x%016zx:0x%016zx +0x%08zx %s\n", map.map(), map.address(), map.size(), map.offset(), map.name().c_str());
  }
}

int list_uio()
{
  std::vector<plogik::UIODescription> devices = plogik::UIODescription::enumerate();
  for (plogik::UIODescription& uio : devices) {
    list_maps_uio(uio);
  }
  return EXIT_SUCCESS;
}

int list_maps(uint16_t n)
{
  if (n == std::numeric_limits<uint16_t>::max()) {
    fprintf(stderr, "No uio number provided\n");
    return EXIT_FAILURE;
  }
  try {
    plogik::UIODescription uio(n);
    list_maps_uio(uio);
  }
  catch (const std::exception& e) {
    fprintf(stderr, "Failed to list maps, %s\n", e.what());
  }
  return EXIT_SUCCESS;
}

int uio_read(uint16_t uio, uint16_t map, int argc, char* const* argv)
{
  if (argc < 1) {
    fprintf(stderr, "No address to read\n");
    return EXIT_FAILURE;
  }
  size_t address;
  try {
    address = grund::from_string<size_t>(argv[0]);
  }
  catch (const std::invalid_argument&) {
    address = std::numeric_limits<size_t>::max();
  }
  if (std::numeric_limits<size_t>::max() == address) {
    fprintf(stderr, "Invalid address to read\n");
    return EXIT_FAILURE;
  }
  uint32_t value = 0;
  grund::MemoryMap mmap;
  try {
    plogik::UIOMapDescription description(uio, map);
    mmap = description.to_memory_map();
  }
  catch (const std::exception& e) {
    fprintf(stderr, "Failed to memory map UIO, %s\n", e.what());
  }
  fprintf(stdout, "%016zx: ?\n", address);
  value = *reinterpret_cast<const uint32_t*>(mmap[address]);
  fprintf(stdout, "%016zx: %08x\n", address, value);
  return EXIT_SUCCESS;
}

int uio_write(uint16_t uio, uint16_t map, int argc, char* const* argv)
{
  if (argc < 2) {
    fprintf(stderr, "No address to read\n");
    return EXIT_FAILURE;
  }
  size_t address;
  uint32_t value;
  try {
    address = grund::from_string<size_t>(argv[0]);
    value = grund::from_string<uint32_t>(argv[1]);
  }
  catch (const std::invalid_argument&) {
    address = std::numeric_limits<size_t>::max();
  }
  if (std::numeric_limits<size_t>::max() == address) {
    fprintf(stderr, "Invalid address or value to write\n");
    return EXIT_FAILURE;
  }
  grund::MemoryMap mmap;
  try {
    plogik::UIOMapDescription description(uio, map);
    mmap = description.to_memory_map();
  }
  catch (const std::exception& e) {
    fprintf(stderr, "Failed to memory map UIO, %s\n", e.what());
  }
  uint32_t* ptr = mmap.offset_bytes_mut<uint32_t>(address);
  uint32_t old_value = *ptr;
  *ptr = value;
  fprintf(stderr, "%016zx: %08x -> %08x\n", address, old_value, value);
  return EXIT_SUCCESS;
}

enum class Command {
  LIST_UIO,
  LIST_MAPS,
  READ,
  WRITE,
};

extern int optind;
extern char* optarg;

int main(int argc, char* argv[])
{
  static struct option long_options[] = {
    { "uio", required_argument, 0, 'u' },
    { "map", required_argument, 0, 'm' },
    { 0, 0, 0, 0 }
  };
  int long_option_index = 0;
  int option_index = 0;
  uint16_t uio = std::numeric_limits<uint16_t>::max();
  uint16_t map = 0;

  while (1) {
    int c = getopt_long(argc, argv, "m:u:",
      long_options, &long_option_index);
    option_index = optind;
    if (c == -1) {
      break;
    }
    switch (c) {
    case 'm':
      try {
        map = grund::from_string<uint16_t>(optarg);
      }
      catch (const std::exception&) {
        fprintf(stderr, "Invalid map value\n");
      }
      break;
    case 'u':
      try {
        uio = grund::from_string<uint16_t>(optarg);
      }
      catch (const std::exception&) {
        fprintf(stderr, "Invalid uio value\n");
      }
      break;
    default:
      break;
    }
  }

  size_t arguments_length = 0;
  char* const* arguments = nullptr;
  Command command = Command::LIST_UIO;
  if ((argc - option_index) > 0) {
    std::string command_str { argv[option_index] };
    arguments_length = argc - option_index - 1;
    arguments = &argv[option_index + 1];
    if (command_str == "read") {
      command = Command::READ;
    }
    else if (command_str == "write") {
      command = Command::WRITE;
    }
    else if (command_str == "uio_list") {
      command = Command::LIST_UIO;
    }
    else if (command_str == "map_list") {
      command = Command::LIST_MAPS;
    }
    else {
      fprintf(stderr, "Unknown command %s\n", command_str.c_str());
      return EXIT_FAILURE;
    }
  }

  int result = EXIT_SUCCESS;
  switch (command) {
  case Command::LIST_UIO:
    result = list_uio();
    break;
  case Command::LIST_MAPS:
    result = list_maps(uio);
    break;
  case Command::READ:
    result = uio_read(uio, map, arguments_length, arguments);
    break;
  case Command::WRITE:
    result = uio_write(uio, map, arguments_length, arguments);
    break;
  }

  return result;
}