#include <algorithm>
#include <cinttypes>
#include <cstdlib>
#include <limits>
extern "C" {
#include <getopt.h>
}
#include <grund/convert.hpp>
#include <grund/time.hpp>
#include <plogik/axi_stream_fifo_mm.hpp>
#include <plogik/uio_device.hpp>

int fifo_read(plogik::AxiStreamFifoMm& fifo, plogik::UIODescription& uio_description, bool interrupt, int argc, char* const* argv)
{
  using namespace std::literals::chrono_literals;
  if (argc < 1) {
    fprintf(stderr, "No address to read\n");
    return EXIT_FAILURE;
  }
  size_t size;
  try {
    size = grund::from_string<size_t>(argv[0]);
  }
  catch (const std::invalid_argument&) {
    size = std::numeric_limits<size_t>::max();
  }
  if (std::numeric_limits<size_t>::max() == size) {
    fprintf(stderr, "Invalid size to read\n");
    return EXIT_FAILURE;
  }
  if (interrupt) {
    fifo.read_interrupt_enable();
    plogik::UIODeviceFile dev(uio_description.uio());
    dev.interrupt_arm();
    dev.interrupt_wait();
  }
  std::vector<uint64_t> data(size, 0);
  auto slice = std::span { data };
  uint8_t destination;
  auto start_time = std::chrono::steady_clock::now();
  size_t length = fifo.read(destination, slice);
  auto end_time = std::chrono::steady_clock::now();
  if (length == 0) {
    fprintf(stderr, "Failed to read FIFO\n");
    return EXIT_FAILURE;
  }
  size_t words = length / sizeof(uint64_t);
  fprintf(stdout, "Destination: %01x Size %zu\n", destination, words);
  size_t part = std::min(words, 16ul);
  for (uint64_t value : std::span { data.begin(), part }) {
    fprintf(stdout, "%016lx\n", value);
  }
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  double bytes_per_seconds = static_cast<double>(std::chrono::nanoseconds(1s).count() * length) / static_cast<double>(elapsed.count());
  fprintf(stdout, "Bytes %zu Read took: %" PRIi64 " ns %9.5f Mb/s\n", length, elapsed.count(), bytes_per_seconds / (1024.0 * 1024.0));
  return EXIT_SUCCESS;
}

int fifo_write(plogik::AxiStreamFifoMm& fifo, int argc, char* const* argv)
{
  using namespace std::literals::chrono_literals;
  if (argc < 2) {
    fprintf(stderr, "No address to read\n");
    return EXIT_FAILURE;
  }
  size_t size;
  uint64_t value;
  try {
    size = grund::from_string<size_t>(argv[0]);
    value = grund::from_string<uint64_t>(argv[1]);
  }
  catch (const std::invalid_argument&) {
    size = std::numeric_limits<size_t>::max();
  }
  if (std::numeric_limits<size_t>::max() == size) {
    fprintf(stderr, "Invalid address or value to write\n");
    return EXIT_FAILURE;
  }
  std::vector<uint64_t> data(size, value);
  for (size_t n = 0; n < size; n++) {
    data[n] = value + static_cast<uint64_t>(n);
  }
  auto start_time = std::chrono::steady_clock::now();
  size_t length = fifo.write(2, std::span { data });
  auto end_time = std::chrono::steady_clock::now();
  if (length == 0) {
    fprintf(stderr, "Failed to write FIFO\n");
    return EXIT_FAILURE;
  }
  size_t bytes = size * sizeof(uint64_t);
  auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  double bytes_per_seconds = static_cast<double>(std::chrono::nanoseconds(1s).count() * bytes) / static_cast<double>(elapsed.count());
  fprintf(stdout, "Bytes %zu Write took: %" PRIi64 " ns %9.5f Mb/s\n", bytes, elapsed.count(), bytes_per_seconds / (1024.0 * 1024.0));
  return EXIT_SUCCESS;
}

enum class Command {
  READ,
  RESET,
  WRITE,
};

extern int optind;
extern char* optarg;

int main(int argc, char* argv[])
{
  static struct option long_options[] = {
    { "uio", required_argument, 0, 'u' },
    { "interrupt", no_argument, 0, 'i' },
    { 0, 0, 0, 0 }
  };
  int long_option_index = 0;
  int option_index = 0;
  uint16_t uio_number = std::numeric_limits<uint16_t>::max();
  std::string uio_name;
  bool interrupt = false;

  while (1) {
    int c = getopt_long(argc, argv, "iu:",
      long_options, &long_option_index);
    option_index = optind;
    if (c == -1) {
      break;
    }
    switch (c) {
    case 'i':
      interrupt = true;
      break;
    case 'u':
      try {
        uio_number = grund::from_string<uint16_t>(optarg);
      }
      catch (const std::exception&) {
        uio_name.assign(optarg);
      }
      break;
    default:
      break;
    }
  }

  size_t arguments_length = 0;
  char* const* arguments = nullptr;
  Command command = Command::READ;
  if ((argc - option_index) > 0) {
    std::string command_str { argv[option_index] };
    arguments_length = argc - option_index - 1;
    arguments = &argv[option_index + 1];
    if (command_str == "read") {
      command = Command::READ;
    }
    else if (command_str == "write") {
      command = Command::WRITE;
    }
    else if (command_str == "reset") {
      command = Command::RESET;
    }
    else {
      fprintf(stderr, "Unknown command %s\n", command_str.c_str());
      return EXIT_FAILURE;
    }
  }

  plogik::UIODescription uio_description;
  try {
    if (uio_number < std::numeric_limits<uint16_t>::max()) {
      uio_description = plogik::UIODescription(uio_number);
    }
    else if (!uio_name.empty()) {
      uio_description = plogik::UIODescription(uio_name);
    }
    else {
      fprintf(stderr, "No UIO device provided\n");
      return EXIT_FAILURE;
    }
  }
  catch (const std::exception& e) {
    fprintf(stderr, "Failed to enumerate uio, %s\n", e.what());
    return EXIT_FAILURE;
  }
  auto fifo = plogik::AxiStreamFifoMm(uio_description, 64);

  int result = EXIT_SUCCESS;
  switch (command) {
  case Command::READ:
    result = fifo_read(fifo, uio_description, interrupt, arguments_length, arguments);
    break;
  case Command::RESET:
    fifo.reset();
    break;
  case Command::WRITE:
    result = fifo_write(fifo, arguments_length, arguments);
    break;
  }

  return result;
}