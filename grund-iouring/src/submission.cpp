#include <grund/iouring/submission.hpp>

using namespace grund::iouring;

Submission::Submission(struct io_uring_sqe* entry)
  : _submission_entry(entry)
{
}

Submission::~Submission()
{
  _submission_entry.release();
}

void Submission::register_completion(Completion& completion)
{
  io_uring_sqe_set_data(_submission_entry.get(), &completion);
}

void Submission::prepare_readv(Completion& completion, const grund::FileDescriptor& fd, const struct iovec& iovec)
{
  io_uring_prep_readv(_submission_entry.get(), fd.fd(), &iovec, 1, 0);
  register_completion(completion);
}