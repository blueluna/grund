
#include <grund/iouring/completion.hpp>
#include <grund/iouring/executor.hpp>

#include <cstring>

using namespace grund::iouring;

Executor::Executor()
  : Executor(32)
{
}

Executor::Executor(int entries)
{
  io_uring_queue_init(entries, &_io_uring, 0);
}

Submission Executor::get_submission()
{
  return Submission(io_uring_get_sqe(&_io_uring));
}

int Executor::submit()
{
  return io_uring_submit(&_io_uring);
}

int Executor::run_one()
{
  struct io_uring_cqe* cqe = NULL;
  int ret = io_uring_wait_cqe(&_io_uring, &cqe);
  if (ret < 0) {
    printf("Executor exit %d, %s\r\n", -ret, strerror(-ret));
    return ret;
  }
  auto completion = reinterpret_cast<Completion*>(io_uring_cqe_get_data(cqe));
  if (completion != NULL) {
    completion->on_completion(*this, cqe->res);
  }
  /* Mark this completion as seen */
  io_uring_cqe_seen(&_io_uring, cqe);
  // Submit any new submissions
  submit();
  return 0;
}
