#ifndef GRUND_IOURING_UDP_MULTICAST_HPP
#define GRUND_IOURING_UDP_MULTICAST_HPP

#include <grund/udp_multicast.hpp>
#include <liburing.h>

namespace grund::iouring {

class UdpMulticast : public grund::UdpMulticast {
public:
  explicit UdpMulticast(sa_family_t family, bool non_blocking)
    : grund::UdpMulticast(family, non_blocking)
  {
  }
  ~UdpMulticast() override = default;
};

}

#endif // GRUND_IOURING_UDP_MULTICAST_HPP
