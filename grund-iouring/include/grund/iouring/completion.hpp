#ifndef GRUND_IOURING_COMPLETION_HPP
#define GRUND_IOURING_COMPLETION_HPP

namespace grund::iouring {

class Executor;

class Completion {
public:
  virtual void on_completion(Executor& executor, int result_code) = 0;
};

}

#endif // GRUND_IOURING_COMPLETION_HPP
