#ifndef GRUND_IOURING_SIGNAL_FD_HPP
#define GRUND_IOURING_SIGNAL_FD_HPP

#include "completion.hpp"
#include "executor.hpp"
#include <grund/signal_fd.hpp>
#include <grund/system_error.hpp>
#include <liburing.h>
extern "C" {
#include <poll.h>
#include <signal.h>
#include <sys/signalfd.h>
}

namespace grund::iouring {
class SignalFd : public Completion {
private:
  grund::SignalFd _signal_fd;
  struct signalfd_siginfo _signal_information;
  struct iovec _buffer;

public:
  SignalFd(Executor& executor, int signals)
    : _signal_fd(signals, false)
  {
    auto submission = executor.get_submission();
    _buffer.iov_base = &_signal_information;
    _buffer.iov_len = sizeof(struct signalfd_siginfo);
    submission.prepare_readv(*this, _signal_fd, _buffer);
  }

  virtual void signal(int signal_identifier) = 0;

  void on_completion(Executor& executor, int result_code) override
  {
    (void)executor;
    if (result_code == sizeof(struct signalfd_siginfo)) {
      signal(_signal_information.ssi_signo);
    }
    else if (result_code < 0) {
      fprintf(stderr, "Signal completion %d, %s\r\n", -result_code, strerror(-result_code));
    }
  }
};
}

#endif // GRUND_IOURING_SIGNAL_FD_HPP
