#ifndef GRUND_IOURING_TIMER_FD_HPP
#define GRUND_IOURING_TIMER_FD_HPP

#include "completion.hpp"
#include "executor.hpp"
#include <grund/timer_fd.hpp>
#include <liburing.h>
extern "C" {
#include <sys/uio.h>
}

namespace grund::iouring {

class TimerFd : public Completion {
private:
  grund::TimerFd _timer_fd;
  uint64_t _expirations;
  struct iovec _buffer;

protected:
  void submit_read(Executor& executor)
  {
    auto submission = executor.get_submission();
    _buffer.iov_base = &_expirations;
    _buffer.iov_len = sizeof(_expirations);
    submission.prepare_readv(*this, _timer_fd, _buffer);
  }

public:
  TimerFd()
    : _timer_fd(false)
    , _expirations(0)
  {
  }

  void arm(Executor& executor, const std::chrono::nanoseconds duration, bool periodic = false)
  {
    _timer_fd.arm(duration, periodic);
    submit_read(executor);
  }

  virtual void timer(uint64_t expirations) = 0;

  void on_completion(Executor& executor, int result_code) override
  {
    (void)executor;
    if (result_code == sizeof(_expirations)) {
      timer(_expirations);
    }
    else if (result_code < 0) {
      fprintf(stderr, "Timer completion %d, %s\r\n", -result_code, strerror(-result_code));
    }
    submit_read(executor);
  }
};
}

#endif // GRUND_IOURING_TIMER_FD_HPP
