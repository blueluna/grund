#ifndef GRUND_IOURING_SUBMISSION_HPP
#define GRUND_IOURING_SUBMISSION_HPP

#include <array>
#include <grund/file_descriptor.hpp>
#include <grund/iouring/completion.hpp>
#include <liburing.h>
#include <memory>

namespace grund::iouring {

class Submission {
protected:
  std::unique_ptr<struct io_uring_sqe> _submission_entry;

public:
  explicit Submission(struct io_uring_sqe* entry);
  virtual ~Submission();

  void register_completion(Completion& completion);

  void prepare_readv(Completion& completion, const grund::FileDescriptor& fd, const struct iovec& iovec);
};

}

#endif // GRUND_IOURING_SUBMISSION_HPP
