
#ifndef GRUND_IOURING_EXECUTOR_HPP
#define GRUND_IOURING_EXECUTOR_HPP

#include "completion.hpp"
#include "submission.hpp"
#include <functional>
#include <liburing.h>
#include <memory>

namespace grund::iouring {

class Executor {
private:
  struct io_uring _io_uring;

public:
  explicit Executor();
  explicit Executor(int entries);

  Submission get_submission();
  // Submit pending submission entries, returns number of entries submitted
  int submit();

  int run_one();
};

}

#endif // GRUND_IOURING_EXECUTOR_HPP
