# Docker Build

## Prepare docker image

```shell
docker build -t grund-dev-debian:stable -f docker/debian-stable --build-arg .
```

## Build in docker

Enter the docker container in interactive mode,
```shell
docker run -it --init --rm --name=grund --user 1000:1000 --workdir=/src --mount type=bind,source=${PWD},target=/src grund-dev-debian:stable
```

Build as usual
```shell
cmake -S . -B build-debian-stable-aarch64 -DUSE_IO_URING=OFF -DCMAKE_TOOLCHAIN_FILE=toolchain-aarch64-gcc.cmake
cmake --build build-debian-stable-aarch64
```
