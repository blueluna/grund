#include <grund/netlink/message.hpp>

namespace grund {
namespace netlink {

  std::size_t Header::aligned_length() const
  {
    return align_to(length);
  }

  std::size_t Header::data_length() const
  {
    return length - HEADER_SIZE;
  }

}
}