#include <fmt/core.h>
#include <grund/netlink/socket.hpp>
#include <grund/system_error.hpp>

namespace grund::netlink {

Socket::Socket(grund::netlink::Protocol protocol, uint32_t groups, bool non_blocking)
  : _socket(AF_NETLINK, grund::Socket::Type::Raw, non_blocking, protocol)
  , _sequence_next(1)
{
  _socket.set_option(SOL_SOCKET, SO_SNDBUF, 32768);
  _socket.set_option(SOL_SOCKET, SO_RCVBUF, 32768);
  SocketAddress local = SocketAddress::netlink(0, groups);
  _peer = SocketAddress::netlink(0, groups);
  _socket.bind(local);
  local = _socket.get_address();
  fmt::print("local: {}\n", local.as_string());
  _process_identifier = reinterpret_cast<const sockaddr_nl*>(&local)->nl_pid;
}

void Socket::request(uint16_t command, const std::initializer_list<const uint8_t>& data, bool acknowledge, bool dump)
{
  return request(command, std::span<const uint8_t>(data.begin(), data.size()), acknowledge, dump);
}

void Socket::request(uint16_t command, const std::span<const uint8_t>& data, bool acknowledge, bool dump)
{
  uint16_t flags = F_REQUEST | (acknowledge ? F_ACKNOWLEDGE : 0) | (dump ? F_DUMP : 0);
  auto header = Header(command, flags, _sequence_next, _process_identifier, data.size());
  std::memcpy(_send_buffer.data(), &header, sizeof(Header));
  std::copy_n(data.begin(), data.size(), _send_buffer.begin() + HEADER_SIZE);
  int result = ::send(_socket.fd(), _send_buffer.data(), header.length, 0);
  if (result < 0) {
    throw SystemError();
  }
  fmt::print("Send: pid {} seq {} cmd {:08x} flags {:04x} - {}\n", _process_identifier, _sequence_next, command, flags, result);
  _sequence_next += 1;
}

std::span<uint8_t> Socket::receive_bytes()
{
  struct iovec iov {
    _receive_buffer.data(), _receive_buffer.size()
  };
  struct msghdr message_header { };
  message_header.msg_name = reinterpret_cast<sockaddr_nl*>(&_peer);
  message_header.msg_namelen = _peer.socklen();
  message_header.msg_flags = 0;
  message_header.msg_control = nullptr;
  message_header.msg_controllen = 0;
  message_header.msg_iov = &iov;
  message_header.msg_iovlen = 1;
  int result = recvmsg(_socket.fd(), &message_header, 0);
  if (result < 0) {
    throw SystemError();
  }
  else if (result == 0) {
    return std::span<uint8_t> { _receive_buffer.begin(), 0 };
  }
  fmt::print("Receive: {} bytes\n", result);
  return std::span<uint8_t>(_receive_buffer.begin(), static_cast<size_t>(result));
}

std::vector<Message> Socket::receive()
{
  std::span<uint8_t> bytes = receive_bytes();
  std::size_t left = bytes.size();
  std::vector<Message> messages;
  while (left > 0) {
    auto part = bytes.last(left);
    auto maybe_header = Header::from_bytes(part);
    if (!maybe_header.has_value()) {
      break;
    }
    Header header = maybe_header.value();
    left -= header.aligned_length();
    messages.push_back({ header, part.subspan(HEADER_SIZE, header.data_length()) });
  }
  return messages;
}

void Socket::close()
{
  _socket.close();
}

}