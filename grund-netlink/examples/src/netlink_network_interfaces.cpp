#include <csignal>
#include <cstring>
#include <fmt/core.h>
#include <fmt/ranges.h>
#include <grund/epoll/eventloop.hpp>
#include <grund/netlink/route/message.hpp>
#include <grund/netlink/socket.hpp>
#include <linux/rtnetlink.h>
#include <string_view>

static volatile sig_atomic_t running = 1;

static void signal_handler(int signal, siginfo_t* info, void* data)
{
  (void)signal;
  (void)info;
  (void)data;
  running = 0;
}

int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;
  struct sigaction sig;
  std::memset(&sig, 0, sizeof(sig));
  sig.sa_sigaction = signal_handler;
  sigfillset(&sig.sa_mask);
  sig.sa_flags = SA_SIGINFO;
  sigaction(SIGINT, &sig, nullptr);

  grund::epoll::EventLoop event_loop;

  auto nl = grund::netlink::Socket(grund::netlink::Protocol::ROUTE, RTMGRP_LINK, true);

  std::array<uint8_t, 128> buffer;
  auto interface_information = grund::netlink::route::InterfaceInformation { AF_UNSPEC, 0, 0, 0 };
  std::size_t used = interface_information.to_bytes(buffer);

  // data is struct ifinfomsg
  nl.request(RTM_GETLINK, std::span { buffer.begin(), used }, false, true);

  auto nl_event = grund::epoll::Event(event_loop, nl.fd(), grund::epoll::Event::READ, [&nl](int fd, uint32_t events) {
    (void)fd;
    (void)events;
    auto messages = nl.receive();
    for (auto& message : messages) {
      switch (message.header.type) {
      case grund::netlink::OP_NONE:
        fmt::print("Receive: NONE pid {} seq {} cmd {:08x} flags {:04x} data length {}\n", message.header.pid, message.header.sequence, message.header.type, message.header.flags, message.header.data_length());
        break;
      case grund::netlink::OP_ERROR:
        {
          auto error = grund::netlink::Error::from_bytes(message.header, message.data());
          if (error.value().is_acknowledge()) {
            fmt::print("Receive: ACK pid {} seq {} cmd {:08x} flags {:04x} data length {}\n", message.header.pid, message.header.sequence, message.header.type, message.header.flags, message.header.data_length());
          }
          else {
            fmt::print("Receive: ERROR {} pid {} seq {} cmd {:08x} flags {:04x} data length {}\n", strerror(-error.value().error_code), message.header.pid, message.header.sequence, message.header.type, message.header.flags, message.header.data_length());
          }
        };
        break;
      case grund::netlink::OP_DONE:
        fmt::print("Receive: DONE pid {} seq {} cmd {:08x} flags {:04x} data length {}\n", message.header.pid, message.header.sequence, message.header.type, message.header.flags, message.header.data_length());
        break;
      case grund::netlink::OP_DATA_LOST:
        fmt::print("Receive: LOST pid {} seq {} cmd {:08x} flags {:04x} data length {}\n", message.header.pid, message.header.sequence, message.header.type, message.header.flags, message.header.data_length());
        break;
      default:
        fmt::print("Receive: pid {} seq {} cmd {:08x} flags {:04x} data length {}\n", message.header.pid, message.header.sequence, message.header.type, message.header.flags, message.header.data_length());
        {
          switch (message.header.type) {
          case RTM_GETLINK:
          case RTM_NEWLINK:;
          case RTM_DELLINK:
            {
              using namespace grund::netlink::route;
              std::string op;
              switch (message.header.type) {
              case RTM_GETLINK:
                op.assign("Get");
                break;
              case RTM_NEWLINK:
                op.assign("New");
                break;
              case RTM_DELLINK:
                op.assign("Del");
                break;
              }
              auto ifinfo = InterfaceInformation::from_bytes(message.data());
              if (ifinfo.has_value()) {
                auto info = ifinfo.value();
                auto left = message.mut_data().subspan(info.SIZE);
                std::size_t used = 0;
                std::size_t offset = 0;
                fmt::print("{} Link family {} type {} index {} flags ({}) size: {}\n", op, info.family, InterfaceInformation::type_label(info.type), info.index, InterfaceInformation::flags_label(info.flags), left.size());
                do {
                  auto result = grund::netlink::RawAttribute::from_bytes(left.subspan(offset));
                  used = std::get<0>(result);
                  auto maybe_attribute = std::get<1>(result);
                  if (maybe_attribute.has_value()) {
                    auto attribute = maybe_attribute.value();
                    fmt::print("  {}:", attribute_label(attribute.type()));
                    auto identifier = static_cast<AttributeIdentifier>(attribute.type());
                    switch (identifier) {
                    case AttributeIdentifier::IFNAME:
                    case AttributeIdentifier::PARENT_DEV_NAME:
                    case AttributeIdentifier::PARENT_DEV_BUS_NAME:
                    case AttributeIdentifier::QUEUEING_DISCIPLINE:
                      {
                        fmt::print(" {}\n", attribute.as_string_view());
                      }
                      break;
                    case AttributeIdentifier::CARRIER:
                    case AttributeIdentifier::PROTO_DOWN:
                      {
                        fmt::print(" {}\n", attribute.as_bool());
                      }
                      break;
                    case AttributeIdentifier::OPERSTATE:
                      {
                        fmt::print(" {}\n", operation_state_label(attribute.as_u8()));
                      }
                      break;
                    case AttributeIdentifier::LINKMODE:
                      {
                        fmt::print(" {}\n", link_mode_label(attribute.as_u8()));
                      }
                      break;
                    default:
                      if (attribute.data.size() == 1) {
                        fmt::print(" {:02x} {}\n", attribute.as_u8(), attribute.as_u8());
                      }
                      else if (attribute.data.size() == 2) {
                        fmt::print(" {:04x} {}\n", attribute.as_u16(), attribute.as_u16());
                      }
                      else if (attribute.data.size() == 4) {
                        fmt::print(" {:08x} {}\n", attribute.as_u32(), attribute.as_u32());
                      }
                      else if (attribute.data.size() == 6) {
                        auto value = attribute.data.data();
                        fmt::print(" {:02x}:{:02x}:{:02x}:{:02x}:{:02x}:{:02x}\n", value[0], value[1], value[2], value[3], value[4], value[5]);
                      }
                      else {
                        fmt::print(" {} bytes\n", attribute.data.size());
                      }
                      break;
                    }
                  }
                  offset += used;
                } while (used > 0);
              }
            }
            break;
          default:
            break;
          }
        }
      }
    }
  });

  do {
    event_loop.run_once(500);
  } while (running);

  nl.close();
}