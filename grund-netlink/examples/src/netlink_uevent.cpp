#include <csignal>
#include <cstring>
#include <fmt/core.h>
#include <grund/epoll/eventloop.hpp>
#include <grund/netlink/socket.hpp>
#include <string_view>

static volatile sig_atomic_t running = 1;

static void signal_handler(int signal, siginfo_t* info, void* data)
{
  (void)signal;
  (void)info;
  (void)data;
  running = 0;
}

int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;
  struct sigaction sig;
  std::memset(&sig, 0, sizeof(sig));
  sig.sa_sigaction = signal_handler;
  sigfillset(&sig.sa_mask);
  sig.sa_flags = SA_SIGINFO;
  sigaction(SIGINT, &sig, nullptr);

  grund::epoll::EventLoop event_loop;

  auto nl = grund::netlink::Socket(grund::netlink::Protocol::KOBJECT_UEVENT, 2, true);

  auto nl_event = grund::epoll::Event(event_loop, nl.fd(), grund::epoll::Event::READ, [&nl](int fd, uint32_t events) {
    (void)fd;
    (void)events;
    auto event = nl.receive_bytes();
    constexpr std::size_t LIBUDEV_HEADER_LENGTH { 40 };
    ;
    auto event_text = std::string_view(reinterpret_cast<const char*>(event.data()), event.size());
    if (event_text.starts_with("libudev\0")) {
      fmt::print("{}\n", event_text.substr(LIBUDEV_HEADER_LENGTH));
    }
  });

  do {
    event_loop.run_once(500);
  } while (running);

  nl.close();
}