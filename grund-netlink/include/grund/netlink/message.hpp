#include <array>
#include <cstdint>
#include <cstring>
#include <grund/bytes.hpp>
#include <memory>
#include <optional>
#include <span>
#include <stdexcept>
#include <string_view>
#include <tuple>
#include <utility>

#ifndef GRUND_NETLINK_MESSAGE_HPP
#define GRUND_NETLINK_MESSAGE_HPP

namespace grund {
namespace netlink {

  /// Message flags
  constexpr uint16_t F_REQUEST { 1 << 0 };
  constexpr uint16_t F_MULTI_PART { 1 << 1 };
  constexpr uint16_t F_ACKNOWLEDGE { 1 << 2 };
  constexpr uint16_t F_ROOT { 1 << 8 };
  constexpr uint16_t F_MATCH { 1 << 9 };
  constexpr uint16_t F_DUMP { F_ROOT | F_MATCH };

  /// Message operation
  /// no-op
  constexpr uint16_t OP_NONE { 1 };
  /// Error message
  constexpr uint16_t OP_ERROR { 2 };
  /// Multi-part message done
  constexpr uint16_t OP_DONE { 3 };
  /// Data was lost
  constexpr uint16_t OP_DATA_LOST { 4 };

  constexpr std::size_t ALIGNMENT { 4 };
  constexpr std::size_t align_to(std::size_t length) { return ((length) + ALIGNMENT - 1) & ~(ALIGNMENT - 1); }

  class Header {
  public:
    static constexpr std::size_t SIZE { 16 };

    explicit constexpr Header(uint16_t type, uint16_t flags, uint32_t sequence, uint32_t pid, std::size_t data_length)
      : Header(SIZE + static_cast<uint32_t>(data_length), type, flags, sequence, pid)
    {
    }

    static constexpr std::optional<Header> from_bytes(const std::span<const uint8_t>& data)
    {
      if (data.size() < align_to(sizeof(Header))) {
        return {};
      }
      auto length = *reinterpret_cast<const uint32_t*>(data.data());
      auto type = *reinterpret_cast<const uint16_t*>(data.data() + sizeof(length));
      auto flags = *reinterpret_cast<const uint16_t*>(data.data() + sizeof(length) + sizeof(type));
      auto sequence = *reinterpret_cast<const uint32_t*>(data.data() + sizeof(length) + sizeof(type) + sizeof(flags));
      auto pid = *reinterpret_cast<const uint32_t*>(data.data() + sizeof(length) + sizeof(type) + sizeof(flags) + sizeof(sequence));
      return Header { length, type, flags, sequence, pid };
    }

    std::size_t aligned_length() const;

    std::size_t data_length() const;

  protected:
    explicit constexpr Header(uint32_t length, uint16_t type, uint16_t flags, uint32_t sequence, uint32_t pid)
      : length(length)
      , type(type)
      , flags(flags)
      , sequence(sequence)
      , pid(pid)
    {
    }

  public:
    /// Length including header
    uint32_t length;
    /// Message type, (command / family / ...)
    uint16_t type;
    /// Message flags, see above
    uint16_t flags;
    /// Message sequence number
    uint32_t sequence;
    /// Message process identifier
    uint32_t pid;
  };

  constexpr std::size_t HEADER_SIZE { align_to(Header::SIZE) };
  constexpr std::size_t MESSAGE_BUFFER_SIZE { 4096 };

  class Message {
  public:
    Message(Header header, std::span<const uint8_t> data)
      : header(header)
      , payload(std::make_unique<uint8_t[]>(data.size()))
      , size(data.size())
    {
      std::copy(data.begin(), data.end(), payload.get());
    }

    Message(Message&& other) noexcept
      : header(other.header)
      , payload(std::exchange(other.payload, nullptr))
      , size(other.size)
    {
    }

    static std::optional<Message> from_bytes(const std::span<const uint8_t>& data)
    {
      auto maybe_header = Header::from_bytes(data);
      if (!maybe_header.has_value()) {
        return {};
      }
      Header header = maybe_header.value();
      if (data.size() < (header.aligned_length())) {
        // Error?
        return {};
      }
      return Message(header, data.subspan(HEADER_SIZE, header.data_length()));
    }

  public:
    Header header;
    std::unique_ptr<uint8_t[]> payload;
    std::size_t size;

    std::span<const uint8_t> data() const
    {
      if (payload) {
        return std::span { payload.get(), size };
      }
      else {
        return {};
      }
    }
    std::span<uint8_t> mut_data()
    {
      if (payload) {
        return std::span { payload.get(), size };
      }
      else {
        return {};
      }
    }
  };

  class Error {
  public:
    constexpr Error(grund::netlink::Header header, int error_code, grund::netlink::Header original_header)
      : header(header)
      , error_code(error_code)
      , original_header(original_header)
    {
    }

    static constexpr std::optional<Error> from_bytes(grund::netlink::Header header, const std::span<const uint8_t> data)
    {
      auto error_code = grund::from_bytes<int>(data);
      auto original_header = Header::from_bytes(data.subspan(sizeof(error_code)));
      if (!original_header.has_value()) {
        return {};
      }
      return Error { header, error_code, original_header.value() };
    }

    constexpr bool is_acknowledge() const { return error_code == 0; }

  public:
    Header header;
    int error_code;
    Header original_header;
  };

  class RawAttribute {
  public:
    static constexpr std::size_t HEADER_SIZE { align_to(sizeof(uint16_t) + sizeof(uint16_t)) };

    static constexpr uint16_t NESTED { 1 << 15 };
    static constexpr uint16_t NETWORK_BYTE_ORDER { 1 << 14 };

    uint16_t _type;
    std::span<uint8_t> data;

    static constexpr std::tuple<std::size_t, std::optional<RawAttribute>> from_bytes(const std::span<uint8_t> data)
    {
      if (data.size() < HEADER_SIZE) {
        return { 0, {} };
      }
      auto length = grund::from_bytes<uint16_t>(data);
      auto type = grund::from_bytes<uint16_t>(data.subspan(sizeof(length)));
      auto size = static_cast<std::size_t>(length);
      auto aligned_size = align_to(size);
      if (data.size() < aligned_size) {
        return { 0, {} };
      }
      auto data_size = size - HEADER_SIZE;
      return { aligned_size, RawAttribute { type, data.subspan(HEADER_SIZE, data_size) } };
    }

    constexpr uint16_t type() const { return _type & ~(NESTED | NETWORK_BYTE_ORDER); }
    constexpr uint16_t nested() const { return (_type & NESTED) == NESTED; }
    constexpr uint16_t network_byte_order() const { return (_type & NETWORK_BYTE_ORDER) == NETWORK_BYTE_ORDER; }

    const std::string_view as_string_view() const
    {
      return std::string_view(reinterpret_cast<const char*>(data.data()), data.size());
    }

    int8_t as_i8() const
    {
      return grund::from_bytes_exact<int8_t>(data);
    }

    uint8_t as_u8() const
    {
      return grund::from_bytes_exact<uint8_t>(data);
    }

    int16_t as_i16() const
    {
      return grund::from_bytes_exact<int16_t>(data);
    }

    uint16_t as_u16() const
    {
      return grund::from_bytes_exact<uint16_t>(data);
    }

    int32_t as_i32() const
    {
      return grund::from_bytes_exact<int32_t>(data);
    }

    uint32_t as_u32() const
    {
      return grund::from_bytes_exact<uint32_t>(data);
    }

    int64_t as_i64() const
    {
      return grund::from_bytes_exact<int64_t>(data);
    }

    uint64_t as_u64() const
    {
      return grund::from_bytes_exact<uint64_t>(data);
    }

    bool as_bool() const
    {
      return grund::from_bytes_exact<uint8_t>(data) > 0;
    }
  };

  template <typename TYPE>
  class AttributeValue {
  public:
    AttributeValue(const RawAttribute& raw)
    {
      (void)raw;
      throw std::runtime_error("Not implemented");
    }
  };

  template <>
  class AttributeValue<std::string> {
  public:
    std::string value;

    AttributeValue(const RawAttribute& raw)
    {
      value.assign(raw.data.begin(), raw.data.end());
    }
  };

  typedef AttributeValue<std::string> StringAttribute;

  template <uint16_t IDENTIFIER, typename TYPE>
  class Attribute {
    TYPE value;

    Attribute(const RawAttribute& raw)
      : value(raw)
    {
      if (raw.type() != IDENTIFIER) {
        throw std::runtime_error("Invalid netlink attribute type");
      }
    }
  };

}
}
#endif // GRUND_NETLINK_MESSAGE_HPP
