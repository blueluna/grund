#include <cstdint>
#include <optional>
#include <span>
extern "C" {
#include <net/if.h>
#include <net/if_arp.h>
}

#ifndef GRUND_NETLINK_ROUTE_MESSAGE_HPP
#define GRUND_NETLINK_ROUTE_MESSAGE_HPP

namespace grund::netlink::route {

class InterfaceInformation {
public:
  uint8_t family;
  uint16_t type;
  int index;
  uint32_t flags;

  static constexpr std::size_t SIZE { 16 };

  // type
  static constexpr uint16_t T_ETHERNET { ARPHRD_ETHER };
  static constexpr uint16_t T_LOOPBACK { ARPHRD_LOOPBACK };

  /// flags
  static constexpr uint32_t F_UP { IFF_UP }; // Interface is up
  static constexpr uint32_t F_BROADCAST { IFF_BROADCAST }; // Broadcast address is valid
  static constexpr uint32_t F_DEBUG { IFF_DEBUG }; // Interface debugging
  static constexpr uint32_t F_LOOPBACK { IFF_LOOPBACK }; // Loopback network
  static constexpr uint32_t F_POINT_TO_POINT { IFF_POINTOPOINT }; // Point to point link
  static constexpr uint32_t F_NO_TRAILERS { IFF_NOTRAILERS }; // Avoid use of trailers
  static constexpr uint32_t F_RUNNING { IFF_RUNNING }; // Operation mode is UP
  static constexpr uint32_t F_NO_ARP { IFF_NOARP }; // No ARP protocol
  static constexpr uint32_t F_PROMISCUOUS { IFF_PROMISC }; // Receive all packets
  static constexpr uint32_t F_ALL_MULTICAST { IFF_ALLMULTI }; // Receive all multi-cast packets
  static constexpr uint32_t F_MASTER { IFF_MASTER }; // Master of a load balancer
  static constexpr uint32_t F_SLAVE { IFF_SLAVE }; // Slave of a load balancer
  static constexpr uint32_t F_MULTICAST { IFF_MULTICAST }; // Supports multi-cast
  static constexpr uint32_t F_PORT_SELECTION { IFF_PORTSEL }; // Can select media type
  static constexpr uint32_t F_AUTOMATIC_MEDIA { IFF_AUTOMEDIA }; // Automatic media selection
  static constexpr uint32_t F_DYNAMIC { IFF_DYNAMIC }; // Dial-up device with changing addresses
  static constexpr uint32_t F_LOWER_UP { 1u << 16 }; // Driver signals L1 up
  static constexpr uint32_t F_DORMANT { 1u << 17 }; // Driver signals dormant
  static constexpr uint32_t F_ECHO { 1u << 18 }; // Echo sent packets

  static constexpr std::optional<InterfaceInformation> from_bytes(const std::span<const uint8_t> data)
  {
    if (data.size() < SIZE) {
      return {};
    }
    auto family = *reinterpret_cast<const uint8_t*>(data.data());
    auto type = *reinterpret_cast<const uint16_t*>(data.data() + sizeof(family) + sizeof(uint8_t));
    auto index = *reinterpret_cast<const int*>(data.data() + sizeof(family) + sizeof(uint8_t) + sizeof(type));
    auto flags = *reinterpret_cast<const uint32_t*>(data.data() + sizeof(family) + sizeof(uint8_t) + sizeof(type) + sizeof(index));
    return InterfaceInformation { family, type, index, flags };
  }

  constexpr size_t to_bytes(std::span<uint8_t> data)
  {
    if (data.size() < SIZE) {
      return 0;
    }
    auto mut = data.data();
    mut[0] = family;
    mut[1] = 0;
    *reinterpret_cast<uint16_t*>(mut + 2) = type;
    *reinterpret_cast<int*>(mut + 2 + sizeof(type)) = index;
    *reinterpret_cast<uint32_t*>(mut + 2 + sizeof(type) + sizeof(index)) = flags;
    *reinterpret_cast<uint32_t*>(mut + 2 + sizeof(type) + sizeof(index) + sizeof(flags)) = 0xffffffff;
    return SIZE;
  }

  static constexpr std::string_view type_label(uint16_t type)
  {
    switch (type) {
    case T_ETHERNET:
      return "Ethernet";
    case T_LOOPBACK:
      return "Loopback";
    default:
      return "Other";
    }
  }

  static std::string flags_label(uint32_t flags)
  {
    std::vector<std::string_view> items;
    if ((flags & F_UP) == F_UP) {
      items.push_back("Up");
    }
    if ((flags & F_BROADCAST) == F_BROADCAST) {
      items.push_back("Broadcast");
    }
    if ((flags & F_DEBUG) == F_DEBUG) {
      items.push_back("Debug");
    }
    if ((flags & F_LOOPBACK) == F_LOOPBACK) {
      items.push_back("Loopback");
    }
    if ((flags & F_POINT_TO_POINT) == F_POINT_TO_POINT) {
      items.push_back("Point To Point");
    }
    if ((flags & F_NO_TRAILERS) == F_NO_TRAILERS) {
      items.push_back("No Trailers");
    }
    if ((flags & F_RUNNING) == F_RUNNING) {
      items.push_back("Running");
    }
    if ((flags & F_NO_ARP) == F_NO_ARP) {
      items.push_back("No ARP");
    }
    if ((flags & F_PROMISCUOUS) == F_PROMISCUOUS) {
      items.push_back("Promiscuous");
    }
    if ((flags & F_ALL_MULTICAST) == F_ALL_MULTICAST) {
      items.push_back("All Multi-cast");
    }
    if ((flags & F_MASTER) == F_MASTER) {
      items.push_back("Master");
    }
    if ((flags & F_SLAVE) == F_SLAVE) {
      items.push_back("Slave");
    }
    if ((flags & F_MULTICAST) == F_MULTICAST) {
      items.push_back("Multi-cast");
    }
    if ((flags & F_PORT_SELECTION) == F_PORT_SELECTION) {
      items.push_back("Port Selection");
    }
    if ((flags & F_AUTOMATIC_MEDIA) == F_AUTOMATIC_MEDIA) {
      items.push_back("Automatic Media");
    }
    if ((flags & F_DYNAMIC) == F_DYNAMIC) {
      items.push_back("Dynamic");
    }
    if ((flags & F_LOWER_UP) == F_LOWER_UP) {
      items.push_back("Lower Up");
    }
    if ((flags & F_DORMANT) == F_DORMANT) {
      items.push_back("Dormant");
    }
    if ((flags & F_ECHO) == F_ECHO) {
      items.push_back("Echo");
    }
    return fmt::format("{}", fmt::join(items, " | "));
  }
};

/*
class LinkMessage
{
public:
  static constexpr std::optional<LinkMessage> from_bytes(const std::span<const uint8_t> data)
  {
    if (data.size() < SIZE) {
      return {};
    }
    auto family = *reinterpret_cast<const uint8_t*>(data.data());
    auto type = *reinterpret_cast<const uint16_t*>(data.data() + sizeof(family) + sizeof(uint8_t));
    auto index = *reinterpret_cast<const int*>(data.data() + sizeof(family) + sizeof(uint8_t) + sizeof(type));
    auto flags = *reinterpret_cast<const uint32_t*>(data.data() + sizeof(family) + sizeof(uint8_t) + sizeof(type) + sizeof(index));
    return LinkMessage { family, type, index, flags };
  }

  constexpr size_t to_bytes(std::span<uint8_t> data)
  {
    if (data.size() < SIZE) {
      return 0;
    }
    auto mut = data.data();
    mut[0] = family;
    mut[1] = 0;
    *reinterpret_cast<uint16_t*>(mut + 2) = type;
    *reinterpret_cast<int*>(mut + 2 + sizeof(type)) = index;
    *reinterpret_cast<uint32_t*>(mut + 2 + sizeof(type) + sizeof(index)) = flags;
    *reinterpret_cast<uint32_t*>(mut + 2 + sizeof(type) + sizeof(index) + sizeof(flags)) = 0xffffffff;
    return SIZE;
  }

public:
  InterfaceInformation _interface_information;

};
*/
enum class AttributeIdentifier : uint16_t {
  UNSPECIFIED,
  ADDRESS,
  BROADCAST,
  IFNAME,
  MTU,
  LINK,
  QUEUEING_DISCIPLINE,
  STATS,
  COST,
  PRIORITY,
  MASTER,
  WIRELESS,
  PROTINFO,
  TXQLEN,
  MAP,
  WEIGHT,
  OPERSTATE,
  LINKMODE,
  LINKINFO,
  NET_NS_PID,
  IFALIAS,
  NUM_VF,
  VFINFO_LIST,
  STATS64,
  VF_PORTS,
  PORT_SELF,
  AF_SPEC,
  GROUP,
  NET_NS_FD,
  EXT_MASK,
  PROMISCUITY,
  NUM_TX_QUEUES,
  NUM_RX_QUEUES,
  CARRIER,
  PHYS_PORT_ID,
  CARRIER_CHANGES,
  PHYS_SWITCH_ID,
  LINK_NETNSID,
  PHYS_PORT_NAME,
  PROTO_DOWN,
  GSO_MAX_SEGS,
  GSO_MAX_SIZE,
  PAD,
  XDP,
  EVENT,
  NEW_NETNSID,
  IF_NETNSID,
  TARGET_NETNSID = IF_NETNSID,
  CARRIER_UP_COUNT,
  CARRIER_DOWN_COUNT,
  NEW_IFINDEX,
  MIN_MTU,
  MAX_MTU,
  PROP_LIST,
  ALT_IFNAME,
  PERM_ADDRESS,
  PROTO_DOWN_REASON,
  PARENT_DEV_NAME,
  PARENT_DEV_BUS_NAME,
  GRO_MAX_SIZE,
  TSO_MAX_SIZE,
  TSO_MAX_SEGS,
  ALLMULTI,
  DEVLINK_PORT,
};

static constexpr std::array<std::tuple<AttributeIdentifier, std::string_view>, 64> ATTRIBUTES_TO_STRING {
  {
    { AttributeIdentifier::UNSPECIFIED, "Unspecified" },
    { AttributeIdentifier::ADDRESS, "Address" },
    { AttributeIdentifier::BROADCAST, "Broadcast" },
    { AttributeIdentifier::IFNAME, "Interface Name" },
    { AttributeIdentifier::MTU, "MTU" },
    { AttributeIdentifier::LINK, "Link" },
    { AttributeIdentifier::QUEUEING_DISCIPLINE, "Queueing Discipline" },
    { AttributeIdentifier::STATS, "Stats" },
    { AttributeIdentifier::COST, "Cost" },
    { AttributeIdentifier::PRIORITY, "Priority" },
    { AttributeIdentifier::MASTER, "Master" },
    { AttributeIdentifier::WIRELESS, "Wireless Extensions" },
    { AttributeIdentifier::PROTINFO, "Protocol Information" },
    { AttributeIdentifier::TXQLEN, "Transmit Queue Length" },
    { AttributeIdentifier::MAP, "Map" }, // Device memory map information, struct ifmap
    { AttributeIdentifier::WEIGHT, "Weight" },
    { AttributeIdentifier::OPERSTATE, "Operation State" },
    { AttributeIdentifier::LINKMODE, "Link Mode" },
    { AttributeIdentifier::LINKINFO, "Link Information" },
    { AttributeIdentifier::NET_NS_PID, "Network Namespace PID" },
    { AttributeIdentifier::IFALIAS, "Interface Alias" },
    { AttributeIdentifier::NUM_VF, "Number of VF" },
    { AttributeIdentifier::VFINFO_LIST, "VF Information" },
    { AttributeIdentifier::STATS64, "Stats 64" },
    { AttributeIdentifier::VF_PORTS, "VF Ports" },
    { AttributeIdentifier::PORT_SELF, "Port Self" },
    { AttributeIdentifier::AF_SPEC, "AF Specification" },
    { AttributeIdentifier::GROUP, "Group" },
    { AttributeIdentifier::NET_NS_FD, "Network Namespace File Descriptor" },
    { AttributeIdentifier::EXT_MASK, "Extended Mask Information" },
    { AttributeIdentifier::PROMISCUITY, "Promiscuity Count" },
    { AttributeIdentifier::NUM_TX_QUEUES, "Number of Transmit Queues" },
    { AttributeIdentifier::NUM_RX_QUEUES, "Number of Receive Queues" },
    { AttributeIdentifier::CARRIER, "Carrier" },
    { AttributeIdentifier::PHYS_PORT_ID, "Physical Port Identifier" },
    { AttributeIdentifier::CARRIER_CHANGES, "Carrier Changes" },
    { AttributeIdentifier::PHYS_SWITCH_ID, "Physical Switch Identifier" },
    { AttributeIdentifier::LINK_NETNSID, "Link Network Namespace Identifier" },
    { AttributeIdentifier::PHYS_PORT_NAME, "Physical Port Name" },
    { AttributeIdentifier::PROTO_DOWN, "Protocol Down" },
    { AttributeIdentifier::GSO_MAX_SEGS, "GSD Maximum Segments" },
    { AttributeIdentifier::GSO_MAX_SIZE, "GSO Maximum Size" },
    { AttributeIdentifier::PAD, "PAD" },
    { AttributeIdentifier::XDP, "XDP" },
    { AttributeIdentifier::EVENT, "Event" },
    { AttributeIdentifier::NEW_NETNSID, "New Network Namespace Identifier" },
    { AttributeIdentifier::IF_NETNSID, "Interface Network Namespace Identifier" },
    { AttributeIdentifier::TARGET_NETNSID, "Target Network Namespace Identifier" },
    { AttributeIdentifier::CARRIER_UP_COUNT, "Carrier Up Count" },
    { AttributeIdentifier::CARRIER_DOWN_COUNT, "Carrier Down Count" },
    { AttributeIdentifier::NEW_IFINDEX, "New Interface Index" },
    { AttributeIdentifier::MIN_MTU, "Minimum MTU" },
    { AttributeIdentifier::MAX_MTU, "Maximum MTU" },
    { AttributeIdentifier::PROP_LIST, "Property List" },
    { AttributeIdentifier::ALT_IFNAME, "Alternate Interface Name" },
    { AttributeIdentifier::PERM_ADDRESS, "Permanent Address" },
    { AttributeIdentifier::PROTO_DOWN_REASON, "Protocol Down Reason" },
    { AttributeIdentifier::PARENT_DEV_NAME, "Parent Device Name" },
    { AttributeIdentifier::PARENT_DEV_BUS_NAME, "Parent Device Bus Name" },
    { AttributeIdentifier::GRO_MAX_SIZE, "GRO Maximum Size" },
    { AttributeIdentifier::TSO_MAX_SIZE, "TSO Maximum Size" },
    { AttributeIdentifier::TSO_MAX_SEGS, "TSO Maximum Segments" },
    { AttributeIdentifier::ALLMULTI, "All Multicast Count" },
    { AttributeIdentifier::DEVLINK_PORT, "Device Link Port" },
  }
};

constexpr std::string_view attribute_label(uint16_t id)
{
  for (auto entry : ATTRIBUTES_TO_STRING) {
    auto entry_id = std::get<0>(entry);
    if (id == static_cast<uint16_t>(entry_id)) {
      return std::get<1>(entry);
    }
  }
  return {};
}

constexpr std::string_view operation_state_label(uint8_t operation_state)
{
  switch (operation_state) {
  case 0:
    return "Unknown";
  case 1:
    return "Not Present";
  case 2:
    return "Down";
  case 3:
    return "Lower Layer Down";
  case 4:
    return "Testing";
  case 5:
    return "Dormant";
  case 6:
    return "Up";
  }
  return {};
}

constexpr std::string_view link_mode_label(uint8_t link_mode)
{
  switch (link_mode) {
  case 0:
    return "Default";
  case 1:
    return "Dormant";
  }
  return {};
}

}

#endif // GRUND_NETLINK_ROUTE_MESSAGE_HPP
