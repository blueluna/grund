#include <grund/netlink/message.hpp>
#include <grund/socket.hpp>
#include <span>
#include <vector>

#ifndef GRUND_NETLINK_SOCKET_HPP
#define GRUND_NETLINK_SOCKET_HPP

namespace grund {
namespace netlink {

  enum Protocol : int {
    ROUTE = 0,
    KOBJECT_UEVENT = 15,
    GENERIC = 16,
  };

  class Socket {
  public:
    explicit Socket(Protocol protocol, uint32_t groups = 0, bool non_blocking = false);

    void request(uint16_t command, const std::span<const uint8_t>& data, bool acknowledge = false, bool dump = false);
    void request(uint16_t command, const std::initializer_list<const uint8_t>& data, bool acknowledge = false, bool dump = false);

    std::span<uint8_t> receive_bytes();

    std::vector<Message> receive();

    int fd() { return _socket.fd(); }
    Protocol protocol() { return Protocol { _socket.get_option<int>(SOL_SOCKET, SO_PROTOCOL) }; }
    void close();

  protected:
    grund::Socket _socket;
    uint32_t _process_identifier;
    grund::SocketAddress _peer;
    uint32_t _sequence_next;
    std::array<uint8_t, MESSAGE_BUFFER_SIZE> _receive_buffer;
    std::array<uint8_t, MESSAGE_BUFFER_SIZE> _send_buffer;
  };

}
}

#endif // GRUND_NETLINK_SOCKET_HPP
