project(grund_tests CXX)

set(test_name grund_tests)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
add_executable(${test_name}
        src/bip_buffer.cpp
        src/bits.cpp
        src/convert.cpp
        src/event_fd.cpp
        src/file_descriptor.cpp
        src/ip_address.cpp
        src/named_semaphore.cpp
        src/runner.cpp
        src/signal_fd.cpp
        src/socket_address.cpp
        src/system.cpp
        src/time.cpp
        src/timer_fd.cpp
        )
set_property(TARGET ${test_name} PROPERTY CXX_STANDARD 20)
target_compile_options(${test_name} PRIVATE $<$<CONFIG:Debug>: ${grund_DEBUG_COMPILER_FLAGS}>)
target_link_options(${test_name} PRIVATE $<$<CONFIG:Debug>: ${grund_DEBUG_LINKER_FLAGS}>)

add_test(${test_name} ${test_name})
target_link_libraries(${test_name} PUBLIC doctest grund-static)

#add_custom_command(TARGET ${test_name}
#                   POST_BUILD
#                   COMMAND ${CMAKE_CTEST_COMMAND} --output-on-failure
#		   )
