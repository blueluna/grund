#include <doctest.h>
#include <grund/signal_fd.hpp>
extern "C" {
#include <poll.h>
}

using namespace grund;

extern "C" void signal_handler(int signal_identifier)
{
  (void)signal_identifier;
}

void block_signals(int signals)
{
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, signals);
  sigprocmask(SIG_BLOCK, &mask, nullptr);
}

void unblock_signals(int signals)
{
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, signals);
  sigprocmask(SIG_UNBLOCK, &mask, nullptr);
}

TEST_CASE("signalfd")
{
  block_signals(SIGUSR2);
  signal(SIGUSR2, signal_handler);
  SignalFd sfd(SIGUSR2);
  raise(SIGUSR2);
  struct pollfd pfd;
  pfd.fd = sfd.fd();
  pfd.events = POLLIN;
  pfd.revents = 0;
  int nevents = poll(&pfd, 1, 1000);
  if (nevents == 0) {
    CHECK_MESSAGE(false, "Timeout");
  }
  else if (nevents == -1) {
    CHECK_MESSAGE(false, "Error");
  }
  else {
    int s = sfd.read_signal();
    CHECK(SIGUSR2 == s);
  }
}
