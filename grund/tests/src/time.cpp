#include <doctest.h>
#include <grund/time.hpp>

using namespace grund;

TEST_CASE("convert chrono to timespec")
{
  using namespace std::literals::chrono_literals;
  timespec ts = timespec_from_duration(1s);
  CHECK(0ull == ts.tv_nsec);
  CHECK(1ull == ts.tv_sec);
  ts = timespec_from_duration(1100ms);
  CHECK(100'000'000ull == ts.tv_nsec);
  CHECK(1ull == ts.tv_sec);
  ts = timespec_from_duration(100ns);
  CHECK(100ull == ts.tv_nsec);
  CHECK(0ull == ts.tv_sec);
  ts = timespec_from_duration(1'000'000'000ns);
  CHECK(0ull == ts.tv_nsec);
  CHECK(1ull == ts.tv_sec);
}