#include <doctest.h>
#include <grund/bip_buffer.hpp>

using namespace grund;

TEST_CASE("bbuffer ctor")
{
  {
    BipBuffer<16> bb;
  }
}

TEST_CASE("bbuffer operations")
{
  {
    BipBuffer<32> bb;
    {
      auto b = bb.grant_exact(31).value();
      CHECK(b.size() == 31);
      b[0] = 127;
      bb.commit(b, 1);
    }
    {
      auto b = bb.read().value();
      CHECK(b.size() == 1);
      CHECK(b[0] == 127);
      bb.release(b, 1);
    }
    {
      auto b = bb.grant_exact(16).value();
      CHECK(b.size() == 16);
      for (int n = 0; n < 16; n++) {
        b[n] = n;
      }
      bb.commit(b, 16);
    }
    {
      auto b = bb.grant_exact(32);
      CHECK_FALSE(b.has_value());
    }
    {
      auto b = bb.grant_exact(15).value();
      CHECK(b.size() == 15);
      for (int n = 0; n < 15; n++) {
        b[n] = n;
      }
      bb.commit(b, 15);
    }
    {
      auto b = bb.grant_exact(1);
      CHECK_FALSE(b.has_value());
    }
  }
}

TEST_CASE("bbuffer write in progress")
{
  {
    BipBuffer<32> bb;
    {
      auto b = bb.grant_exact(31).value();
      CHECK(b.size() == 31);
      b[0] = 127;
      bb.commit(b, 1);
    }
    {
      auto b = bb.read().value();
      CHECK(b.size() == 1);
      CHECK(b[0] == 127);
      bb.release(b, 1);
    }
    {
      auto b1 = bb.grant(1).value();
      CHECK(b1.size() == 1);
      auto b2 = bb.grant(1);
      CHECK_FALSE(b2.has_value());
      b1[0] = 127;
      bb.commit(b1, 1);
      auto b3 = bb.grant(1).value();
      CHECK(b3.size() == 1);
      bb.commit(b3, 0);
    }
  }
}

TEST_CASE("bbuffer read in progress")
{
  {
    BipBuffer<32> bb;
    {
      auto b = bb.grant_exact(31).value();
      CHECK(b.size() == 31);
      b[0] = 127;
      b[1] = 32;
      bb.commit(b, 2);
    }
    {
      auto b1 = bb.read().value();
      CHECK(b1.size() == 2);
      CHECK(b1[0] == 127);
      auto b2 = bb.read();
      CHECK_FALSE(b2.has_value());
      bb.release(b1, 1);
      auto b3 = bb.read().value();
      CHECK(b3.size() == 1);
    }
  }
}

TEST_CASE("bbuffer operations 2")
{
  {
    BipBuffer<32> bb;
    {
      auto b = bb.grant_exact(8).value();
      CHECK(b.size() == 8);
      b[0] = 0x10;
      b[1] = 0x20;
      b[2] = 0x30;
      b[3] = 0x40;
      b[4] = 0x50;
      b[5] = 0x60;
      b[6] = 0x70;
      b[7] = 0x80;
      bb.commit(b, 8);
    }
    {
      auto b = bb.grant_exact(8).value();
      CHECK(b.size() == 8);
      b[0] = 0x11;
      b[1] = 0x21;
      b[2] = 0x31;
      b[3] = 0x41;
      b[4] = 0x51;
      b[5] = 0x61;
      b[6] = 0x71;
      b[7] = 0x81;
      bb.commit(b, 8);
    }
    {
      auto b = bb.grant_exact(8).value();
      CHECK(b.size() == 8);
      b[0] = 0x12;
      b[1] = 0x22;
      b[2] = 0x32;
      b[3] = 0x42;
      b[4] = 0x52;
      b[5] = 0x62;
      b[6] = 0x72;
      b[7] = 0x82;
      bb.commit(b, 8);
    }
    {
      auto b = bb.grant_exact(8).value();
      CHECK(b.size() == 8);
      b[0] = 0x13;
      b[1] = 0x23;
      b[2] = 0x33;
      b[3] = 0x43;
      b[4] = 0x53;
      b[5] = 0x63;
      b[6] = 0x73;
      b[7] = 0x83;
      bb.commit(b, 8);
    }
    {
      CHECK_FALSE(bb.grant_exact(8).has_value());
    }
    {
      auto b = bb.read().value();
      CHECK(b.size() >= 8);
      CHECK(b[0] == 0x10);
      CHECK(b[1] == 0x20);
      CHECK(b[2] == 0x30);
      CHECK(b[3] == 0x40);
      CHECK(b[4] == 0x50);
      CHECK(b[5] == 0x60);
      CHECK(b[6] == 0x70);
      CHECK(b[7] == 0x80);
      bb.release(b, 8);
    }
    {
      auto b = bb.grant(8).value();
      CHECK(b.size() == 7);
      bb.commit(b, 0);
    }
    {
      auto b = bb.read().value();
      CHECK(b.size() >= 8);
      CHECK(b[0] == 0x11);
      CHECK(b[1] == 0x21);
      CHECK(b[2] == 0x31);
      CHECK(b[3] == 0x41);
      CHECK(b[4] == 0x51);
      CHECK(b[5] == 0x61);
      CHECK(b[6] == 0x71);
      CHECK(b[7] == 0x81);
      bb.release(b, 8);
    }
    {
      auto b = bb.grant_exact(8).value();
      CHECK(b.size() == 8);
      b[0] = 0x14;
      b[1] = 0x24;
      b[2] = 0x34;
      b[3] = 0x44;
      b[4] = 0x54;
      b[5] = 0x64;
      b[6] = 0x74;
      b[7] = 0x84;
      bb.commit(b, 8);
    }
    {
      CHECK_FALSE(bb.grant_exact(8).has_value());
    }
    {
      auto b = bb.read().value();
      CHECK(b.size() >= 8);
      CHECK(b[0] == 0x12);
      CHECK(b[1] == 0x22);
      CHECK(b[2] == 0x32);
      CHECK(b[3] == 0x42);
      CHECK(b[4] == 0x52);
      CHECK(b[5] == 0x62);
      CHECK(b[6] == 0x72);
      CHECK(b[7] == 0x82);
      bb.release(b, 8);
    }
    {
      auto b = bb.read().value();
      CHECK(b.size() >= 8);
      CHECK(b[0] == 0x13);
      CHECK(b[1] == 0x23);
      CHECK(b[2] == 0x33);
      CHECK(b[3] == 0x43);
      CHECK(b[4] == 0x53);
      CHECK(b[5] == 0x63);
      CHECK(b[6] == 0x73);
      CHECK(b[7] == 0x83);
      bb.release(b, 8);
    }
    {
      auto b = bb.read().value();
      CHECK(b.size() >= 8);
      CHECK(b[0] == 0x14);
      CHECK(b[1] == 0x24);
      CHECK(b[2] == 0x34);
      CHECK(b[3] == 0x44);
      CHECK(b[4] == 0x54);
      CHECK(b[5] == 0x64);
      CHECK(b[6] == 0x74);
      CHECK(b[7] == 0x84);
      bb.release(b, 8);
    }
    {
      CHECK_FALSE(bb.read().has_value());
    }
    {
      CHECK_FALSE(bb.read().has_value());
    }
  }
}
