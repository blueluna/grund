#include <doctest.h>
#include <grund/socket_address.hpp>
#include <grund/system_error.hpp>

using namespace grund;

TEST_CASE("IPv4 localhost")
{
  {
    auto address = SocketAddress::lookup_client(AF_INET, "localhost", nullptr);
    CHECK(address.is_ipv4());
    CHECK(address.ip_address() == IpAddress::ipv4_loopback());
  }
}

TEST_CASE("IPv4 exception")
{
  {
    CHECK_THROWS_AS(SocketAddress::lookup_client(AF_INET, "unknown", nullptr), SystemError);
  }
}

// This test case seems to throw an exception on older systems
TEST_CASE("IPv6 localhost")
{
  {
    auto address = SocketAddress::lookup_client(AF_INET6, "localhost", nullptr);
    CHECK(address.is_ipv6());
    CHECK(address.ip_address() == IpAddress::ipv6_loopback());
  }
}

TEST_CASE("IPv6 exception")
{
  {
    CHECK_THROWS_AS(SocketAddress::lookup_client(AF_INET6, "unknown", nullptr), SystemError);
  }
}

TEST_CASE("Localhost")
{
  {
    auto address = SocketAddress::lookup_client("localhost", nullptr);
    if (address.is_ipv6()) {
      CHECK(address.ip_address() == IpAddress::ipv6_loopback());
    }
    else {
      CHECK(address.ip_address() == IpAddress::ipv4_loopback());
    }
  }
  {
    auto address = SocketAddress::lookup_client("127.0.0.1", nullptr);
    CHECK(address.is_ipv4());
    CHECK(address.ip_address() == IpAddress::ipv4_loopback());
  }
  {
    auto address = SocketAddress::lookup_client("::1", nullptr);
    CHECK(address.is_ipv6());
    CHECK(address.ip_address() == IpAddress::ipv6_loopback());
  }
}

TEST_CASE("Any")
{
  {
    auto address = SocketAddress::lookup_client("0.0.0.0", nullptr);
    CHECK(address.is_ipv4());
    CHECK(address.ip_address() == IpAddress::ipv4_any());
  }
  {
    auto address = SocketAddress::lookup_client("::", nullptr);
    CHECK(address.is_ipv6());
    CHECK(address.ip_address() == IpAddress::ipv6_any());
  }
}

TEST_CASE("IPv4 service any")
{
  {
    auto address = SocketAddress::lookup_server(AF_INET, nullptr, "8080");
    CHECK(address.is_ipv4());
    CHECK(address.ip_address() == IpAddress::ipv4_any());
    CHECK(address.port() == 8080);
  }
  {
    auto address = SocketAddress::lookup_server(AF_INET, "0.0.0.0", "8080");
    CHECK(address.is_ipv4());
    CHECK(address.ip_address() == IpAddress::ipv4_any());
    CHECK(address.port() == 8080);
  }
}

TEST_CASE("IPv6 service any")
{
  {
    auto address = SocketAddress::lookup_server(AF_INET6, nullptr, "8080");
    CHECK(address.is_ipv6());
    CHECK(address.ip_address() == IpAddress::ipv6_any());
    CHECK(address.port() == 8080);
  }
  {
    auto address = SocketAddress::lookup_server(AF_INET6, "::", "8080");
    CHECK(address.is_ipv6());
    CHECK(address.ip_address() == IpAddress::ipv6_any());
    CHECK(address.port() == 8080);
  }
}

TEST_CASE("Service any")
{
  {
    auto address = SocketAddress::lookup_server("0.0.0.0", "8080");
    CHECK(address.is_ipv4());
    CHECK(address.ip_address() == IpAddress::ipv4_any());
    CHECK(address.port() == 8080);
  }
  {
    auto address = SocketAddress::lookup_server("::", "8080");
    CHECK(address.is_ipv6());
    CHECK(address.ip_address() == IpAddress::ipv6_any());
    CHECK(address.port() == 8080);
  }
}
