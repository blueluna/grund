#include <doctest.h>
#include <grund/ip_address.hpp>

using namespace grund;

TEST_CASE("IPv4 Constants")
{
  CHECK(IpAddress::Ipv4 == AF_INET);
  {
    IpAddress address = IpAddress::ipv4_any();
    CHECK(address.family() == IpAddress::Ipv4);
    CHECK(address.socklen() == sizeof(struct sockaddr_in));
    struct in_addr ia;
    bool ok = address.into_in_addr(ia);
    CHECK(ok);
    CHECK(ia.s_addr == ntohl(INADDR_ANY));
  }
  {
    IpAddress address = IpAddress::ipv4_broadcast();
    CHECK(address.family() == IpAddress::Ipv4);
    CHECK(address.socklen() == sizeof(struct sockaddr_in));
    struct in_addr ia;
    bool ok = address.into_in_addr(ia);
    CHECK(ok);
    CHECK(ia.s_addr == ntohl(INADDR_BROADCAST));
  }
  {
    IpAddress address = IpAddress::ipv4_loopback();
    CHECK(address.family() == IpAddress::Ipv4);
    CHECK(address.socklen() == sizeof(struct sockaddr_in));
    struct in_addr ia;
    bool ok = address.into_in_addr(ia);
    CHECK(ok);
    CHECK(ia.s_addr == ntohl(INADDR_LOOPBACK));
  }
}

TEST_CASE("IPv6 Constants")
{
  CHECK(IpAddress::Ipv6 == AF_INET6);
  {
    IpAddress address = IpAddress::ipv6_any();
    CHECK(address.family() == IpAddress::Ipv6);
    CHECK(address.socklen() == sizeof(struct sockaddr_in6));
    auto sa = address.into_sockaddr_storage(0);
    struct sockaddr_in6* sa6 = reinterpret_cast<struct sockaddr_in6*>(&sa);
    CHECK(memcmp(sa6->sin6_addr.s6_addr, in6addr_any.s6_addr, 16) == 0);
  }
  {
    IpAddress address = IpAddress::ipv6_loopback();
    CHECK(address.family() == IpAddress::Ipv6);
    CHECK(address.socklen() == sizeof(struct sockaddr_in6));
    auto sa = address.into_sockaddr_storage(0);
    struct sockaddr_in6* sa6 = reinterpret_cast<struct sockaddr_in6*>(&sa);
    CHECK(memcmp(sa6->sin6_addr.s6_addr, in6addr_loopback.s6_addr, 16) == 0);
  }
}

TEST_CASE("IP address from string")
{
  {
    IpAddress address("0.0.0.0");
    CHECK(address == IpAddress::ipv4_any());
  }
  {
    IpAddress address("255.255.255.255");
    CHECK(address == IpAddress::ipv4_broadcast());
  }
  {
    IpAddress address("127.0.0.1");
    CHECK(address == IpAddress::ipv4_loopback());
  }
  {
    IpAddress address("::");
    CHECK(address == IpAddress::ipv6_any());
  }
  {
    IpAddress address("::1");
    CHECK(address == IpAddress::ipv6_loopback());
  }
}
