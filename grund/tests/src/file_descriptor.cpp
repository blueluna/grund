#include <doctest.h>
#include <grund/file_descriptor.hpp>
extern "C" {
#include <fcntl.h>
#include <unistd.h>
}
#include <cstdlib>
#include <grund/system_error.hpp>
#include <string>

using namespace grund;

int create_temporary_file()
{
  std::string template_path { "/tmp/_grund_test_XXXXXX" };
  int fd = mkstemp(template_path.data());
  if (fd < 0) {
    throw SystemError();
  }
  return fd;
}

TEST_CASE("file_descriptor ctor")
{
  {
    FileDescriptor fd;
    CHECK(!fd.is_open());
    CHECK(fd.fd() == FileDescriptor::INVALID);
  }
  {
    FileDescriptor fd(0);
    CHECK(fd.is_open());
    CHECK(fd.fd() == 0);
  }
  {
    auto a = create_temporary_file();
    auto b = create_temporary_file();
    auto c = create_temporary_file();
    // Constructor int
    FileDescriptor fd1(a);
    FileDescriptor fd2(b);
    CHECK(fd1.is_open());
    CHECK(fd1.fd() == a);
    CHECK(fd2.is_open());
    CHECK(fd2.fd() == b);
    // Move constructor
    FileDescriptor fd3(std::move(fd1));
    CHECK(!fd1.is_open());
    CHECK(fd1.fd() == FileDescriptor::INVALID);
    CHECK(fd3.is_open());
    CHECK(fd3.fd() == a);
    // Move assignment
    fd1 = std::move(fd3);
    CHECK(fd1.is_open());
    CHECK(fd1.fd() == a);
    CHECK(!fd3.is_open());
    CHECK(fd3.fd() == FileDescriptor::INVALID);
    // Copy constructor int
    fd3 = fd1.fd();
    CHECK(fd1.is_open());
    CHECK(fd1.fd() == a);
    CHECK(fd3.is_open());
    CHECK(fd3.fd() == a);
    // Move assignment
    FileDescriptor fd4;
    fd4 = FileDescriptor { c };
    CHECK(fd4.is_open());
    CHECK(fd4.fd() == c);
  }
  {
    auto a = create_temporary_file();
    auto b = create_temporary_file();
    // Move assignment
    FileDescriptor fd4(a);
    fd4 = FileDescriptor { b };
    CHECK(fd4.is_open());
    CHECK(fd4.fd() == b);
  }
}

TEST_CASE("file_descriptor equals")
{
  auto a = create_temporary_file();
  auto b = create_temporary_file();
  FileDescriptor fd1(a);
  FileDescriptor fd2(b);
  FileDescriptor fd3(a);
  CHECK(fd1 == fd3);
  CHECK(fd1 != fd2);
  CHECK(fd1 == a);
  CHECK(fd1 != b);
}

TEST_CASE("file_descriptor create_temporary_file")
{
  auto fd = FileDescriptor::create_temporary_file();
  CHECK(fd.is_open());
}

TEST_CASE("file_descriptor read_object")
{
  FileDescriptor pipe_rd, pipe_wr;
  std::tie(pipe_rd, pipe_wr) = FileDescriptor::create_pipe();
  std::array<uint8_t, 4> data { 0xff, 0x01, 0x02, 0x03 };
  ssize_t length = ::write(pipe_wr, data.data(), data.size());
  CHECK(length == 4);
  uint32_t value = pipe_rd.read_object<uint32_t>();
  CHECK(value == 0x030201ff);
}

TEST_CASE("file_descriptor read_object short")
{
  FileDescriptor pipe_rd, pipe_wr;
  std::tie(pipe_rd, pipe_wr) = FileDescriptor::create_pipe();
  std::array<uint8_t, 3> data { 0xff, 0x01, 0x02 };
  ssize_t length = ::write(pipe_wr, data.data(), data.size());
  CHECK(length == 3);
  // Read, throws SystemError with EBADMSG
  CHECK_THROWS_AS(pipe_rd.read_object<uint32_t>(), SystemError);
}

TEST_CASE("file_descriptor write_object")
{
  FileDescriptor pipe_rd, pipe_wr;
  std::tie(pipe_rd, pipe_wr) = FileDescriptor::create_pipe();
  uint32_t value = 0x030201ff;
  pipe_wr.write_object(value);
  std::array<uint8_t, 4> data { 0x00, 0x00, 0x00, 0x00 };
  ssize_t length = ::read(pipe_rd, data.data(), data.size());
  CHECK(length == 4);
  CHECK(data[0] == 0xff);
  CHECK(data[1] == 0x01);
  CHECK(data[2] == 0x02);
  CHECK(data[3] == 0x03);
}

TEST_CASE("file_descriptor open_flags")
{
  {
    int mode = FileDescriptor::open_flags(FileDescriptor::READ);
    CHECK((O_CLOEXEC | O_RDONLY) == mode);
  }
  {
    int mode = FileDescriptor::open_flags(FileDescriptor::WRITE);
    CHECK((O_CLOEXEC | O_WRONLY) == mode);
  }
  {
    int mode = FileDescriptor::open_flags(FileDescriptor::READ | FileDescriptor::WRITE);
    CHECK((O_CLOEXEC | O_RDWR) == mode);
  }
  {
    int mode = FileDescriptor::open_flags(FileDescriptor::READ | FileDescriptor::WRITE | FileDescriptor::SYNC);
    CHECK((O_CLOEXEC | O_RDWR | O_SYNC) == mode);
  }
  {
    int mode = FileDescriptor::open_flags(FileDescriptor::READ | FileDescriptor::WRITE | FileDescriptor::NON_BLOCKING);
    CHECK((O_CLOEXEC | O_RDWR | O_NONBLOCK) == mode);
  }
}
