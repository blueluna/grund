#include <doctest.h>
#include <grund/named_semaphore.hpp>

using namespace grund;

static const std::string ns_name("/grund-tests-named-semaphore");

TEST_CASE("NamedSemaphore name")
{
  {
    auto name = NamedSemaphore::fix_name("apa");
    CHECK_EQ("/apa", name);
  }
  {
    auto name = NamedSemaphore::fix_name("one/two");
    CHECK_EQ("/one-two", name);
  }
  {
    auto name = NamedSemaphore::fix_name("/one/two");
    CHECK_EQ("/one-two", name);
  }
  {
    auto name = NamedSemaphore::fix_name("/one-two");
    CHECK_EQ("/one-two", name);
  }
}

TEST_CASE("NamedSemaphore c-tor")
{
  NamedSemaphore none;
  CHECK_FALSE(none);
  {
    CHECK_THROWS_AS(NamedSemaphore::open(ns_name), std::system_error);
    auto apa_1 = NamedSemaphore::create(ns_name, 0);
    CHECK(apa_1);
    CHECK_THROWS_AS(NamedSemaphore::create(ns_name, 0), std::system_error);
    auto apa_2 = NamedSemaphore::open(ns_name);
    CHECK(apa_2);
  }
  NamedSemaphore::unlink(ns_name);
  {
    CHECK_THROWS_AS(NamedSemaphore::open(ns_name), std::system_error);
    auto apa_1 = NamedSemaphore::open_create(ns_name, 0);
    auto apa_2 = NamedSemaphore::open_create(ns_name, 0);
  }
  NamedSemaphore::unlink(ns_name);
}

TEST_CASE("NamedSemaphore operation")
{
  using namespace std::literals::chrono_literals;
  {
    NamedSemaphore none;
    CHECK_FALSE(none);
    CHECK_THROWS_AS(none.post(), std::system_error);
    CHECK_THROWS_AS(none.wait(), std::system_error);
    CHECK_THROWS_AS(none.try_wait(), std::system_error);
    CHECK_THROWS_AS(none.wait(1s), std::system_error);
  }
  {
    auto apa_1 = NamedSemaphore::open_create(ns_name, 0);
    auto apa_2 = NamedSemaphore::open_create(ns_name, 0);
    apa_1.post();
    apa_2.wait();
    apa_2.post();
    CHECK(apa_1.wait(1s));
    apa_1.post();
    CHECK(apa_2.try_wait());
    CHECK_FALSE(apa_2.try_wait());
    CHECK_FALSE(apa_1.wait(1us));
  }
  {
    NamedSemaphore none;
    {
      auto apa_1 = NamedSemaphore::open_create(ns_name, 0);
      none = std::move(apa_1);
      CHECK_FALSE(apa_1);
      CHECK_THROWS_AS(apa_1.post(), std::system_error);
    }
    CHECK(none);
    auto apa_2 = NamedSemaphore::open_create(ns_name, 0);
    CHECK(apa_2);
    none.post();
    apa_2.wait();
    apa_2.post();
    CHECK(none.wait(1s));
    none.post();
    CHECK(apa_2.try_wait());
    CHECK_FALSE(apa_2.try_wait());
    CHECK_FALSE(none.wait(1us));
  }
  {
    NamedSemaphore apa_1;
    CHECK_FALSE(apa_1);
    {
      apa_1 = NamedSemaphore::open_create(ns_name, 0);
    }
    CHECK(apa_1);
    auto apa_2 = NamedSemaphore::open_create(ns_name, 0);
    apa_1.post();
    apa_2.wait();
    apa_2.post();
    CHECK(apa_1.wait(1s));
    apa_1.post();
    CHECK(apa_2.try_wait());
    CHECK_FALSE(apa_2.try_wait());
    CHECK_FALSE(apa_1.wait(1us));
  }
  NamedSemaphore::unlink(ns_name);
}
