#include <doctest.h>
#include <grund/event_fd.hpp>

using namespace grund;

TEST_CASE("eventfd")
{
  EventFd event;
  event.emit();
  uint64_t count = event.read_event();
  CHECK(count == 1);
  count = event.read_event();
  CHECK(count == 0);
  event.emit(4);
  count = event.read_event();
  CHECK(count == 4);
  count = event.read_event();
  CHECK(count == 0);
}

TEST_CASE("eventfd_semaphore")
{
  EventFdSemaphore event;
  event.emit();
  uint64_t count = event.read_event();
  CHECK(count == 1);
  count = event.read_event();
  CHECK(count == 0);
  event.emit(4);
  count = event.read_event();
  CHECK(count == 1);
  count = event.read_event();
  CHECK(count == 1);
  count = event.read_event();
  CHECK(count == 1);
  count = event.read_event();
  CHECK(count == 1);
  count = event.read_event();
  CHECK(count == 0);
}
