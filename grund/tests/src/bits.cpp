#include <doctest.h>
#include <grund/bits.hpp>

using namespace grund;

TEST_CASE("modify")
{
  {
    uint32_t a { 0x00800000 };
    uint32_t b { 0x00c00000 };
    uint32_t c { 0x01400001 };
    CHECK(0x01800001 == bits_modify(c, a, b));
  }
  {
    uint32_t a { 0x00000000 };
    uint32_t b { 0x00c00000 };
    uint32_t c { 0xffffffff };
    CHECK(0xff3fffff == bits_modify(c, a, b));
  }
  {
    uint32_t a { 0xffffffff };
    uint32_t b { 0x00c00000 };
    uint32_t c { 0x00000000 };
    CHECK(0x00c00000 == bits_modify(c, a, b));
  }
}
