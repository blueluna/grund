#include <doctest.h>
#include <grund/timer_fd.hpp>
extern "C" {
#include <poll.h>
}

using namespace grund;
using namespace std::literals::chrono_literals;

TEST_CASE("timerfd")
{
  TimerFd timer;
  timer.arm(1ms, false);
  struct pollfd pfd;
  pfd.fd = timer.fd();
  pfd.events = POLLIN;
  pfd.revents = 0;
  int nevents = poll(&pfd, 1, 1);
  if (nevents == 0) {
    CHECK_MESSAGE(false, "Timeout");
  }
  else if (nevents == -1) {
    CHECK_MESSAGE(false, "Error");
  }
  else {
    timer.clear();
    CHECK(true);
  }
}

TEST_CASE("timerfd periodic")
{
  TimerFd timer;
  timer.arm(1ms, true);
  struct pollfd pfd;
  pfd.fd = timer.fd();
  pfd.events = POLLIN;
  pfd.revents = 0;
  int nevents = poll(&pfd, 1, 1);
  if (nevents == 0) {
    CHECK_MESSAGE(false, "Timeout");
  }
  else if (nevents == -1) {
    CHECK_MESSAGE(false, "Error");
  }
  else {
    timer.clear();
    CHECK(true);
  }

  nevents = poll(&pfd, 1, 1);
  if (nevents == 0) {
    CHECK_MESSAGE(false, "Timeout");
  }
  else if (nevents == -1) {
    CHECK_MESSAGE(false, "Error");
  }
  else {
    timer.clear();
    CHECK(true);
  }
}

TEST_CASE("timerfd move")
{
  TimerFd timer1;
  int fd1 = timer1.fd();
  CHECK(timer1.is_open());
  CHECK(fd1 == timer1.fd());
  TimerFd timer2(std::move(timer1));
  CHECK(!timer1.is_open());
  CHECK(FileDescriptor::INVALID == timer1.fd());
  CHECK(timer2.is_open());
  CHECK(fd1 == timer2.fd());
  TimerFd timer3 = std::move(timer2);
  CHECK(!timer2.is_open());
  CHECK(FileDescriptor::INVALID == timer2.fd());
  CHECK(timer3.is_open());
  CHECK(fd1 == timer3.fd());
}