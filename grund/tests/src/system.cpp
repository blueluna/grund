#include <doctest.h>
#include <grund/system.hpp>

using namespace grund;

TEST_CASE("Get Page Size")
{
  std::size_t page_size = System::get_page_size();
  CHECK(page_size > 0);
}
