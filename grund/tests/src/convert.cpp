#include <cmath>
#include <doctest.h>
#include <grund/convert.hpp>

using namespace grund;

TEST_CASE("convert to ssize_t")
{
  CHECK(0ull == from_string<ssize_t>("0"));
  CHECK(std::numeric_limits<ssize_t>::max() == from_string<ssize_t>("9223372036854775807"));
  CHECK(std::numeric_limits<ssize_t>::min() == from_string<ssize_t>("-9223372036854775808"));
  CHECK_THROWS_AS(from_string<ssize_t>("-9223372036854775809"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<ssize_t>("9223372036854775808"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<ssize_t>(""), std::invalid_argument);
  CHECK_THROWS_AS(from_string<ssize_t>("one"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<ssize_t>("~1"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<ssize_t>("2b"), std::invalid_argument);
  CHECK(15ull == from_string<ssize_t>("0xf"));
  CHECK_THROWS_AS(from_string<ssize_t>("0xfffffffg"), std::invalid_argument);
}

TEST_CASE("convert to int32_t")
{
  CHECK(0 == from_string<int32_t>("0"));
  CHECK(std::numeric_limits<int32_t>::max() == from_string<int32_t>("2147483647"));
  CHECK(std::numeric_limits<int32_t>::min() == from_string<int32_t>("-2147483648"));
  CHECK_THROWS_AS(from_string<int32_t>("-2147483649"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<int32_t>("2147483648"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<int32_t>(""), std::invalid_argument);
  CHECK_THROWS_AS(from_string<int32_t>("one"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<int32_t>("~1"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<int32_t>("2b"), std::invalid_argument);
  CHECK(15 == from_string<int32_t>("0xf"));
  CHECK_THROWS_AS(from_string<int32_t>("0xfffffffg"), std::invalid_argument);
}

TEST_CASE("convert to size_t")
{
  CHECK(0ull == from_string<size_t>("0"));
  CHECK(18446744073709551615ull == from_string<size_t>("18446744073709551615"));
  CHECK_THROWS_AS(from_string<size_t>("-1"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<size_t>("18446744073709551616"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<size_t>(""), std::invalid_argument);
  CHECK_THROWS_AS(from_string<size_t>("one"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<size_t>("~1"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<size_t>("2b"), std::invalid_argument);
  CHECK(15ull == from_string<size_t>("0xf"));
  CHECK_THROWS_AS(from_string<size_t>("0xfffffffg"), std::invalid_argument);
}

TEST_CASE("convert to uint32_t")
{
  CHECK(0 == from_string<uint32_t>("0"));
  CHECK(std::numeric_limits<uint32_t>::max() == from_string<uint32_t>("4294967295"));
  CHECK_THROWS_AS(from_string<uint32_t>("-1"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint32_t>("4294967296"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint32_t>(""), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint32_t>("one"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint32_t>("~1"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint32_t>("2b"), std::invalid_argument);
  CHECK(15ull == from_string<uint32_t>("0xf"));
  CHECK_THROWS_AS(from_string<uint32_t>("0xfffffffg"), std::invalid_argument);
}

TEST_CASE("convert to uint16_t")
{
  CHECK(0 == from_string<uint16_t>("0"));
  CHECK(65535 == from_string<uint16_t>("65535"));
  CHECK_THROWS_AS(from_string<uint16_t>("-1"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint16_t>("65536"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint16_t>(""), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint16_t>("one"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint16_t>("~1"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint16_t>("2b"), std::invalid_argument);
  CHECK(15ull == from_string<uint16_t>("0xf"));
  CHECK_THROWS_AS(from_string<uint16_t>("0xfffffffg"), std::invalid_argument);
}

TEST_CASE("convert to uint8_t")
{
  CHECK(0 == from_string<uint8_t>("0"));
  CHECK(255 == from_string<uint8_t>("255"));
  CHECK_THROWS_AS(from_string<uint8_t>("-1"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint8_t>("256"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint8_t>(""), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint8_t>("one"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint8_t>("~1"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<uint8_t>("2b"), std::invalid_argument);
  CHECK(15ull == from_string<uint8_t>("0xf"));
  CHECK_THROWS_AS(from_string<uint8_t>("0xfffffffg"), std::invalid_argument);
}

TEST_CASE("convert to double")
{
  CHECK(0.0 == from_string<double>("0"));
  CHECK(std::numeric_limits<double>::max() == from_string<double>("179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368.0"));
  CHECK(-1.1 == from_string<double>("-1.1"));
  CHECK(1.1 == from_string<double>("1.1"));
  CHECK(2.0e9 == from_string<double>("2.0e9"));
  CHECK(std::isinf(from_string<double>("inf")));
  CHECK_THROWS_AS(from_string<double>(""), std::invalid_argument);
  CHECK_THROWS_AS(from_string<double>("one"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<double>("~1.0"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<double>("2.0b"), std::invalid_argument);
  CHECK(15.0 == from_string<double>("0xf"));
  CHECK_THROWS_AS(from_string<double>("0xfffffffg"), std::invalid_argument);
}

TEST_CASE("convert to float")
{
  CHECK(0.0f == from_string<float>("0"));
  CHECK(std::numeric_limits<float>::max() == from_string<float>("340282346638528859811704183484516925440.0"));
  CHECK(-1.1f == from_string<float>("-1.1"));
  CHECK(1.1f == from_string<float>("1.1"));
  CHECK(2.0e9f == from_string<float>("2.0e9"));
  CHECK(std::isinf(from_string<float>("inf")));
  CHECK_THROWS_AS(from_string<float>(""), std::invalid_argument);
  CHECK_THROWS_AS(from_string<float>("one"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<float>("~1.0"), std::invalid_argument);
  CHECK_THROWS_AS(from_string<float>("2.0b"), std::invalid_argument);
  CHECK(15.0f == from_string<float>("0xf"));
  CHECK_THROWS_AS(from_string<float>("0xfffffffg"), std::invalid_argument);
}
