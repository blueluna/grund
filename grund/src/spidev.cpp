#include <cstring>
#include <fmt/core.h>
#include <grund/spidev.hpp>
#include <linux/spi/spidev.h>
#include <vector>

namespace grund {

std::filesystem::path spi_device_path(uint16_t bus, uint16_t device)
{
  return std::filesystem::path { fmt::format("/dev/spidev{}.{}", bus, device) };
}

SpiDev::SpiDev(uint16_t bus, uint16_t device)
  : FileDescriptor(spi_device_path(bus, device), FileDescriptor::READ | FileDescriptor::WRITE)
{
}

SpiDev::MODE SpiDev::get_mode()
{
  uint8_t _mode;
  ioctl_get(SPI_IOC_RD_MODE, _mode);
  switch (_mode) {
  case SPI_MODE_0:
    return SpiDev::MODE::MODE_0;
  case SPI_MODE_1:
    return SpiDev::MODE::MODE_1;
  case SPI_MODE_2:
    return SpiDev::MODE::MODE_2;
  case SPI_MODE_3:
    return SpiDev::MODE::MODE_3;
  }
  throw std::runtime_error("Invalid SPI mode");
}

void SpiDev::set_mode(SpiDev::MODE mode)
{
  uint8_t _mode = SPI_MODE_0;
  switch (mode) {
  case MODE::MODE_0:
    _mode = SPI_MODE_0;
    break;
  case MODE::MODE_1:
    _mode = SPI_MODE_1;
    break;
  case MODE::MODE_2:
    _mode = SPI_MODE_2;
    break;
  case MODE::MODE_3:
    _mode = SPI_MODE_3;
    break;
  }
  ioctl_set(SPI_IOC_WR_MODE, _mode);
}

uint8_t SpiDev::get_bits_per_word()
{
  uint8_t bits_per_word;
  ioctl_get(SPI_IOC_RD_BITS_PER_WORD, bits_per_word);
  return bits_per_word;
}

void SpiDev::set_bits_per_word(uint8_t bits_per_word)
{
  ioctl_set(SPI_IOC_WR_BITS_PER_WORD, bits_per_word);
}

uint32_t SpiDev::get_frequency()
{
  uint32_t frequency;
  ioctl_get(SPI_IOC_RD_MAX_SPEED_HZ, frequency);
  return frequency;
}

void SpiDev::set_frequency(uint32_t frequency)
{
  ioctl_set(SPI_IOC_WR_MAX_SPEED_HZ, frequency);
}

bool SpiDev::send(const uint8_t* tx_ptr, size_t tx_len)
{
  static const size_t TRANSFER_COUNT = 1;
  spi_ioc_transfer transfer[TRANSFER_COUNT];
  std::memset(transfer, 0, sizeof(transfer));
  transfer[0].tx_buf = reinterpret_cast<unsigned long>(tx_ptr);
  transfer[0].len = tx_len;

  int count = ioctl(SPI_IOC_MESSAGE(TRANSFER_COUNT), &transfer);

  return TRANSFER_COUNT == count;
}

bool SpiDev::send(const std::initializer_list<const uint8_t>& tx)
{
  return send(tx.begin(), tx.size());
}

bool SpiDev::send(const std::span<uint8_t>& tx)
{
  return send(tx.data(), tx.size());
}

bool SpiDev::send_receive(const uint8_t* tx_ptr, size_t tx_len, uint8_t* rx_ptr, size_t rx_len)
{
  static const size_t TRANSFER_COUNT = 2;
  spi_ioc_transfer transfer[TRANSFER_COUNT];
  std::memset(transfer, 0, sizeof(transfer));
  transfer[0].rx_buf = 0;
  transfer[0].tx_buf = reinterpret_cast<unsigned long>(tx_ptr);
  transfer[0].len = tx_len;

  transfer[1].rx_buf = reinterpret_cast<unsigned long>(rx_ptr);
  transfer[1].tx_buf = 0;
  transfer[1].len = rx_len;

  int count = ioctl(SPI_IOC_MESSAGE(TRANSFER_COUNT), &transfer);

  return TRANSFER_COUNT == count;
}

bool SpiDev::send_receive(const std::initializer_list<const uint8_t>& tx, std::span<uint8_t> rx)
{
  return send_receive(tx.begin(), tx.size(), rx.data(), rx.size());
}

bool SpiDev::send_receive(const std::span<const uint8_t>& tx, std::span<uint8_t>& rx)
{
  return send_receive(tx.data(), tx.size(), rx.data(), rx.size());
}

}