#include "grund/system_resource_lock.hpp"

namespace grund {

SystemResourceLock::SystemResourceLock()
{
}

SystemResourceLock::SystemResourceLock(const std::string& name)
  : _semaphore { NamedSemaphore::open_create(name, 1) }
  , _locked(false)
{
}

bool SystemResourceLock::acquire(std::chrono::nanoseconds timeout)
{
  if (_locked) {
    return true;
  }
  if (_semaphore.wait(timeout)) {
    // exhaust semaphore counter
    while (_semaphore.try_wait()) {
      ;
    }
    _locked = true;
    return true;
  }
  return false;
}

void SystemResourceLock::release()
{
  if (_locked) {
    _semaphore.post();
    _locked = false;
  }
}

}