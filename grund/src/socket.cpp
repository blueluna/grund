//
// Created by esvensson on 2021-12-18.
//

#include "grund/socket.hpp"
#include "grund/system_error.hpp"

using namespace grund;

Socket::Socket(const sa_family_t& family, Socket::Type type, bool non_blocking, int protocol)
{
  int fd = ::socket(family, static_cast<int>(type) | SOCK_CLOEXEC | (non_blocking ? SOCK_NONBLOCK : 0), protocol);
  if (fd == -1) {
    throw SystemError();
  }
  _fd = fd;
}

void Socket::bind(const SocketAddress& address)
{
  if (::bind(_fd, static_cast<const struct sockaddr*>(address), address.socklen()) == -1) {
    int error = errno;
    close();
    throw SystemError(error);
  }
}

void Socket::bind(const IpAddress& address, const uint16_t port)
{
  bind(SocketAddress(address, port));
}

void Socket::bind(const uint16_t port)
{
  bind(IpAddress::ipv4_any(), port);
}

bool Socket::connect(const SocketAddress& address)
{
  bool in_progress = false;
  if (::connect(_fd, (struct sockaddr*)&address, address.socklen()) == -1) {
    int result = errno;
    if (result == EINPROGRESS) {
      in_progress = true;
    }
    else {
      throw SystemError(result);
    }
  }
  return in_progress;
}

void Socket::listen(const int backlog)
{
  if (::listen(_fd, backlog) == -1) {
    throw SystemError();
  }
}

Socket Socket::accept()
{
  int fd = ::accept(_fd, nullptr, 0);
  if (fd == -1) {
    throw SystemError();
  }
  return Socket(fd);
}

std::tuple<Socket, SocketAddress> Socket::accept_with_address()
{
  struct sockaddr_storage sa { };
  auto ptr = &sa;
  memset(ptr, 0, sizeof(struct sockaddr_storage));
  socklen_t sl = sizeof(struct sockaddr_storage);
  int fd = ::accept(_fd, reinterpret_cast<sockaddr*>(ptr), &sl);
  if (fd == -1) {
    throw SystemError();
  }
  return std::tuple<Socket, SocketAddress>(Socket(fd), SocketAddress(sa));
}

template <typename T>
T Socket::get_option(int level, int operation)
{
  T value;
  if (setsockopt(fd(), level, operation, (void*)&value, sizeof(T)) < 0) {
    throw SystemError();
  }
  return value;
}

void Socket::set_option(int level, int operation, bool value)
{
  int boolean = value ? 1 : 0;
  return set_option(level, operation, boolean);
}

void Socket::set_option(int level, int operation, int value)
{
  if (setsockopt(fd(), level, operation, (void*)&value, sizeof(int)) < 0) {
    throw SystemError();
  }
}

void Socket::shutdown()
{
  ::shutdown(fd(), SHUT_RDWR);
}

SocketAddress Socket::get_address() const
{
  struct sockaddr_storage storage { };
  auto address = &storage;
  memset(address, 0, sizeof(struct sockaddr_storage));
  socklen_t length = sizeof(struct sockaddr_storage);
  if (-1 == ::getsockname(fd(), reinterpret_cast<sockaddr*>(address), &length)) {
    throw SystemError();
  }
  return SocketAddress { storage };
}
