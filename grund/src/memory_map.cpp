#include <grund/memory_map.hpp>
#include <grund/system.hpp>
#include <grund/system_error.hpp>
#include <utility>

namespace grund {

inline size_t calculate_alignment(const size_t offset)
{
  size_t alignment = 0;
  if (offset != 0) [[likely]] {
    size_t page_size = System::get_page_size();
    alignment = offset % page_size;
  }
  return alignment;
}

inline size_t calculate_alignment(const uint8_t* pointer)
{
  size_t address = reinterpret_cast<size_t>(pointer);
  return address % System::get_page_size();
}

MemoryMap::MemoryMap()
  : _memory(nullptr)
  , _size(0)
{
}

MemoryMap::MemoryMap(MemoryMap&& other)
  : _memory(std::exchange(other._memory, nullptr))
  , _size(std::exchange(other._size, 0))
{
}

MemoryMap& MemoryMap::operator=(MemoryMap&& other)
{
  std::swap(_memory, other._memory);
  std::swap(_size, other._size);
  return *this;
}

MemoryMap::MemoryMap(void* target, size_t size, int protection, int flags, int fd, size_t offset)
  : _memory(nullptr)
  , _size(0)
{
  size_t alignment = calculate_alignment(offset);
  void* memory = mmap(target, size + alignment, protection, flags, fd, offset - alignment);
  if (memory == MAP_FAILED) [[unlikely]] {
    throw SystemError();
  }
  madvise(memory, size, MADV_RANDOM);
  _memory = reinterpret_cast<uint8_t*>(memory) + alignment;
  _size = size;
}

MemoryMap::MemoryMap(size_t size, int protection, int flags, int fd, size_t offset)
  : MemoryMap(nullptr, size, protection, flags, fd, offset)
{
}

MemoryMap::~MemoryMap()
{
  if (_memory != nullptr) {
    size_t alignment = calculate_alignment(_memory);
    size_t size = _size + alignment;
    munmap(_memory - alignment, size);
  }
  _memory = nullptr;
  _size = 0;
}

MemoryMap MemoryMap::map_read(int fd, size_t size, size_t offset)
{
  return MemoryMap(size, PROT_READ, MAP_SHARED, fd, offset);
}

MemoryMap MemoryMap::map_read_write(int fd, size_t size, size_t offset)
{
  return MemoryMap(size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, offset);
}

uint8_t* MemoryMap::pointer_mut()
{
  if (_memory == nullptr || _size == 0) [[unlikely]] {
    return nullptr;
  }
  return _memory;
}

uint8_t* MemoryMap::pointer_mut(size_t offset)
{
  uint8_t* root = pointer_mut();
  if (nullptr == root) [[unlikely]] {
    return nullptr;
  }
  if (offset >= _size) [[unlikely]] {
    return nullptr;
  }
  return root + offset;
}

const uint8_t* MemoryMap::pointer()
{
  return const_cast<const uint8_t*>(pointer_mut());
}

const uint8_t* MemoryMap::operator[](size_t offset)
{
  return const_cast<const uint8_t*>(offset_bytes<uint8_t>(offset));
}

void MemoryMap::flush(size_t offset, size_t length, int flags)
{
  size_t alignment = calculate_alignment(_memory + offset);
  void* ptr = reinterpret_cast<void*>(_memory + offset - alignment);
  if (msync(ptr, length + alignment, flags) == 0) [[likely]] {
    return;
  }
  throw SystemError();
}

void MemoryMap::flush(size_t offset, size_t length)
{
  flush(offset, length, MS_SYNC);
}

void MemoryMap::flush_async(size_t offset, size_t length)
{
  flush(offset, length, MS_ASYNC);
}

}