#include <grund/string.hpp>

#include <cstring>

namespace grund {

bool string_starts_with(const char* needle, const char* haystack)
{
  return strncmp(needle, haystack, strlen(needle)) == 0;
}

} // namespace grund