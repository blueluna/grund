#include <cstring>

#include "grund/system_error.hpp"
#include "grund/udp_multicast.hpp"

namespace grund {

void UdpMulticast::multicast_loop_enable(bool enable)
{
  int enable_value = enable ? 1 : 0;
  if (setsockopt(fd(), IPPROTO_IP, IP_MULTICAST_LOOP, &enable_value, sizeof(enable_value)) == -1) {
    throw SystemError();
  }
}

void UdpMulticast::join_ipv4(struct in_addr* address)
{
  multicast_loop_enable(true);
  IpAddress a4(*address);
  struct ip_mreq mreq = {};
  memcpy(&mreq.imr_multiaddr, address, sizeof(struct in_addr));
  mreq.imr_interface.s_addr = htonl(INADDR_ANY);
  if (setsockopt(fd(), IPPROTO_IP, IP_ADD_MEMBERSHIP, (void*)&mreq, sizeof(mreq)) == -1) {
    throw SystemError();
  }
  mreq.imr_interface.s_addr = htonl(INADDR_LOOPBACK);
  if (setsockopt(fd(), IPPROTO_IP, IP_ADD_MEMBERSHIP, (void*)&mreq, sizeof(mreq)) < 0) {
    throw SystemError();
  }
}

void UdpMulticast::join_ipv6(struct in6_addr* address)
{
  multicast_loop_enable(true);
  struct ipv6_mreq mreq = {};
  memcpy(&mreq.ipv6mr_multiaddr, address, sizeof(struct in6_addr));
  mreq.ipv6mr_interface = 0;
  if (setsockopt(fd(), IPPROTO_IP, IPV6_JOIN_GROUP, (void*)&mreq, sizeof(mreq)) < 0) {
    throw SystemError();
  }
}

void UdpMulticast::join(IpAddress& address)
{
  if (address.family() == IpAddress::Ipv4) {
    struct in_addr addr;
    address.into_in_addr(addr);
    join_ipv4(&addr);
    return;
  }
  else if (address.family() == IpAddress::Ipv6) {
    struct in6_addr addr;
    address.into_in6_addr(addr);
    join_ipv6(&addr);
    return;
  }
  throw SystemError(EINVAL);
}

std::tuple<std::size_t, SocketAddress> UdpMulticast::receive_from(void* buffer, std::size_t buffer_len)
{
  struct sockaddr_storage sa;
  socklen_t sl = sizeof(struct sockaddr_storage);
  ssize_t bytes = recvfrom(_fd, buffer, buffer_len, 0, reinterpret_cast<sockaddr*>(&sa), &sl);
  if (bytes == -1) {
    throw SystemError();
  }
  std::size_t return_size = static_cast<std::size_t>(bytes);
  return std::tuple<std::size_t, SocketAddress>(return_size, SocketAddress(sa));
}

}