extern "C" {
#include <sys/utsname.h>
#include <unistd.h>
}
#include "grund/system.hpp"
#include "grund/system_error.hpp"
#include <cstdio>

using namespace grund;

std::size_t System::get_page_size()
{
  long page_size = sysconf(_SC_PAGESIZE);
  return (size_t)page_size;
}

std::tuple<uint32_t, uint32_t, uint32_t> System::get_kernel_version()
{
  struct utsname info;
  if (uname(&info) != 0) {
    throw SystemError();
  }
  uint32_t a, b, c;
  if (sscanf(info.release, "%u.%u.%u", &a, &b, &c) == 3) {
    return std::tuple<uint32_t, uint32_t, uint32_t>(a, b, c);
  }
  return std::tuple<uint32_t, uint32_t, uint32_t>(0, 0, 0);
}
