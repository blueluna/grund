#include "grund/process_identifier_file.hpp"
#include <fstream>
#include <grund/process_information.hpp>

namespace grund {

ProcessIdentifierFile::ProcessIdentifierFile(std::filesystem::path& path)
  : _file_path(path)
{
}

uint64_t ProcessIdentifierFile::pid() const
{
  uint64_t other_pid { 0 };
  if (exists(_file_path)) {
    std::ifstream pid_file(_file_path);
    pid_file >> other_pid;
    pid_file.close();
  }
  return other_pid;
}

bool ProcessIdentifierFile::occupied() const
{
  auto id = pid();
  if (id == 0) {
    return false;
  }
  ProcessInformation self_info;
  ProcessInformation other_info(id);
  if (other_info.exists() && other_info.name() == self_info.name()) {
    return true;
  }
  // Process does not exists, remove pid file.
  std::filesystem::remove(_file_path);
  return false;
}

void ProcessIdentifierFile::write() const
{
  ProcessInformation self_info;
  std::ofstream pid_file;
  pid_file.open(_file_path);
  pid_file << static_cast<uint64_t>(self_info.pid());
  pid_file.close();
}

}