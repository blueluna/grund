extern "C" {
#include <signal.h>
#include <sys/signalfd.h>
}
#include <grund/signal_fd.hpp>
#include <grund/system_error.hpp>

using namespace grund;

int SignalFd::open(sigset_t& signal_mask, bool non_blocking)
{
  int fd = signalfd(-1, &signal_mask, (non_blocking ? SFD_NONBLOCK : 0) | SFD_CLOEXEC);
  if (fd < 0) {
    throw SystemError();
  }
  return fd;
}

SignalFd::SignalFd(sigset_t& signal_mask, bool non_blocking)
  : FileDescriptor(open(signal_mask, non_blocking))
{
}

SignalFd::SignalFd(int signal, bool non_blocking)
{
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, signal);
  _fd = open(mask, non_blocking);
}

int SignalFd::read_signal()
{
  if (!is_open()) {
    return 0;
  }
  struct signalfd_siginfo signal;
  try {
    signal = read_object<struct signalfd_siginfo>();
  }
  catch (SystemError& e) {
    if (e.error_code() == EAGAIN) {
      return 0;
    }
    else {
      throw e;
    }
  }
  return signal.ssi_signo;
}
