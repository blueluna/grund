#include <grund/dev_mem.hpp>
#include <grund/file_descriptor.hpp>
#include <string_view>

namespace grund {

static constexpr std::string_view dev_mem_path { "/dev/mem" };

MemoryMap DevMem::to_memory_map(size_t physical_address, size_t length)
{
  FileDescriptor fd = FileDescriptor::open(dev_mem_path, FileDescriptor::READ | FileDescriptor::WRITE | FileDescriptor::SYNC);
  return MemoryMap::map_read_write(fd.fd(), length, physical_address);
}

}
