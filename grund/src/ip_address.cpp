#include <cstring>
#include <utility>
extern "C" {
#include <arpa/inet.h>
#include <memory.h>
#include <sys/socket.h>
}

#include <grund/ip_address.hpp>

namespace grund {

const sa_family_t IpAddress::Ipv4 = AF_INET;
const sa_family_t IpAddress::Ipv6 = AF_INET6;

IpAddress::IpAddress(std::uint8_t a, std::uint8_t b, std::uint8_t c, std::uint8_t d)
  : _family(AF_INET)
  , _address {}
{
  _address.ipv4 = static_cast<std::uint32_t>(a) << 24 | static_cast<std::uint32_t>(b) << 16 | static_cast<std::uint32_t>(c) << 8 | static_cast<std::uint32_t>(d);
}

IpAddress::IpAddress(const in_addr_t addr)
  : _family(AF_INET)
  , _address {}
{
  _address.ipv4 = addr;
}

IpAddress::IpAddress(const struct in_addr addr)
  : _family(AF_INET)
  , _address {}
{
  _address.ipv4 = ntohl(addr.s_addr);
}

IpAddress::IpAddress(const struct in6_addr addr)
  : _family(AF_INET6)
  , _address {}
{
  std::memcpy(_address.ipv6, &addr, sizeof(struct in6_addr));
}

IpAddress::IpAddress(const struct sockaddr_storage& addr)
  : _family(addr.ss_family)
  , _address {}
{
  if (is_ipv4()) {
    auto sa = reinterpret_cast<const struct sockaddr_in*>(&addr);
    _address.ipv4 = ntohl(sa->sin_addr.s_addr);
  }
  else if (is_ipv6()) {
    auto sa = reinterpret_cast<const struct sockaddr_in6*>(&addr);
    std::memcpy(_address.ipv6, sa->sin6_addr.s6_addr, sizeof(struct sockaddr_in6));
  }
}

IpAddress::IpAddress(const std::string& addr)
  : IpAddress(addr.c_str())
{
}

IpAddress::IpAddress(const char* addr)
  : _family(AF_UNSPEC)
  , _address {}
{
  {
    struct in_addr ia = {};
    if (inet_pton(AF_INET, addr, &ia) == 1) {
      _family = AF_INET;
      _address.ipv4 = ntohl(ia.s_addr);
      return;
    }
  }
  {
    struct in6_addr ia = {};
    if (inet_pton(AF_INET6, addr, &ia) == 1) {
      _family = AF_INET6;
      std::memcpy(_address.ipv6, &ia, sizeof(struct in6_addr));
    }
  }
}

IpAddress::IpAddress(IpAddress&& other)
  : _family(std::exchange(other._family, AF_UNSPEC))
  , _address(std::move(other._address))
{
}

IpAddress::IpAddress(const IpAddress& other)
  : _family(other._family)
  , _address(other._address)
{
}

IpAddress& IpAddress::operator=(IpAddress&& other) noexcept
{
  _family = std::exchange(other._family, AF_UNSPEC);
  _address = std::move(other._address);
  return *this;
}

struct sockaddr_storage IpAddress::into_sockaddr_storage(uint16_t port) const
{
  struct sockaddr_storage addr = {};
  auto ptr = &addr;
  memset(ptr, 0, sizeof(struct sockaddr_storage));
  addr.ss_family = _family;
  if (is_ipv4()) {
    auto sa = reinterpret_cast<struct sockaddr_in*>(ptr);
    sa->sin_port = htons(port);
    into_in_addr(sa->sin_addr);
  }
  else if (is_ipv6()) {
    auto sa = reinterpret_cast<struct sockaddr_in6*>(ptr);
    sa->sin6_port = htons(port);
    std::memcpy(sa->sin6_addr.s6_addr, _address.ipv6, sizeof(struct in6_addr));
  }
  return addr;
}

bool IpAddress::into_in_addr(struct in_addr& sa) const
{
  if (is_ipv4()) {
    sa.s_addr = htonl(_address.ipv4);
    return true;
  }
  return false;
}

bool IpAddress::into_in6_addr(struct in6_addr& sa) const
{
  if (is_ipv6()) {
    std::memcpy(sa.s6_addr, _address.ipv6, sizeof(struct in6_addr));
    return true;
  }
  return false;
}

socklen_t IpAddress::socklen() const
{
  if (is_ipv4()) {
    return sizeof(struct sockaddr_in);
  }
  else if (is_ipv6()) {
    return sizeof(struct sockaddr_in6);
  }
  return 0;
}

std::string IpAddress::as_string() const
{
  char address[INET6_ADDRSTRLEN];
  const char* dst = nullptr;
  if (is_ipv4()) {
    struct in_addr a = {};
    a.s_addr = htonl(_address.ipv4);
    dst = inet_ntop(_family, &a, address, INET6_ADDRSTRLEN);
  }
  else if (is_ipv6()) {
    dst = inet_ntop(_family, &_address.ipv6, address, INET6_ADDRSTRLEN);
  }
  if (dst != nullptr) {
    return std::string { dst };
  }
  return std::string {};
}

} // namespace grund
