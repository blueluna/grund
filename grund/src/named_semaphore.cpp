#include <algorithm>
#include <chrono>
extern "C" {
#include <fcntl.h>
}
#include "grund/named_semaphore.hpp"
#include "grund/time.hpp"

namespace grund {

auto namedSemaphoreDeleter = [](sem_t* ptr) { ::sem_close(ptr); ptr = nullptr; };

std::string NamedSemaphore::fix_name(const std::string& name)
{
  if (name.length() < 1) {
    return std::string {};
  }
  std::string new_name { name };
  if (name[0] != '/') {
    new_name.insert(0, 1, '/');
  }
  auto begin = new_name.begin() + 1;
  std::replace(begin, new_name.end(), '/', '-');
  return new_name;
}

NamedSemaphore::NamedSemaphore(::sem_t* handle)
  : _handle { handle, namedSemaphoreDeleter }
{
}

NamedSemaphore::NamedSemaphore()
  : _handle { nullptr, namedSemaphoreDeleter }
{
}

NamedSemaphore::NamedSemaphore(NamedSemaphore&& other) noexcept
  : _handle(std::move(other._handle))
{
}

NamedSemaphore NamedSemaphore::create(const std::string& name, int value, int open_flags, mode_t mode)
{
  auto fixed_name = fix_name(name);
  auto ptr = ::sem_open(fixed_name.c_str(), open_flags, mode, value);
  if (SEM_FAILED == ptr) {
    throw std::system_error(errno, std::system_category());
  }
  return NamedSemaphore(ptr);
}

NamedSemaphore NamedSemaphore::create(const std::string& name, int value)
{
  return create(name, value, O_RDWR | O_EXCL | O_CREAT, 0660);
}

NamedSemaphore NamedSemaphore::open_create(const std::string& name, int value)
{
  return create(name, value, O_RDWR | O_CREAT, 0660);
}

NamedSemaphore NamedSemaphore::open(const std::string& name)
{
  auto fixed_name = fix_name(name);
  auto ptr = ::sem_open(fixed_name.c_str(), 0);
  if (SEM_FAILED == ptr) {
    throw std::system_error(errno, std::system_category());
  }
  return NamedSemaphore(ptr);
}

void NamedSemaphore::unlink(const std::string& name)
{
  ::sem_unlink(name.c_str());
}

void NamedSemaphore::post()
{
  if (!_handle) {
    throw std::system_error(ENODEV, std::system_category());
  }
  ::sem_post(_handle.get());
}

void NamedSemaphore::wait()
{
  if (!_handle) {
    throw std::system_error(ENODEV, std::system_category());
  }
  if (-1 == ::sem_wait(_handle.get())) {
    throw std::system_error(errno, std::system_category());
  }
}

bool NamedSemaphore::try_wait()
{
  if (!_handle) {
    throw std::system_error(ENODEV, std::system_category());
  }
  if (-1 == ::sem_trywait(_handle.get())) {
    int error_code = errno;
    if (error_code == EAGAIN) {
      return false;
    }
    throw std::system_error(error_code, std::system_category());
  }
  return true;
}

bool NamedSemaphore::wait(std::chrono::nanoseconds timeout)
{
  if (!_handle) {
    throw std::system_error(ENODEV, std::system_category());
  }
  // sem_timedwait takes absolute time
  const auto now = std::chrono::system_clock::now().time_since_epoch();
  auto end_time = now + timeout;
  timespec ts = timespec_from_duration(end_time);
  if (-1 == ::sem_timedwait(_handle.get(), &ts)) {
    int error_code = errno;
    if (error_code == ETIMEDOUT) {
      return false;
    }
    throw std::system_error(error_code, std::system_category());
  }
  return true;
}

int NamedSemaphore::value()
{
  if (!_handle) {
    throw std::system_error(ENODEV, std::system_category());
  }
  int value;
  if (-1 == ::sem_getvalue(_handle.get(), &value)) {
    throw std::system_error(errno, std::system_category());
  }
  return value;
}

}
