extern "C" {
#include <sys/timerfd.h>
#include <unistd.h>
}

#include <grund/system_error.hpp>
#include <grund/time.hpp>
#include <grund/timer_fd.hpp>

using namespace grund;

TimerFd::TimerFd(bool non_blocking)
  : FileDescriptor(timerfd_create(CLOCK_BOOTTIME, TFD_CLOEXEC | (non_blocking ? TFD_NONBLOCK : 0)))
{
}

void TimerFd::arm(const std::chrono::nanoseconds duration, bool periodic)
{
  struct itimerspec its = {};
  its.it_value = timespec_from_duration(duration);
  if (periodic) {
    its.it_interval = its.it_value;
  }
  else {
    its.it_interval.tv_sec = 0;
    its.it_interval.tv_nsec = 0;
  }
  if (timerfd_settime(fd(), 0, &its, nullptr) == -1) {
    throw SystemError();
  }
}

void TimerFd::clear()
{
  // Need to read the file descriptor to clear the event
  uint64_t value = 0;
  read_object(value);
}
