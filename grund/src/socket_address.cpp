#include <cassert>
#include <cstring>
#include <fmt/core.h>
#include <utility>
extern "C" {
#include <arpa/inet.h>
#include <linux/netlink.h>
#include <netdb.h> // getaddrinfo
#include <sys/socket.h>
#include <sys/types.h>
}

#include <grund/convert.hpp>
#include <grund/socket_address.hpp>
#include <grund/system_error.hpp>

namespace grund {

struct sockaddr_storage netlink_address(uint32_t pid, uint32_t groups)
{
  sockaddr_storage storage = {};
  auto nla = reinterpret_cast<struct sockaddr_nl*>(&storage);
  nla->nl_family = AF_NETLINK;
  nla->nl_pad = 0;
  nla->nl_pid = pid;
  nla->nl_groups = groups;
  return storage;
}

SocketAddress::SocketAddress()
  : _storage {}
{
  _storage.ss_family = AF_UNSPEC;
}

SocketAddress::SocketAddress(const struct sockaddr_storage addr)
  : _storage(addr)
{
}

SocketAddress::SocketAddress(const IpAddress& address, const uint16_t port)
  : _storage(address.into_sockaddr_storage(port))
{
}

SocketAddress::SocketAddress(SocketAddress&& other) noexcept
  : _storage(std::move(other._storage))
{
  other._storage.ss_family = AF_UNSPEC;
}

SocketAddress SocketAddress::netlink(uint32_t pid, uint32_t groups)
{
  return SocketAddress { netlink_address(pid, groups) };
}

SocketAddress& SocketAddress::operator=(SocketAddress&& other) noexcept
{
  _storage = std::move(other._storage);
  other._storage.ss_family = AF_UNSPEC;
  return *this;
}

struct sockaddr_storage SocketAddress::into_sockaddr_storage() const
{
  return _storage;
}

bool SocketAddress::is_ipv4() const
{
  return _storage.ss_family == IpAddress::Ipv4;
}

bool SocketAddress::is_ipv6() const
{
  return _storage.ss_family == IpAddress::Ipv6;
}

bool SocketAddress::is_netlink() const
{
  return _storage.ss_family == AF_NETLINK;
}

socklen_t SocketAddress::socklen() const
{
  if (is_ipv4()) {
    return sizeof(struct sockaddr_in);
  }
  else if (is_ipv6()) {
    return sizeof(struct sockaddr_in6);
  }
  else if (is_netlink()) {
    return sizeof(struct sockaddr_nl);
  }
  return sizeof(struct sockaddr_storage);
}

IpAddress SocketAddress::ip_address() const
{
  if (is_ipv4()) {
    auto sa = reinterpret_cast<const struct sockaddr_in*>(&_storage);
    return IpAddress(sa->sin_addr);
  }
  else if (is_ipv6()) {
    auto sa = reinterpret_cast<const struct sockaddr_in6*>(&_storage);
    return IpAddress(sa->sin6_addr);
  }
  throw SystemError(EADDRNOTAVAIL);
}

uint16_t SocketAddress::port() const
{
  if (is_ipv4()) {
    auto sa = reinterpret_cast<const struct sockaddr_in*>(&_storage);
    return ntohs(sa->sin_port);
  }
  else if (is_ipv6()) {
    auto sa = reinterpret_cast<const struct sockaddr_in6*>(&_storage);
    return ntohs(sa->sin6_port);
  }
  return 0;
}

std::string SocketAddress::as_string() const
{
  char address[INET6_ADDRSTRLEN];
  const char* dst = nullptr;
  uint16_t port = 0;
  if (is_ipv4()) {
    auto sa = reinterpret_cast<const struct sockaddr_in*>(&_storage);
    port = ntohs(sa->sin_port);
    dst = inet_ntop(sa->sin_family, &sa->sin_addr, address, INET6_ADDRSTRLEN);
  }
  else if (is_ipv6()) {
    auto sa = reinterpret_cast<const struct sockaddr_in6*>(&_storage);
    port = ntohs(sa->sin6_port);
    dst = inet_ntop(sa->sin6_family, &sa->sin6_addr, address, INET6_ADDRSTRLEN);
  }
  else if (is_netlink()) {
    auto nla = reinterpret_cast<const struct sockaddr_nl*>(&_storage);
    return fmt::format("{}:{}", nla->nl_pid, nla->nl_groups);
  }
  if (dst != nullptr) {
    return fmt::format("{}:{}", dst, port);
  }
  return std::string {};
}

SocketAddress SocketAddress::lookup(const char* address, const char* port, const struct addrinfo& hints)
{
  struct addrinfo* results;
  int gai_result = getaddrinfo(address, port, &hints, &results);
  if (gai_result != 0 || results == nullptr) {
    throw SystemError(EADDRNOTAVAIL);
  }
  struct sockaddr_storage sockaddr;
  switch (results->ai_family) {
  case AF_INET:
    {
      assert(sizeof(struct sockaddr_in) == results->ai_addrlen);
      std::memcpy(&sockaddr, results->ai_addr, results->ai_addrlen);
    }
    break;
  case AF_INET6:
    {
      assert(sizeof(struct sockaddr_in6) == results->ai_addrlen);
      std::memcpy(&sockaddr, results->ai_addr, results->ai_addrlen);
    }
    break;
  default:
    throw SystemError(EADDRNOTAVAIL);
  }
  freeaddrinfo(results);
  return SocketAddress(sockaddr);
}

SocketAddress SocketAddress::lookup_client(const sa_family_t family, const char* address, const char* port)
{
  if (address == nullptr) {
    throw SystemError(EINVAL);
  }
  struct addrinfo hints = {
    .ai_flags = AI_NUMERICSERV,
    .ai_family = family,
    .ai_socktype = 0,
    .ai_protocol = 0,
    .ai_addrlen = 0,
    .ai_addr = nullptr,
    .ai_canonname = nullptr,
    .ai_next = nullptr,
  };
  return lookup(address, port, hints);
}

SocketAddress SocketAddress::lookup_server(const sa_family_t family, const char* address, const char* port)
{
  if (address == nullptr && port == nullptr) {
    throw SystemError(EINVAL);
  }
  struct addrinfo hints = {
    .ai_flags = AI_PASSIVE | AI_NUMERICSERV,
    .ai_family = family,
    .ai_socktype = 0,
    .ai_protocol = 0,
    .ai_addrlen = 0,
    .ai_addr = nullptr,
    .ai_canonname = nullptr,
    .ai_next = nullptr,
  };
  return lookup(address, port, hints);
}

SocketAddress SocketAddress::lookup_client(const char* address, const char* port)
{
  return lookup_client(AF_UNSPEC, address, port);
}

SocketAddress SocketAddress::lookup_server(const char* address, const char* port)
{
  return lookup_server(AF_UNSPEC, address, port);
}

} // namespace grund
