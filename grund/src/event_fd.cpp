extern "C" {
#include <signal.h>
#include <sys/eventfd.h>
#include <unistd.h>
}
#include <grund/event_fd.hpp>
#include <grund/system_error.hpp>

using namespace grund;

int _create_eventfd(int initial_value, int flags)
{
  int fd = eventfd(initial_value, flags | EFD_CLOEXEC | EFD_NONBLOCK);
  if (fd < 0) {
    throw SystemError();
  }
  return fd;
}

EventFd::EventFd()
  : FileDescriptor(_create_eventfd(0, 0))
{
}

void EventFd::emit(uint64_t addition)
{
  // Add the count to the file descriptor
  if (::write(fd(), (void*)&addition, sizeof(uint64_t)) == -1) {
    throw SystemError();
  }
}

void EventFd::emit()
{
  emit(1);
}

uint64_t EventFd::read_event()
{
  uint64_t value = 0;
  if (::read(fd(), (void*)&value, sizeof(uint64_t)) == -1) {
    // Do not report an error for timeout since operations are non-blocking
    if (errno == EAGAIN) {
      return 0;
    }
    else {
      throw SystemError();
    }
  }
  return value;
}

EventFdSemaphore::EventFdSemaphore()
  : FileDescriptor(_create_eventfd(0, EFD_SEMAPHORE))
{
}

void EventFdSemaphore::emit(uint64_t addition)
{
  // Add the count to the file descriptor
  if (::write(fd(), (void*)&addition, sizeof(uint64_t)) == -1) {
    throw SystemError();
  }
}

void EventFdSemaphore::emit()
{
  emit(1);
}

bool EventFdSemaphore::read_event()
{
  uint64_t value = 0;
  // In semaphore mode the counter is decreased with one for each read
  if (::read(fd(), (void*)&value, sizeof(uint64_t)) == -1) {
    // Do not report an error for timeout since operations are non-blocking
    if (errno == EAGAIN) {
      value = 0;
    }
    else {
      throw SystemError();
    }
  }
  return value == 1;
}
