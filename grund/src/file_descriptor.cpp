extern "C" {
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
}
#include <cstdlib>
#include <grund/file_descriptor.hpp>
#include <grund/string.hpp>
#include <grund/system_error.hpp>

namespace grund {

int FileDescriptor::open_flags(const FileDescriptor::OpenMode mode)
{
  int flags = O_CLOEXEC;
  static constexpr FileDescriptor::OpenMode READ_WRITE = FileDescriptor::OpenMode::READ | FileDescriptor::OpenMode::WRITE;
  if ((mode & READ_WRITE) == READ_WRITE) {
    flags |= O_RDWR;
  }
  else if ((mode & FileDescriptor::OpenMode::READ) == FileDescriptor::OpenMode::READ) {
    flags |= O_RDONLY;
  }
  else if ((mode & FileDescriptor::OpenMode::WRITE) == FileDescriptor::OpenMode::WRITE) {
    flags |= O_WRONLY;
  }
  if ((mode & FileDescriptor::OpenMode::NON_BLOCKING) == FileDescriptor::OpenMode::NON_BLOCKING) {
    flags |= O_NONBLOCK;
  }
  if ((mode & FileDescriptor::OpenMode::SYNC) == FileDescriptor::OpenMode::SYNC) {
    flags |= O_SYNC;
  }
  return flags;
}

FileDescriptor::FileDescriptor(const std::string_view& path, FileDescriptor::OpenMode flags)
{
  int fd = ::open(path.data(), open_flags(flags));
  if (fd < 0) [[unlikely]] {
    throw SystemError();
  }
  _fd = fd;
}

FileDescriptor::FileDescriptor(const std::filesystem::path& path, FileDescriptor::OpenMode flags)
  : FileDescriptor(std::string_view { path.string() }, flags)
{
}

FileDescriptor FileDescriptor::open_read(const std::string_view& path)
{
  return open(path, OpenMode::READ);
}

FileDescriptor FileDescriptor::open(const std::string_view& path, const FileDescriptor::OpenMode mode)
{
  return FileDescriptor(path, mode);
}

FileDescriptor FileDescriptor::open(const std::filesystem::path& path, const FileDescriptor::OpenMode mode)
{
  return FileDescriptor(path, mode);
}

FileDescriptor FileDescriptor::create_temporary_file()
{
  std::string template_path { "/tmp/_temporary_XXXXXX" };
  int fd = mkstemp(template_path.data());
  if (fd < 0) {
    throw SystemError();
  }
  return FileDescriptor(fd);
}

std::tuple<FileDescriptor, FileDescriptor> FileDescriptor::create_pipe(bool blocking)
{
  int pipe_fd[2];
  if (pipe2(pipe_fd, blocking ? 0 : O_NONBLOCK) < 0) {
    throw SystemError();
  }
  return { FileDescriptor(pipe_fd[0]), FileDescriptor(pipe_fd[1]) };
}

void FileDescriptor::close()
{
  if (is_open()) {
    ::close(_fd);
  }
  _fd = INVALID;
}

ssize_t FileDescriptor::read(void* data, size_t data_size)
{
  ssize_t count = ::read(_fd, data, data_size);
  if (-1 == count) {
    return -errno;
  }
  return count;
}

std::optional<size_t> FileDescriptor::read_exception_non_blocking(void* data, size_t data_size)
{
  ssize_t count = read(data, data_size);
  if (count > 0) {
    return count;
  }
  else if (count == -EAGAIN) {
    return {};
  }
  else {
    throw SystemError(-count);
  }
}

size_t FileDescriptor::read_exception(void* data, size_t data_size)
{
  ssize_t count = ::read(_fd, data, data_size);
  if (-1 == count) [[unlikely]] {
    throw SystemError();
  }
  return static_cast<size_t>(count);
}

void FileDescriptor::read_exact(void* data, size_t data_size)
{
  auto count = read_exception(data, data_size);
  if (count != data_size) [[unlikely]] {
    throw SystemError(EBADMSG);
  }
}

bool FileDescriptor::read_exact_non_blocking(void* data, size_t data_size)
{
  auto count = read_exception_non_blocking(data, data_size);
  if (!count.has_value()) {
    return false;
  }
  else if (count != data_size) [[unlikely]] {
    throw SystemError(EBADMSG);
  }
  return true;
}

ssize_t FileDescriptor::read_string(std::string& string)
{
  return read(string.data(), string.size());
}

ssize_t FileDescriptor::write(const void* data, size_t data_size)
{
  ssize_t count = ::write(_fd, data, data_size);
  if (-1 == count) [[unlikely]] {
    return -errno;
  }
  return count;
}

size_t FileDescriptor::write_exception(const void* data, size_t data_size)
{
  ssize_t count = write(data, data_size);
  if (-1 == count) [[unlikely]] {
    throw SystemError();
  }
  return static_cast<size_t>(count);
}

void FileDescriptor::write_exact(const void* data, size_t data_size)
{
  size_t count = write_exception(data, data_size);
  if (count != data_size) [[unlikely]] {
    throw SystemError(EBADMSG);
  }
}

ssize_t FileDescriptor::write_string(const std::string& string)
{
  return write(string.data(), string.size());
}

int FileDescriptor::ioctl(unsigned long request)
{
  int result = ::ioctl(_fd, request);
  if (-1 == result) {
    throw SystemError();
  }
  return result;
}

int FileDescriptor::ioctl(unsigned long request, void* data)
{
  int result = ::ioctl(_fd, request, data);
  if (-1 == result) {
    throw SystemError();
  }
  return result;
}

std::string read_string(const std::string_view& path, ssize_t size_hint)
{
  FileDescriptor fd = FileDescriptor::open_read(path);
  std::string string(size_hint, '\0');
  ssize_t length = fd.read_string(string);
  if (length < 0) [[unlikely]] {
    throw SystemError();
  }
  string.resize(length);
  return string;
}

std::string read_string(const std::filesystem::path& path, ssize_t size_hint)
{
  return read_string(std::string_view { path.string() }, size_hint);
}

std::string read_string_rtrim(const std::filesystem::path& path, ssize_t size_hint)
{
  return grund::rtrim(read_string(std::string_view { path.string() }, size_hint));
}

}