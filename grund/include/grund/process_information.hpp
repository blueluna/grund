#include <cstdint>
#include <filesystem>
#include <fmt/core.h>
#include <grund/convert.hpp>
#include <grund/file_descriptor.hpp>
#include <grund/string.hpp>

#ifndef GRUND_PROCESS_INFORMATION_HPP
#define GRUND_PROCESS_INFORMATION_HPP

namespace grund {

class ProcessInformation {
public:
  enum class State {
    Unknown,
    Running,
    Sleeping,
    UninterruptibleWait,
    Zombie,
    Stopped,
  };

  explicit constexpr ProcessInformation()
    : _pid(0)
  {
  }

  explicit constexpr ProcessInformation(uint64_t pid)
    : _pid(pid)
  {
  }

  uint64_t pid() const
  {
    if (_pid == 0) {
      auto parts = proc_stat_parts();
      if (parts.size() < 1) {
        return 0;
      }
      return grund::from_string<uint64_t>(parts[0]);
    }
    else {
      return _pid;
    }
  }

  bool exists() const
  {
    return std::filesystem::is_directory(proc_path());
  }
  std::filesystem::path path() const
  {
    auto parts = proc_cmdline_parts();
    if (parts.size() < 1) {
      return {};
    }
    return std::filesystem::path(parts[0]);
  }

  std::string name() const
  {
    return path().filename().string();
  }

  State state() const
  {
    auto parts = proc_stat_parts();
    if (parts.size() < 3) {
      return State::Unknown;
    }
    std::string process_state = parts[2];
    if (process_state.starts_with('R')) {
      return State::Running;
    }
    else if (process_state.starts_with('S')) {
      return State::Sleeping;
    }
    else if (process_state.starts_with('D')) {
      return State::UninterruptibleWait;
    }
    else if (process_state.starts_with('Z')) {
      return State::Zombie;
    }
    else if (process_state.starts_with('T')) {
      return State::Stopped;
    }
    else {
      return State::Unknown;
    }
  }

protected:
  std::filesystem::path proc_path() const
  {
    if (_pid == 0) {
      return std::filesystem::path("/proc/self");
    }
    else {
      return std::filesystem::path(fmt::format("/proc/{}", _pid));
    }
  }

  const std::string proc_cmdline() const
  {
    if (!exists()) {
      return {};
    }
    auto proc_cmdline_path = proc_path() / "cmdline";
    return grund::read_string_rtrim(proc_cmdline_path, 512);
  }

  std::vector<std::string> proc_cmdline_parts() const
  {
    auto cmdline = proc_cmdline();
    if (cmdline.empty()) {
      return {};
    }
    return split(cmdline, '\0');
  }

  const std::string proc_stat() const
  {
    if (!exists()) {
      return {};
    }
    auto proc_stat_path = proc_path() / "stat";
    return grund::read_string_rtrim(proc_stat_path, 512);
  }

  std::vector<std::string> proc_stat_parts() const
  {
    auto stat = proc_stat();
    if (stat.empty()) {
      return {};
    }
    return split(stat, ' ');
  }

protected:
  const uint64_t _pid;
};

}

#endif // GRUND_PROCESS_INFORMATION_HPP
