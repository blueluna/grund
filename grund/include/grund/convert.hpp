#include <cerrno>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <stdexcept>
#include <string>

#ifndef GRUND_CONVERT_HPP
#define GRUND_CONVERT_HPP

namespace grund {

/// Throw exception if the provided value is out of bounds of the desired type T
template <typename T>
inline void check_out_of_bounds(int64_t value)
{
  int64_t min = static_cast<int64_t>(std::numeric_limits<T>::min());
  int64_t max = static_cast<int64_t>(std::numeric_limits<T>::max());
  if ((value < min) || (value > max)) [[unlikely]] {
    throw std::invalid_argument("parsed value out of bounds");
  }
}

/// Convert string into value T
template <typename T>
inline T from_string(const std::string& string)
{
  (void)string;
  throw std::invalid_argument("Unimplemented");
}

/// Convert string to int64_t
template <>
inline int64_t from_string(const std::string& string)
{
  if (string.empty()) {
    throw std::invalid_argument("cannot parse empty string");
  }
  std::size_t end;
  int64_t v;
  try {
    v = std::stoll(string, &end, 0);
  }
  catch (std::exception&) {
    throw std::invalid_argument("cannot parse string");
  }
  if (string.size() != end) {
    throw std::invalid_argument("cannot parse string with extra characters");
  }
  return static_cast<int64_t>(v);
}

/// Convert string to int32_t
template <>
inline int32_t from_string(const std::string& string)
{
  int64_t v = from_string<int64_t>(string);
  check_out_of_bounds<int32_t>(v);
  return static_cast<int32_t>(v);
}

/// Convert string to int16_t
template <>
inline int16_t from_string(const std::string& string)
{
  int64_t v = from_string<int64_t>(string);
  check_out_of_bounds<int16_t>(v);
  return static_cast<int16_t>(v);
}

/// Convert string to int8_t
template <>
inline int8_t from_string(const std::string& string)
{
  int64_t v = from_string<int64_t>(string);
  check_out_of_bounds<int8_t>(v);
  return static_cast<int8_t>(v);
}

/// Convert string to uint64_t
template <>
inline uint64_t from_string(const std::string& string)
{
  if (string.empty()) {
    throw std::invalid_argument("cannot parse empty string");
  }
  if (string.starts_with('-')) {
    throw std::invalid_argument("parsed value out of bounds");
  }
  std::size_t end;
  long long v;
  try {
    v = std::stoull(string, &end, 0);
  }
  catch (std::exception&) {
    throw std::invalid_argument("cannot parse string");
  }
  if (string.size() != end) {
    throw std::invalid_argument("cannot parse string with extra characters");
  }
  return static_cast<uint64_t>(v);
}

/// Convert string to uint32_t
template <>
inline uint32_t from_string(const std::string& string)
{
  int64_t v = from_string<int64_t>(string);
  check_out_of_bounds<uint32_t>(v);
  return static_cast<uint32_t>(v);
}

/// Convert string to uint16_t
template <>
inline uint16_t from_string(const std::string& string)
{
  int64_t v = from_string<int64_t>(string);
  check_out_of_bounds<uint16_t>(v);
  return static_cast<uint16_t>(v);
}

/// Convert string to uint8_t
template <>
inline uint8_t from_string(const std::string& string)
{
  int64_t v = from_string<int64_t>(string);
  check_out_of_bounds<uint8_t>(v);
  return static_cast<uint8_t>(v);
}

/// Convert string to double
template <>
inline double from_string(const std::string& string)
{
  if (string.empty()) {
    throw std::invalid_argument("cannot parse empty string");
  }
  std::size_t end;
  double v;
  try {
    v = std::stod(string, &end);
  }
  catch (std::exception&) {
    throw std::invalid_argument("cannot parse string");
  }
  if (string.size() != end) {
    throw std::invalid_argument("cannot parse string with extra characters");
  }
  return v;
}

/// Convert string to float
template <>
inline float from_string(const std::string& string)
{
  if (string.empty()) {
    throw std::invalid_argument("cannot parse empty string");
  }
  std::size_t end;
  float v;
  try {
    v = std::stof(string, &end);
  }
  catch (std::exception&) {
    throw std::invalid_argument("cannot parse string");
  }
  if (string.size() != end) {
    throw std::invalid_argument("cannot parse string with extra characters");
  }
  return v;
}

} // namespace grund

#endif // GRUND_CONVERT_HPP
