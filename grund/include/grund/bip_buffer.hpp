/// Bi-partite circular buffer based on BBQueue
///
/// Information about Bi-partite circular buffer
/// https://www.codeproject.com/articles/3479/the-bip-buffer-the-circular-buffer-with-a-twist
/// https://ferrous-systems.com/blog/lock-free-ring-buffer/
///
/// Reference, BBQueue
/// https://github.com/jamesmunns/bbqueue
///
#include <array>
#include <atomic>
#include <cassert>
#include <optional>
#include <span>
#include <tuple>

#ifndef GRUND_BIP_BUFFER_HPP
#define GRUND_BIP_BUFFER_HPP

namespace grund {

#if defined(__aarch64__) && defined(__APPLE__)
// Apple M1 / M2 ?
constexpr int CACHE_LINE_SIZE = 128;
#else
constexpr int CACHE_LINE_SIZE = 64;
#endif

/// Bi-partite circular buffer
template <std::size_t N>
class BipBuffer {
public:
  explicit BipBuffer()
    : _i_read(0)
    , _i_write(0)
    , _i_last(0)
    , _i_reserve(0)
    , _data_view { _data }
    , _read_in_progress(false)
    , _write_in_progress(false)
  {
    _data.fill(0);
  }

  /// Request a write grant
  ///
  /// Grant a write buffer with up to requested number of bytes depending
  /// on how much is left in the buffer.
  ///
  /// \param requested Number of bytes requested
  /// \return Optional span of the underlying buffer, If a grant couln't be
  /// fulfilled the method returns None.
  std::optional<std::span<uint8_t>> grant(const size_t requested)
  {
    bool expected = false;
    if (!_write_in_progress.compare_exchange_strong(expected, true, std::memory_order_acq_rel)) {
      // in progress
      return {};
    }
    /* Preload variables with adequate memory ordering */
    const std::size_t write = _i_write.load(std::memory_order_acquire);
    const std::size_t read = _i_read.load(std::memory_order_acquire);

    size_t start = 0;
    size_t granted = 0;
    std::tie(start, granted) = inner_grant(read, write, requested);

    if (granted == 0) {
      _write_in_progress.store(false, std::memory_order_release);
      return {};
    }
    _i_reserve.store(start + granted, std::memory_order_release);
    return _data_view.subspan(start, granted);
  }

  /// Request a write grant with exact size
  ///
  /// Grant a write buffer with exactly the number of requested bytes.
  ///
  /// \param requested Number of bytes requested
  /// \return Optional span of the underlying buffer, If a grant couln't be
  /// fulfilled the method returns None.
  std::optional<std::span<uint8_t>> grant_exact(const size_t requested)
  {
    bool expected = false;
    if (!_write_in_progress.compare_exchange_strong(expected, true, std::memory_order_acq_rel)) {
      // in progress
      return {};
    }
    /* Preload variables with adequate memory ordering */
    const std::size_t write = _i_write.load(std::memory_order_acquire);
    const std::size_t read = _i_read.load(std::memory_order_acquire);

    size_t start = 0;
    size_t granted = 0;
    std::tie(start, granted) = inner_grant(read, write, requested);

    if (granted < requested) {
      _write_in_progress.store(false, std::memory_order_release);
      return {};
    }
    _i_reserve.store(start + granted, std::memory_order_release);
    return _data_view.subspan(start, granted);
  }

  void commit(std::span<uint8_t>& buffer, const size_t written)
  {
    if (!_write_in_progress.load(std::memory_order_acquire)) {
      return;
    }
    size_t len = buffer.size();
    size_t used = std::min(len, written);

    /* Preload variables with adequate memory ordering */
    std::size_t write = _i_write.load(std::memory_order_acquire);
    _i_reserve.fetch_sub(len - used, std::memory_order_acq_rel);

    std::size_t last = _i_last.load(std::memory_order_acquire);
    std::size_t new_write = _i_reserve.load(std::memory_order_acquire);

    if ((new_write < write) && (write != N)) {
      // We have already wrapped, but we are skipping some bytes at the end of the ring.
      // Mark `last`, where the write pointer used to be to hold the line here
      _i_last.store(write, std::memory_order_release);
    }
    else if (new_write > last) {
      // We're about to pass the last pointer, which was previously the artificial
      // end of the ring. Now that we've passed it, we can "unlock" the section
      // that was previously skipped.
      //
      // Since new_write is strictly larger than last, it is safe to move this as
      // the other thread will still be halted by the (about to be updated) write
      // value
      _i_last.store(N, std::memory_order_release);
    }
    // else: If new_write == last, either:
    // * last == max, so no need to write, OR
    // * If we write in the end chunk again, we'll update last to max next time
    // * If we write to the start chunk in a wrap, we'll update last when we
    //     move write backwards

    // Write must be updated AFTER last, otherwise read could think it was
    // time to invert early!
    _i_write.store(new_write, std::memory_order_release);

    // Allow subsequent grants
    _write_in_progress.store(false, std::memory_order_release);
  }

  std::optional<const std::span<uint8_t>> read()
  {
    bool expected = false;
    if (!_read_in_progress.compare_exchange_strong(expected, true, std::memory_order_acq_rel)) {
      // in progress
      return {};
    }
    /* Preload variables with adequate memory ordering */
    const std::size_t write = _i_write.load(std::memory_order_acquire);
    const std::size_t last = _i_last.load(std::memory_order_acquire);
    std::size_t read = _i_read.load(std::memory_order_acquire);

    // Resolve the inverted case or end of read
    if ((read == last) && (write < read)) {
      read = 0;
      // This has some room for error, the other thread reads this
      // Impact to Grant:
      //   Grant checks if read < write to see if inverted. If not inverted, but
      //     no space left, Grant will initiate an inversion, but will not trigger it
      // Impact to Commit:
      //   Commit does not check read, but if Grant has started an inversion,
      //   grant could move Last to the prior write position
      // MOVING READ BACKWARDS!
      _i_read.store(0, std::memory_order_release);
    }

    size_t size;
    if (write < read) {
      // Inverted, only believe last
      size = last;
    }
    else {
      // Not inverted, only believe write
      size = write;
    }
    size -= read;

    if (size == 0) {
      _read_in_progress.store(false, std::memory_order_release);
      return {};
    }

    /* There is some data until the invalidate index */
    return _data_view.subspan(read, size);
  }

  void release(const std::span<uint8_t>& buffer, const std::size_t used)
  {
    if (!_read_in_progress.load(std::memory_order_acquire)) {
      return;
    }

    // This should always be checked by the public interfaces
    assert(used <= buffer.size());
    size_t buffer_size = buffer.size();
    size_t s = std::min(used, buffer_size);

    // This should be fine, purely incrementing
    _i_read.fetch_add(s, std::memory_order_release);
    _read_in_progress.store(false, std::memory_order_release);
  }

protected:
  /// Owned by the Reader, "private"
  alignas(CACHE_LINE_SIZE) std::atomic_size_t _i_read;
  /// Owned by the Writer, "private"
  alignas(CACHE_LINE_SIZE) std::atomic_size_t _i_write;
  /// Shared
  alignas(CACHE_LINE_SIZE) std::atomic_size_t _i_last;
  /// Owned by the Writer, "private"
  alignas(CACHE_LINE_SIZE) std::atomic_size_t _i_reserve;
  /// Shared
  alignas(CACHE_LINE_SIZE) std::array<uint8_t, N> _data;
  /// Shared
  std::span<uint8_t, N> _data_view;
  /// Owned by the Reader, "private"
  std::atomic_bool _read_in_progress;
  /// Owned by the Writer, "private"
  std::atomic_bool _write_in_progress;

  inline std::tuple<size_t, size_t> inner_grant(const size_t read, const size_t write, const size_t requested)
  {
    size_t start = 0;
    size_t granted = 0;
    if (write < read) {
      // In inverted case, read is always > write
      size_t remaining = read - write - 1;

      if (remaining != 0) {
        granted = std::min(remaining, requested);
        start = write;
      }
    }
    else {
      if (write != N) {
        // Some (or all) room remaining in un-inverted case
        granted = std::min(N - write, requested);
        start = write;
      }
      else {
        // Not inverted, but need to go inverted

        // NOTE: We check read > 1, NOT read >= 1, because
        // write must never == read in an inverted condition, since
        // we will then not be able to tell if we are inverted or not
        if (read > 1) {
          granted = std::min(read - 1, requested);
          start = 0;
        }
      }
    }
    return { start, granted };
  }
};

}
#endif // GRUND_BIP_BUFFER_HPP
