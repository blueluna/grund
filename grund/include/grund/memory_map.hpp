#include <cstdint>
#include <cstdio>
#include <stdexcept>
extern "C" {
#include <sys/mman.h>
}
#include <grund/bits.hpp>

#ifndef GRUND_MEMORY_MAP_HPP
#define GRUND_MEMORY_MAP_HPP

namespace grund {

class MemoryMap {
public:
  /// Uninitialized constructor
  MemoryMap();

  /// Move constructor
  MemoryMap(MemoryMap&& other);

  /// Destructor
  ~MemoryMap();

  /// Is memory mapped
  explicit operator bool() const noexcept { return _memory != nullptr; }

  /// Move assignment
  MemoryMap& operator=(MemoryMap&& other);

  /// Memory map file for reading
  static MemoryMap map_read(int fd, size_t size, size_t offset = 0);

  /// Memory map file for reading and writing
  static MemoryMap map_read_write(int fd, size_t size, size_t offset = 0);

  /// Get pointer to the byte offset
  ///
  /// \param offset byte offset
  /// \return Pointer to the position in memory or nullptr
  const uint8_t* operator[](size_t offset);

  /// Get pointer to the byte offset
  ///
  /// \tparam T requested type
  /// \param pos Position in memory in strides of bytes
  /// \return Pointer to the position in memory or nullptr
  template <typename T>
  const T* offset_bytes(size_t pos);

  /// Get pointer to the typed offset
  ///
  /// \tparam T requested type
  /// \param pos Position in memory in strides of type size
  /// \return Pointer to the position in memory or nullptr
  template <typename T>
  const T* offset(size_t pos);

  /// Get mutable pointer to the byte offset
  ///
  /// \tparam T requested type
  /// \param pos Position in memory in strides of bytes
  /// \return Pointer to the position in memory or nullptr
  template <typename T>
  T* offset_bytes_mut(size_t pos);

  /// Get mutable pointer to the typed offset
  ///
  /// \tparam T requested type
  /// \param pos Position in memory in strides of type size
  /// \return Pointer to the position in memory or nullptr
  template <typename T>
  T* offset_mut(size_t pos);

  /// Flush memory region, blocking flush
  ///
  /// \param offset offset in bytes to region
  /// \param length length in bytes of region
  void flush(size_t offset, size_t length);

  /// Flush memory region, schedule a flush and return immediately
  ///
  /// \param offset offset in bytes to region
  /// \param length length in bytes of region
  void flush_async(size_t offset, size_t length);

  /// Get value at offset
  ///
  /// \tparam T value type
  /// \param offset offset in memory in strides of type size
  /// \return value or std::invalid_argument exception
  template <typename T>
  const T get(size_t offset);

  /// Set value at offset
  ///
  /// \tparam T value type
  /// \param offset offset in memory in strides of type size
  /// \param value value to set
  /// \return std::invalid_argument exception on error
  template <typename T>
  void set(size_t offset, const T& value);

  /// Modify value at offset, with mask
  ///
  /// Read the register, mask out some bits and write to the register
  ///
  /// \tparam T value type
  /// \param offset offset in memory in strides of type size
  /// \param value value to set
  /// \param mask mask used to mask out bits in the value
  /// \return std::invalid_argument exception on error
  template <typename T>
  void modify(size_t offset, const T& value, const T& mask);

  /// Get value at offset
  ///
  /// \tparam T value type
  /// \param offset offset in memory in strides of type size
  /// \return value or std::invalid_argument exception
  template <typename T>
  const T get_stride(size_t offset);

  /// Set value at offset
  ///
  /// \tparam T value type
  /// \param offset offset in memory in strides of type size
  /// \param value value to set
  /// \return std::invalid_argument exception on error
  template <typename T>
  void set_stride(size_t offset, const T& value);

  /// Modify value at offset, with mask
  ///
  /// Read the register, mask out some bits and write to the register
  ///
  /// \tparam T value type
  /// \param offset offset in memory in strides of type size
  /// \param value value to set
  /// \param mask mask used to mask out bits in the value
  /// \return std::invalid_argument exception on error
  template <typename T>
  void modify_stride(size_t offset, const T& value, const T& mask);

  /// Get pointer to underlying memory with byte offset
  ///
  /// \return Pointer to the memory or nullptr
  const uint8_t* pointer();

  /// Get pointer to underlying memory
  ///
  /// \return Pointer to the memory or nullptr
  uint8_t* pointer_mut();

protected:
  uint8_t* _memory;
  size_t _size;

  MemoryMap(void* target, size_t size, int protection, int flags, int fd, size_t offset);
  MemoryMap(size_t size, int protection, int flags, int fd, size_t offset);

  /// Synchronise memory region
  ///
  /// \param offset offset in bytes to region
  /// \param length length in bytes of region
  /// \param flags msync flags
  void flush(size_t offset, size_t length, int flags);

  /// Get pointer to underlying memory with byte offset
  ///
  /// \return Pointer to the memory or nullptr
  uint8_t* pointer_mut(size_t offset);
};

template <typename T>
const T* MemoryMap::offset(size_t offset)
{
  return offset_mut<T>(offset);
}

template <typename T>
const T* MemoryMap::offset_bytes(size_t offset)
{
  return offset_bytes_mut<T>(offset);
}

template <typename T>
T* MemoryMap::offset_mut(size_t offset)
{
  return offset_bytes_mut<T>(offset * sizeof(T));
}

template <typename T>
T* MemoryMap::offset_bytes_mut(size_t offset)
{
  return reinterpret_cast<T*>(pointer_mut(offset));
}

template <typename T>
const T MemoryMap::get(size_t offset)
{
  const T* ptr = MemoryMap::offset_bytes<T>(offset);
  if (ptr == nullptr) [[unlikely]] {
    throw std::invalid_argument("Failed to read value from memory map");
  }
  return *ptr;
}

template <typename T>
void MemoryMap::set(size_t offset, const T& value)
{
  T* ptr = MemoryMap::offset_bytes_mut<T>(offset);
  if (ptr == nullptr) [[unlikely]] {
    throw std::invalid_argument("Failed to write value from memory map");
  }
  *ptr = value;
}

template <typename T>
void MemoryMap::modify(size_t offset, const T& value, const T& mask)
{
  T* ptr = MemoryMap::offset_bytes_mut<T>(offset);
  if (ptr == nullptr) [[unlikely]] {
    throw std::invalid_argument("Failed to write value from memory map");
  }
  T current = *ptr;
  *ptr = bits_modify(current, value, mask);
}

template <typename T>
const T MemoryMap::get_stride(size_t offset)
{
  const T* ptr = MemoryMap::offset<T>(offset);
  if (ptr == nullptr) [[unlikely]] {
    throw std::invalid_argument("Failed to read value from memory map");
  }
  return *ptr;
}

template <typename T>
void MemoryMap::set_stride(size_t offset, const T& value)
{
  T* ptr = MemoryMap::offset_mut<T>(offset);
  if (ptr == nullptr) [[unlikely]] {
    throw std::invalid_argument("Failed to write value from memory map");
  }
  *ptr = value;
}

template <typename T>
void MemoryMap::modify_stride(size_t offset, const T& value, const T& mask)
{
  T* ptr = MemoryMap::offset_mut<T>(offset);
  if (ptr == nullptr) [[unlikely]] {
    throw std::invalid_argument("Failed to write value from memory map");
  }
  T current = *ptr;
  *ptr = bits_modify(current, value, mask);
}

}

#endif // GRUND_MEMORY_MAP_HPP
