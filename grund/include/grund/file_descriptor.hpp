#include <filesystem>
#include <optional>
#include <span>
#include <string>
#include <tuple>
#include <utility>

#ifndef GRUND_FILE_DESCRIPTOR_HPP
#define GRUND_FILE_DESCRIPTOR_HPP

namespace grund {

/// File descriptor encapsulation
class FileDescriptor {
public:
  static constexpr int INVALID = -1;

  enum OpenMode {
    /// Open in read mode
    READ = 0x00000001,
    /// Open in write mode
    WRITE = 0x00000002,
    /// Open in non blocking mode
    NON_BLOCKING = 0x00000100,
    /// Open in synchronised mode
    SYNC = 0x00000200,
  };

protected:
  int _fd;

public:
  /// \brief Construct an invalid file descriptor
  explicit FileDescriptor()
    : _fd(INVALID)
  {
  }

  /// \brief FileDescriptor move constructor
  FileDescriptor(FileDescriptor&& other) noexcept
    : _fd(std::exchange(other._fd, INVALID))
  {
  }

  /// \brief Construct FileDescriptor from file descriptor
  FileDescriptor(int fd)
    : _fd(fd)
  {
  }

  /// \brief FileDescriptor destructor
  virtual ~FileDescriptor()
  {
    close();
    _fd = INVALID;
  }

  /// \brief Open file path as read only
  ///
  /// \param path path to file
  /// \return FileDescriptor, throws SystemError if open fails
  static FileDescriptor open_read(const std::string_view& path);

  /// \brief Open file path
  ///
  /// \param path path to file
  /// \param mode read / write mode
  /// \return FileDescriptor, throws SystemError if open fails
  static FileDescriptor open(const std::string_view& path, const OpenMode mode);

  /// \brief Open file path
  ///
  /// \param path path to file
  /// \param mode read / write mode
  /// \return FileDescriptor, throws SystemError if open fails
  static FileDescriptor open(const std::filesystem::path& path, const OpenMode mode);

  /// \brief Create a temporary file
  ///
  /// The file will have prefix /tmp/_temporary_.
  ///
  /// \return FileDescriptor, throws SystemError on failure
  static FileDescriptor create_temporary_file();

  /// Create anonymous pipe
  /// \return tuple with two FileDescriptors, first one is the read fd the second one is the write fd
  static std::tuple<FileDescriptor, FileDescriptor> create_pipe(bool blocking = false);

  FileDescriptor& operator=(const int fd)
  {
    close();
    _fd = fd;
    return *this;
  }

  FileDescriptor& operator=(FileDescriptor&& other) noexcept
  {
    std::swap(_fd, other._fd);
    return *this;
  }

  void close();

  static bool is_valid(int fd) { return fd != INVALID; }

  bool is_open() const { return _fd != INVALID; }

  int fd() const { return _fd; }

  operator int() const { return _fd; }

  bool operator==(const int fd)
  {
    return _fd == fd;
  }

  bool operator==(const FileDescriptor& fd)
  {
    return _fd == fd._fd;
  }

  /// \brief Write bytes to the file descriptor
  ///
  /// \return Number of bytes written, Throws SystemError on error.
  template <class T>
  size_t read(std::span<T>& data);

  /// \brief Read string from the file.
  ///
  /// \return Returns number of bytes read or -errno on error
  ssize_t read_string(std::string& string);

  /// \brief Read the raw object from the file descriptor
  /// Returns zero (0) if the object was successfully read. On error return -errno.
  template <class T>
  ssize_t read_object(T& object);

  /// \brief Read the raw object from the file descriptor
  /// \return The read object. Throws SystemError on error.
  template <class T>
  T read_object();

  /// \brief Read the raw object from the file descriptor
  /// \return The read object or none if the read call would block. Throws SystemError on error.
  template <class T>
  std::optional<T> read_object_non_blocking();

  /// \brief Write the raw object to the file descriptor
  /// \return Throws SystemError on error.
  template <class T>
  void write_object(const T& object);

  /// \brief Write bytes to the file descriptor
  ///
  /// \return Number of bytes written, Throws SystemError on error.
  template <class T>
  size_t write(const std::span<T>& data);

  /// \brief Write string into file
  ///
  /// \param string string to write to the file
  /// \return Returns number of bytes written or -errno on error
  ssize_t write_string(const std::string& string);

  /// \brief Run IO control on the file descriptor, see ioctl
  ///
  /// \param request
  /// \return
  int ioctl(unsigned long request);

  template <class T>
  int ioctl_set(unsigned long request, const T object);

  template <class T>
  int ioctl_set_ref(unsigned long request, const T& object);

  template <class T>
  int ioctl_get(unsigned long request, T& object);

  static int open_flags(const FileDescriptor::OpenMode mode);

protected:
  /// \brief Open file path, throws SystemError if open fails
  explicit FileDescriptor(const std::string_view& path, FileDescriptor::OpenMode flags);

  /// \brief Open file path, throws SystemError if open fails
  explicit FileDescriptor(const std::filesystem::path& path, FileDescriptor::OpenMode flags);

  /// \brief Read bytes into the buffer, tries to read data_size bytes.
  /// \return Number of bytes read or -errno on error
  ssize_t read(void* data, size_t data_size);

  /// \brief Read bytes into the buffer, tries to read data_size bytes.
  /// \return number of bytes read, throws SystemError if read failed
  size_t read_exception(void* data, size_t data_size);

  /// \brief Read bytes into the buffer, tries to read data_size bytes.
  /// \return number of bytes read or none if the read call would block, throws SystemError if read failed
  std::optional<size_t> read_exception_non_blocking(void* data, size_t data_size);

  /// \brief Read bytes into the buffer, tries to read data_size bytes.
  /// \return Throws SystemError if read failed or data_size bytes wasn't read
  void read_exact(void* data, size_t data_size);

  /// \brief Read bytes into the buffer, tries to read data_size bytes.
  /// \return true if the value was read, false if the read call would block. Throws SystemError if read failed or data_size bytes wasn't read
  bool read_exact_non_blocking(void* data, size_t data_size);

  /// \brief Write bytes into the buffer, tries to write data_size bytes.
  /// \return Number of bytes read or -errno on error
  ssize_t write(const void* data, size_t data_size);

  /// \brief Write bytes into the buffer, tries to write data_size bytes.
  /// \return number of bytes written, throws SystemError if read failed
  size_t write_exception(const void* data, size_t data_size);

  /// \brief Write bytes into the buffer, tries to write data_size bytes.
  /// \return throws SystemError if read failed or data_size bytes wasn't written
  void write_exact(const void* data, size_t data_size);

  /// \brief Run IO control on the file descriptor, see ioctl
  ///
  /// \param request
  /// \param data
  /// \return
  int ioctl(unsigned long request, void* data);
};

template <class T>
size_t FileDescriptor::read(std::span<T>& data)
{
  return read_exception(data.data(), data.size() * sizeof(T));
}

template <class T>
ssize_t FileDescriptor::read_object(T& object)
{
  ssize_t bytes = read(&object, sizeof(T));
  if (bytes == sizeof(T)) {
    return 0;
  }
  else {
    return -errno;
  }
}

template <class T>
T FileDescriptor::read_object()
{
  T object;
  read_exact(&object, sizeof(T));
  return object;
}

template <class T>
std::optional<T> FileDescriptor::read_object_non_blocking()
{
  T object;
  if (read_exact_non_blocking(&object, sizeof(T))) {
    return object;
  }
  return {};
}

template <class T>
void FileDescriptor::write_object(const T& object)
{
  write_exact(&object, sizeof(T));
}

template <class T>
size_t FileDescriptor::write(const std::span<T>& data)
{
  return write_exception(data.data(), data.size() * sizeof(T));
}

template <class T>
int FileDescriptor::ioctl_get(unsigned long request, T& object)
{
  return ioctl(request, &object);
}

template <class T>
int FileDescriptor::ioctl_set(unsigned long request, const T object)
{
  return ioctl(request, const_cast<T*>(&object));
}

template <class T>
int FileDescriptor::ioctl_set_ref(unsigned long request, const T& object)
{
  return ioctl(request, *object);
}

inline constexpr FileDescriptor::OpenMode operator|(const FileDescriptor::OpenMode lhs, const FileDescriptor::OpenMode rhs)
{
  using T = std::underlying_type_t<FileDescriptor::OpenMode>;
  return static_cast<FileDescriptor::OpenMode>(static_cast<T>(lhs) | static_cast<T>(rhs));
}

/// \brief Open path as read and read
/// \param path File path to read
/// \param size_hint Maximum size to read
/// \return Read string, throws SystemError if read failed
std::string read_string(const std::string_view& path, ssize_t size_hint = 128);

/// \brief Open path as read and read
/// \param path File path to read
/// \param size_hint Maximum size to read
/// \return Read string, throws SystemError if read failed
std::string read_string(const std::filesystem::path& path, ssize_t size_hint = 128);

/// \brief Open path as read and read string, trim the end of the string
/// \param path File path to read
/// \param size_hint Maximum size to read
/// \return Read string, throws SystemError if read failed
std::string read_string_rtrim(const std::filesystem::path& path, ssize_t size_hint = 128);

} // namespace grund

#endif // GRUND_FILE_DESCRIPTOR_HPP
