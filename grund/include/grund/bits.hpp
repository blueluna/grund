#include <cstdint>

#ifndef GRUND_BITS_HPP
#define GRUND_BITS_HPP

namespace grund {

/// Bit manipulation, change the bits masked with mask.
///
/// Apply the mask on current_value and change the masked bit to new_value.
///
/// \code
/// uint32_t new { 0x00800000 };
/// uint32_t mask { 0x00c00000 };
/// uint32_t current { 0x01400001 };
/// 0x01800001 == bits_modify(current, new, mask);
/// \endcode
///
/// \tparam T Value type
/// \param current_value Current bits
/// \param new_value New bits
/// \param mask Mask, where bits that shall change shall be one (1)
/// \return New value with modified bits
template <typename T>
inline T bits_modify(T current_value, T new_value, T mask)
{
  return (current_value & ~mask) | (new_value & mask);
}

}

#endif // GRUND_BITS_HPP
