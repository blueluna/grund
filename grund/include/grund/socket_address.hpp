// Socket address

#include <string>
extern "C" {
#include <linux/netlink.h>
}
#include <grund/ip_address.hpp>

#ifndef GRUND_SOCKET_ADDRESS_HPP
#define GRUND_SOCKET_ADDRESS_HPP

namespace grund {

/// Socket address
class SocketAddress {
protected:
  struct sockaddr_storage _storage;

public:
  /// SocketAddress move constructor
  SocketAddress(SocketAddress&&) noexcept;
  /// Default constructor, constructs a invalid socket address
  explicit SocketAddress();
  /// Construct SocketAddress from IP address and port
  ///
  /// \param address socket IP address
  /// \param port socket port
  explicit SocketAddress(const IpAddress& address, uint16_t port);
  /// Construct SocketAddress from struct sockaddr_storage
  explicit SocketAddress(struct sockaddr_storage address);

  static SocketAddress netlink(uint32_t pid, uint32_t groups);

  /// Socketaddress move assignment
  SocketAddress& operator=(SocketAddress&&) noexcept;

  /// Does this socket address have a IPv4 address?
  /// \return true if IPv4 otherwise false
  bool is_ipv4() const;
  /// Does this socket address have a IPv6 address?
  /// \return true if IPv6 otherwise false
  bool is_ipv6() const;
  /// Does this socket address have a Netlink address?
  /// \return true if Netlink otherwise false
  bool is_netlink() const;

  /// Return the internal struct sockaddr_storage
  struct sockaddr_storage into_sockaddr_storage() const;

  /// Get the socket family
  sa_family_t family() const { return _storage.ss_family; }

  /// Get the socklen
  socklen_t socklen() const;

  /// Get the socket IP address
  IpAddress ip_address() const;

  /// Get the socket port
  uint16_t port() const;

  /// Format socket address as string
  std::string as_string() const;

  /// Cast to struct sockaddr_storage
  explicit operator struct sockaddr_storage() { return _storage; }
  /// Cast to struct sockaddr*
  explicit operator struct sockaddr *() { return reinterpret_cast<struct sockaddr*>(&_storage); }
  /// Cast to const struct sockaddr*
  explicit operator const struct sockaddr *() const { return reinterpret_cast<const struct sockaddr*>(&_storage); }
  /// Cast to const struct sockaddr_nl*
  explicit operator const struct sockaddr_nl *() const { return reinterpret_cast<const struct sockaddr_nl*>(&_storage); }

  /// Lookup socket address
  ///
  /// \param address Socket address as string
  /// \param port Socket port as string
  /// \param hints Search hints
  /// \return SocketAddress, throws SystemError on error
  static SocketAddress lookup(const char* address, const char* port, const struct addrinfo& hints);
  /// Lookup socket address for a client socket. Address lookup for a address suitable for connect.
  ///
  /// \param family Socket family
  /// \param address Socket address as string
  /// \param port Socket port as string
  /// \return SocketAddress, throws SystemError on error
  static SocketAddress lookup_client(const sa_family_t family, const char* address, const char* port);
  /// Lookup socket address for a server socket. Address lookup for a address suitable for bind.
  ///
  /// \param family Socket family
  /// \param address Socket address as string
  /// \param port Socket port as string
  /// \return SocketAddress, throws SystemError on error
  static SocketAddress lookup_server(const sa_family_t family, const char* address, const char* port);
  /// Lookup socket address for a client socket. Address lookup for a address suitable for connect.
  ///
  /// \param address Socket address as string
  /// \param port Socket port as string
  /// \return SocketAddress, throws SystemError on error
  static SocketAddress lookup_client(const char* address, const char* port);
  /// Lookup socket address for a server socket. Address lookup for a address suitable for bind.
  ///
  /// \param address Socket address as string
  /// \param port Socket port as string
  /// \return SocketAddress, throws SystemError on error
  static SocketAddress lookup_server(const char* address, const char* port);
};

} // namespace grund

#endif // GRUND_SOCKET_ADDRESS_HPP
