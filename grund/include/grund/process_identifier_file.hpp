#include <filesystem>

#ifndef GRUND_PROCESS_IDENTIFIER_FILE_HPP
#define GRUND_PROCESS_IDENTIFIER_FILE_HPP

namespace grund {

class ProcessIdentifierFile {
public:
  explicit ProcessIdentifierFile(std::filesystem::path& path);

  uint64_t pid() const;

  bool occupied() const;

  void write() const;

protected:
  std::filesystem::path _file_path;
};

}

#endif // GRUND_PROCESS_IDENTIFIER_FILE_HPP
