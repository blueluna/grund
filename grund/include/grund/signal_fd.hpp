//

#ifndef GRUND_SIGNAL_FD_HPP
#define GRUND_SIGNAL_FD_HPP

#include <csignal>
#include <grund/file_descriptor.hpp>

namespace grund {

// File descriptor signal handler
class SignalFd : public FileDescriptor {
private:
  static int open(sigset_t& signal_mask, bool non_blocking = true);

public:
  // Create a signalfd from the provided signal mask
  explicit SignalFd(sigset_t& signal_mask, bool non_blocking = true);
  // Create a signalfd from the provided signals
  // signals is one or more signal numbers or:ed together
  explicit SignalFd(int signals, bool non_blocking = true);
  // Read the signal status, returns a signal number on success.
  // Returns zero (0) if the read timed out.
  // Throws SystemError on system errors.
  int read_signal();
};

}

#endif // GRUND_SIGNAL_FD_HPP
