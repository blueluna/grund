#ifndef GRUND_SYSTEM_HPP
#define GRUND_SYSTEM_HPP

#include <cstddef>
#include <cstdint>
#include <tuple>

namespace grund {

class System {
public:
  /// Get the current page size
  ///
  /// \return Size of a memory page in the system
  static std::size_t get_page_size();
  /// Get the Linux kernel version
  ///
  /// \return Kernel version as tuple with major-, minor-, patch version
  static std::tuple<uint32_t, uint32_t, uint32_t> get_kernel_version();
};

}

#endif // GRUND_SYSTEM_HPP
