#include <grund/memory_map.hpp>

#ifndef GRUND_DEV_MEM_HPP
#define GRUND_DEV_MEM_HPP

namespace grund {

class DevMem {
public:
  /// Memory map physical address and length with /dev/mem
  ///
  /// \param physical_address Physical address to map
  /// \param length Size of the memory mapped region
  /// \return Memory mapped region
  static grund::MemoryMap to_memory_map(size_t physical_address, size_t length);
};

}

#endif // GRUND_DEV_MEM_HPP
