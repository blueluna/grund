#include <chrono>
#include <memory>
#include <utility>
extern "C" {
#include <semaphore.h>
}

#ifndef GRUND_NAMED_SEMAPHORE_HPP
#define GRUND_NAMED_SEMAPHORE_HPP

namespace grund {

class NamedSemaphore {
public:
  /// Default constructor, creates an object with invalid semaphore handle (nullptr)
  NamedSemaphore();
  /// Move constructor
  NamedSemaphore(NamedSemaphore&& other) noexcept;
  /// \brief Creates a named semaphore
  ///
  /// \param name Semaphore name
  /// \param value Initial value
  /// \param open_flags Open flags, O_RDWR, O_EXCL, O_CREAT, ...
  /// \param mode Access flags
  /// \return NamedSemaphore or throws an std::system_error exception
  static NamedSemaphore create(const std::string& name, int value, int open_flags, mode_t mode);
  /// \brief Creates a named semaphore
  ///
  /// Open flags O_RDWR | O_EXCL | O_CREAT.
  /// Access flags 0660.
  ///
  /// \param name Semaphore name
  /// \param value Initial value
  /// \return NamedSemaphore or throws an std::system_error exception
  static NamedSemaphore create(const std::string& name, int value);
  /// \brief Open or creates a named semaphore
  ///
  /// Open flags O_RDWR | O_CREAT.
  /// Access flags 0660
  ///
  /// \param name Semaphore name
  /// \param value Initial value
  /// \return NamedSemaphore or throws an std::system_error exception
  static NamedSemaphore open_create(const std::string& name, int value);
  /// \brief Open a named semaphore
  ///
  /// \param name Semaphore name
  /// \return NamedSemaphore or throws an std::system_error exception
  static NamedSemaphore open(const std::string& name);
  /// Move assignment
  NamedSemaphore& operator=(NamedSemaphore&& other) noexcept
  {
    _handle = std::exchange(other._handle, nullptr);
    return *this;
  }
  /// Boolean operator, indicating if the named semaphore is valid
  explicit operator bool() const noexcept { return _handle != nullptr; }
  /// Unlink (delete) named semaphore
  static void unlink(const std::string& name);
  /// Post. Increases the semaphore counter
  void post();
  /// Wait. Waits until the semaphore counter is greater than zero. Decreases the counter on success.
  void wait();
  /// Try Wait. Checks if the the semaphore counter is greater than zero. If true, decreases the counter.
  ///
  /// \return true if wait succeeded
  bool try_wait();
  /// Wait until timeout. Waits until the semaphore counter is greater than zero or timeout. If true, decreases the counter.
  ///
  /// \return true if wait succeeded
  bool wait(std::chrono::nanoseconds timeout);
  /// Read the semaphore value
  ///
  /// \return Semaphore counter value
  int value();

  static std::string fix_name(const std::string& name);

protected:
  explicit NamedSemaphore(::sem_t* handle);

  std::unique_ptr<sem_t, void (*)(sem_t*)> _handle;
};
}

#endif // GRUND_NAMED_SEMAPHORE_HPP
