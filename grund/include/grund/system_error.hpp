#include <cerrno>
#include <cstring>
#include <exception>

#ifndef GRUND_SYSTEM_ERROR_HPP
#define GRUND_SYSTEM_ERROR_HPP

namespace grund {

// Exception for system error codes (errno)
class SystemError
  : virtual public std::exception {
protected:
  int _error_code;

public:
  /// Create exception for current errno
  explicit SystemError()
    : _error_code(errno)
  {
  }
  /// Create exception with provided error code (errno)
  explicit SystemError(int code)
    : _error_code(code)
  {
  }
  // Error descripton
  const char* what() const noexcept
  {
    return strerror(_error_code);
  }
  // Return the system error code
  int error_code() const { return _error_code; }
};

} // namespace grund

#endif // GRUND_SYSTEM_ERROR_HPP
