#ifndef GRUND_EVENT_FD_HPP
#define GRUND_EVENT_FD_HPP

#include <cinttypes>
#include <grund/file_descriptor.hpp>

namespace grund {

// File descriptor event
class EventFd : public FileDescriptor {
public:
  // Create a evente file descriptor with counter set to zero
  explicit EventFd();
  // Adds `addition` to the counter
  void emit(uint64_t addition);
  // Adds one to the counter
  void emit();
  // Read the event counter, reset the value to zero
  // Returns the counter value.
  // Throws SystemError.
  uint64_t read_event();
};

// File descriptor event in semaphore mode
// Same as `EventFd` but decreases the counter by one on each read
class EventFdSemaphore : public FileDescriptor {
public:
  // Create a evente file descriptor in semaphore mode with counter set to zero
  explicit EventFdSemaphore();
  // Adds `addition` to the counter
  void emit(uint64_t addition);
  // Adds one to the counter
  void emit();
  // Read the event counter, decrease the value with one
  // Returns true if the counter was non-zero, otherwise false.
  // Throws SystemError.
  bool read_event();
};

}

#endif // GRUND_EVENT_FD_HPP
