
#include <grund/socket.hpp>

#ifndef GRUND_UDP_MULTICAST_HPP
#define GRUND_UDP_MULTICAST_HPP

namespace grund {

class UdpMulticast : public Socket {
public:
  explicit UdpMulticast(sa_family_t family, bool non_blocking)
    : Socket(family, Socket::Type::Udp, non_blocking)
  {
  }
  ~UdpMulticast() override = default;

  void join(IpAddress& address);

  std::tuple<std::size_t, SocketAddress> receive_from(void* buffer, std::size_t buffer_len);

protected:
  void multicast_loop_enable(bool enable);
  void join_ipv4(struct in_addr* address);
  void join_ipv6(struct in6_addr* address);
};

}

#endif // GRUND_UDP_MULTICAST_HPP
