#ifndef GRUND_TIMER_FD_HPP
#define GRUND_TIMER_FD_HPP

#include <cstdint>
#include <grund/file_descriptor.hpp>

namespace grund {

// File descriptor timer
class TimerFd : public FileDescriptor {
public:
  // Create a file descriptor timer
  explicit TimerFd(bool non_blocking = true);
  // Arm the timer to fire in `duration` nanoseconds.
  // If `periodic` is true, periodically fire each `duration`.
  void arm(const std::chrono::nanoseconds duration, bool periodic);
  // Clear an timer event
  void clear();
};

}

#endif // GRUND_TIMER_FD_HPP
