#ifndef GRUND_THREAD_HPP
#define GRUND_THREAD_HPP

#include <atomic>
#include <thread>

namespace grund {

class Thread {
protected:
  std::unique_ptr<std::thread> _thread;
  std::atomic_bool _stopSignal;

public:
  /// @brief Create a thread object
  Thread()
    : _thread { nullptr }
    , _stopSignal(false)
  {
  }

  ~Thread()
  {
    request_stop();
    join();
  }

  /// @brief Start the thread
  void start()
  {
    if (_thread) {
      return;
    }
    _stopSignal.store(false, std::memory_order::relaxed);
    _thread = std::make_unique<std::thread>(&Thread::thread_function, this);
  }

  /// @brief Request the thread to stop
  void request_stop()
  {
    if (_thread && _thread->joinable()) {
      _stopSignal.store(true, std::memory_order::relaxed);
    }
  }

  /// @brief Join the thread
  void join()
  {
    if (_thread && _thread->joinable()) {
      _thread->join();
    }
  }

protected:
  /// @brief Thread function to run
  virtual void thread_function() = 0;

  /// @brief Has stop been requested?
  /// @return true if stop has been requested, else false
  bool stop_requested()
  {
    return _stopSignal.load(std::memory_order::relaxed);
  }
};

}

#endif // GRUND_THREAD_HPP
