#include <cstdint>
#include <optional>
#include <span>
#include <stdexcept>

#ifndef GRUND_BYTES_HPP
#define GRUND_BYTES_HPP

namespace grund {

namespace raw {

  template <typename T>
  constexpr T from_bytes_raw(std::span<const uint8_t> bytes)
  {
    (void)bytes;
    return {};
  }

  template <>
  constexpr int8_t from_bytes_raw(std::span<const uint8_t> bytes)
  {
    return static_cast<int8_t>(bytes[0]);
  }

  template <>
  constexpr uint8_t from_bytes_raw(std::span<const uint8_t> bytes)
  {
    return static_cast<uint8_t>(bytes[0]);
  }

  template <>
  constexpr int16_t from_bytes_raw(std::span<const uint8_t> bytes)
  {
    return static_cast<int16_t>(bytes[0]) | (static_cast<int16_t>(bytes[1]) << 8);
  }

  template <>
  constexpr uint16_t from_bytes_raw(std::span<const uint8_t> bytes)
  {
    return static_cast<uint16_t>(bytes[0]) | (static_cast<uint16_t>(bytes[1]) << 8);
  }

  template <>
  constexpr int32_t from_bytes_raw(std::span<const uint8_t> bytes)
  {
    return static_cast<int32_t>(bytes[0]) | (static_cast<int32_t>(bytes[1]) << 8) | (static_cast<int32_t>(bytes[2]) << 16) | (static_cast<int32_t>(bytes[3]) << 24);
  }

  template <>
  constexpr uint32_t from_bytes_raw(std::span<const uint8_t> bytes)
  {
    return static_cast<uint32_t>(bytes[0]) | (static_cast<uint32_t>(bytes[1]) << 8) | (static_cast<uint32_t>(bytes[2]) << 16) | (static_cast<uint32_t>(bytes[3]) << 24);
  }

  template <>
  constexpr int64_t from_bytes_raw(std::span<const uint8_t> bytes)
  {
    return static_cast<int64_t>(bytes[0]) | (static_cast<int64_t>(bytes[1]) << 8) | (static_cast<int64_t>(bytes[2]) << 16) | (static_cast<int64_t>(bytes[3]) << 24)
      | (static_cast<int64_t>(bytes[4]) << 32) | (static_cast<int64_t>(bytes[5]) << 40) | (static_cast<int64_t>(bytes[6]) << 48) | (static_cast<int64_t>(bytes[7]) << 56);
  }

  template <>
  constexpr uint64_t from_bytes_raw(std::span<const uint8_t> bytes)
  {
    return static_cast<uint64_t>(bytes[0]) | (static_cast<uint64_t>(bytes[1]) << 8) | (static_cast<uint64_t>(bytes[2]) << 16) | (static_cast<uint64_t>(bytes[3]) << 24)
      | (static_cast<uint64_t>(bytes[4]) << 32) | (static_cast<uint64_t>(bytes[5]) << 40) | (static_cast<uint64_t>(bytes[6]) << 48) | (static_cast<uint64_t>(bytes[7]) << 56);
  }
}

template <typename T>
constexpr std::optional<T> try_from_bytes(std::span<const uint8_t> bytes)
{
  if (bytes.size() >= sizeof(T)) {
    return raw::from_bytes_raw<T>(bytes);
  }
  return {};
}

template <typename T>
constexpr T from_bytes(std::span<const uint8_t> bytes)
{
  if (bytes.size() < sizeof(T)) {
    throw std::invalid_argument("Invalid argument to from_bytes");
  }
  return raw::from_bytes_raw<T>(bytes);
}

template <typename T>
constexpr T from_bytes_exact(std::span<const uint8_t> bytes)
{
  if (bytes.size() != sizeof(T)) {
    throw std::invalid_argument("Invalid argument to from_bytes_exact");
  }
  return raw::from_bytes_raw<T>(bytes);
}

}

#endif // GRUND_BYTES_HPP
