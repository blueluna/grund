#include <grund/named_semaphore.hpp>

#ifndef GRUND_BASE_RESOURCE_LOCK_HPP
#define GRUND_BASE_RESOURCE_LOCK_HPP

namespace grund {

class SystemResourceLock {
public:
  SystemResourceLock();
  SystemResourceLock(const std::string& name);

  bool acquire(std::chrono::nanoseconds timeout);
  void release();

protected:
  NamedSemaphore _semaphore;
  bool _locked;
};

}

#endif // GRUND_BASE_RESOURCE_LOCK_HPP
