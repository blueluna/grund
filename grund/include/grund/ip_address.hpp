// IP address

#include <cstdint>
extern "C" {
#include <memory.h>
#include <netinet/in.h>
}
#include <string>

#ifndef GRUND_IP_ADDRESS_HPP
#define GRUND_IP_ADDRESS_HPP

namespace grund {

// IP address representation
class IpAddress {
protected:
  union _ip_address {
    uint32_t ipv4;
    uint8_t ipv6[16];
  };
  sa_family_t _family;
  _ip_address _address;

public:
  // Create IP address from four octets
  explicit IpAddress(std::uint8_t a, std::uint8_t b, std::uint8_t c, std::uint8_t d);
  // Create IP address from `struct in_addr`.
  explicit IpAddress(const struct in_addr address);
  // Create IP address from `in_addr_t`.
  explicit IpAddress(const in_addr_t address);
  // Create IP address from `struct in6_addr`
  explicit IpAddress(const struct in6_addr address);
  // Create IP address from `struct sockaddr_storage`
  explicit IpAddress(const struct sockaddr_storage& address);
  // Create an IP address from a string.
  // Either IPv4 (127.0.0.1) or IPv6 (::1).
  // Don't do any name resolution
  explicit IpAddress(const std::string& address);
  // Create an IP address from a string.
  // Either IPv4 (127.0.0.1) or IPv6 (::1).
  // Don't do any name resolution
  explicit IpAddress(const char* address);
  // Move constructor
  IpAddress(IpAddress&&);
  // Copy constructor
  IpAddress(const IpAddress&);
  // Move assignment
  IpAddress& operator=(IpAddress&&) noexcept;
  // `sa_family_t` constant describing IPv4
  static const sa_family_t Ipv4;
  // `sa_family_t` constant describing IPv6
  static const sa_family_t Ipv6;

  static IpAddress ipv4_any() { return IpAddress { INADDR_ANY }; };
  static IpAddress ipv4_loopback() { return IpAddress { INADDR_LOOPBACK }; }
  static IpAddress ipv4_broadcast() { return IpAddress { INADDR_BROADCAST }; };

  static IpAddress ipv6_any() { return IpAddress { in6addr_any }; };
  static IpAddress ipv6_loopback() { return IpAddress { in6addr_loopback }; }

  // IPv4 address family check. Returns true if the address is an IPv4 address.
  bool is_ipv4() const { return _family == Ipv4; }
  // IPv6 address family check. Returns true if the address is an IPv6 address.
  bool is_ipv6() const { return _family == Ipv6; }

  struct sockaddr_storage into_sockaddr_storage(uint16_t port) const;
  bool into_in_addr(struct in_addr& sa) const;
  bool into_in6_addr(struct in6_addr& sa) const;

  sa_family_t family() const { return _family; }

  socklen_t socklen() const;

  std::string as_string() const;

  inline bool operator==(const IpAddress& other)
  {
    if (_family == other._family) {
      if (is_ipv4()) {
        return _address.ipv4 == other._address.ipv4;
      }
      else if (is_ipv6()) {
        return memcmp(_address.ipv6, other._address.ipv6, 16) == 0;
      }
    }
    return false;
  }
};

} // namespace grund

#endif // GRUND_IP_ADDRESS_HPP
