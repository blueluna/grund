// Time helper functions

#include <chrono>
#include <cstdint>
#include <ctime>

#ifndef GRUND_TIME_HPP
#define GRUND_TIME_HPP

namespace grund {

constexpr ::timespec timespec_from_duration(const std::chrono::nanoseconds duration)
{
  auto nanoseconds = duration;
  auto secs = std::chrono::duration_cast<std::chrono::seconds>(nanoseconds);
  nanoseconds -= secs;
  return timespec { secs.count(), nanoseconds.count() };
}

} // namespace grund

#endif
