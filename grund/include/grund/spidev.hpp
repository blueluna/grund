#include <grund/file_descriptor.hpp>
#include <initializer_list>
#include <span>

#ifndef GRUND_SPIDEV_HPP
#define GRUND_SPIDEV_HPP

namespace grund {

class SpiDev : FileDescriptor {
public:
  enum class MODE {
    /// SPI mode 0, CPHA = 0, CPOL = 0, Data sampled on rising edge and shifted out on the falling edge. Clock idle is low.
    MODE_0,
    /// SPI mode 1, CPHA = 1, CPOL = 0, Data sampled on the falling edge and shifted out on the rising edge. Clock idle is low.
    MODE_1,
    /// SPI mode 2, CPHA = 0, CPOL = 1, Data sampled on the rising edge and shifted out on the falling edge. Clock idle is high.
    MODE_2,
    /// SPI mode 3, CPHA = 1, CPOL = 1, Data sampled on the falling edge and shifted out on the rising edge. Clock idle is high.
    MODE_3,
  };

  /// SPI mode 0, CPHA = 0, CPOL = 0, Data sampled on rising edge and shifted out on the falling edge. Clock idle is low.
  static constexpr MODE MODE_0 = MODE::MODE_0;
  /// SPI mode 1, CPHA = 1, CPOL = 0, Data sampled on the falling edge and shifted out on the rising edge. Clock idle is low.
  static constexpr MODE MODE_1 = MODE::MODE_1;
  /// SPI mode 2, CPHA = 0, CPOL = 1, Data sampled on the rising edge and shifted out on the falling edge. Clock idle is high.
  static constexpr MODE MODE_2 = MODE::MODE_2;
  /// SPI mode 3, CPHA = 1, CPOL = 1, Data sampled on the falling edge and shifted out on the rising edge. Clock idle is high.
  static constexpr MODE MODE_3 = MODE::MODE_3;

  /// Open SPI device
  ///
  /// \param bus SPI bus number for the SPI device
  /// \param chip_select SPI chip select number for the SPI device
  explicit SpiDev(uint16_t bus, uint16_t chip_select);

  /// Get SPI mode for device
  ///
  /// \return current SPI mode
  MODE get_mode();
  /// Set SPI mode for device
  ///
  /// \param mode SPI mode
  void set_mode(MODE mode);
  /// Get bits per word for device
  ///
  /// \return current bits per word
  uint8_t get_bits_per_word();
  /// Set bits per word for device
  ///
  /// \param bits_per_word number of bits per word
  void set_bits_per_word(uint8_t bits_per_word);
  /// Get maximum clock frequency for device
  ///
  /// \return current maximum clock frequency in Hz
  uint32_t get_frequency();
  /// Set maximum clock frequency for device
  ///
  /// \param frequency maximum clock frequency in Hz
  void set_frequency(uint32_t frequency);

  /// Send sequence of bytes on the SPI device
  ///
  /// \param tx Byte sequence to send
  /// \return true if transfer succeeded
  bool send(const std::span<uint8_t>& tx);
  /// Send sequence of bytes on the SPI device
  ///
  /// \param tx Byte sequence to send
  /// \return true if transfer succeeded
  bool send(const std::initializer_list<const uint8_t>& tx);
  /// Send sequence of bytes on the SPI device
  ///
  /// \tparam TX_LEN Number of bytes in the array
  /// \param tx Byte sequence to send
  /// \return true if transfer succeeded
  template <std::size_t TX_LEN>
  bool send(const std::array<uint8_t, TX_LEN>& tx);
  /// Send then receive sequences of bytes on the SPI device
  ///
  /// \param tx Byte sequence to send
  /// \param rx Byte sequence to store received bytes
  /// \return true if transfer succeeded
  bool send_receive(const std::initializer_list<const uint8_t>& tx, std::span<uint8_t> rx);
  /// Send then receive sequences of bytes on the SPI device
  ///
  /// \param tx Byte sequence to send
  /// \param rx Byte sequence to store received bytes
  /// \return true if transfer succeeded
  bool send_receive(const std::span<const uint8_t>& tx, std::span<uint8_t>& rx);
  /// Send then receive sequences of bytes on the SPI device
  ///
  /// \tparam RX_LEN Number of bytes in the receive array
  /// \param tx Byte sequence to send
  /// \param rx Byte sequence to store received bytes
  /// \return true if transfer succeeded
  template <std::size_t RX_LEN>
  bool send_receive(const std::initializer_list<const uint8_t>& tx, std::array<uint8_t, RX_LEN>& rx);

protected:
  bool send(const uint8_t* tx_ptr, size_t tx_len);
  bool send_receive(const uint8_t* tx_ptr, size_t tx_len, uint8_t* rx_ptr, size_t rx_len);
};

template <std::size_t TX_LEN>
bool SpiDev::send(const std::array<uint8_t, TX_LEN>& tx)
{
  return send(std::span { tx });
}

template <std::size_t RX_LEN>
bool SpiDev::send_receive(const std::initializer_list<const uint8_t>& tx, std::array<uint8_t, RX_LEN>& rx)
{
  return send_receive(tx, std::span { rx });
}

}

#endif // GRUND_SPIDEV_HPP
