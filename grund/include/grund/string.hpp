#include <functional>
#include <string>

#ifndef GRUND_STRING_HPP
#define GRUND_STRING_HPP

namespace grund {

bool string_starts_with(const char* needle, const char* haystack);

// trim from end
static inline std::string rtrim(std::string&& s)
{
  auto not_space = [](unsigned char c) { return !std::isspace(c); };
  s.erase(std::find_if(s.rbegin(), s.rend(), not_space).base(), s.end());
  return s;
}

static inline std::vector<std::string> split(const std::string_view& source, const char delimiter)
{
  size_t start = 0;
  size_t end;
  std::vector<std::string> tokens;
  for (; (end = source.find(delimiter, start)) != std::string::npos; start = end + 1) {
    if (start != end) {
      tokens.emplace_back(source.substr(start, end - start));
    }
  }
  end = source.length();
  if (start != end) {
    tokens.emplace_back(source.substr(start, end - start));
  }
  return tokens;
}

}

#endif // GRUND_STRING_HPP
