#include "grund/file_descriptor.hpp"
#include "grund/socket_address.hpp"

#include <tuple>

#ifndef GRUND_SOCKET_HPP
#define GRUND_SOCKET_HPP

namespace grund {

/// Network socket
class Socket : public FileDescriptor {
public:
  /// Socket type
  enum class Type {
    /// UDP socket
    Udp = SOCK_DGRAM,
    /// RAW socket
    Raw = SOCK_RAW,
    /// TCP socket
    Tcp = SOCK_STREAM,
  };
  /// Default constructor, construct a invalid socket
  Socket()
    : FileDescriptor()
  {
  }
  /// Construct Socket from file descriptor
  Socket(int socket)
    : FileDescriptor(socket)
  {
  }

  explicit Socket(const sa_family_t& family, Socket::Type type, bool non_blocking, int protocol);

  explicit Socket(const sa_family_t& family, Socket::Type type, bool non_blocking)
    : Socket(family, type, non_blocking, 0)
  {
  }
  /// Create a UDP socket
  static Socket UdpSocket(const sa_family_t& family, bool non_blocking)
  {
    return Socket(family, Socket::Type::Udp, non_blocking, 0);
  }
  /// Create a TCP socket
  static Socket TcpSocket(const sa_family_t& family, bool non_blocking)
  {
    return Socket(family, Socket::Type::Tcp, non_blocking, 0);
  }
  /// On a listening socket, accept a incoming connection
  ///
  /// \return Socket associated with the connection
  Socket accept();
  /// On a listening socket, accept a incoming connection
  ///
  /// \return A tuple with the socket associated with the connection and
  /// the remote address for the connection
  std::tuple<Socket, SocketAddress> accept_with_address();
  /// Bind the socket to the provided socket address
  void bind(const SocketAddress& sa);
  /// Bind the socket to the provided IP address and port
  void bind(const IpAddress& address, uint16_t port);
  /// Bind the socket to IPv4 any address and the provided port
  void bind(uint16_t port);
  /// Connect the socket to the provided socket address
  bool connect(const SocketAddress& sa);
  /// Listen on the socket
  ///
  /// \param backlog Connection queue length
  void listen(const int backlog);
  /// Shutdown the communication on the socket
  void shutdown();
  /// Get socket option
  template <typename T>
  T get_option(int level, int operation);
  /// Set socket option
  void set_option(int level, int operation, bool value);
  /// Set socket option
  void set_option(int level, int operation, int value);
  /// Get the socket address
  SocketAddress get_address() const;
};

}

#endif // GRUND_SOCKET_HPP
