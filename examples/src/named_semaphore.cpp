#include <atomic>
#include <csignal>
#include <cstdlib>
#include <cstring>
#include <grund/epoll/eventloop.hpp>
#include <grund/file_descriptor.hpp>
#include <grund/named_semaphore.hpp>
#include <grund/system_resource_lock.hpp>
extern "C" {
#include <getopt.h>
#include <termios.h>
#include <unistd.h>
}

static volatile sig_atomic_t running = 1;

static void signal_handler(int signal, siginfo_t* info, void* data)
{
  (void)signal;
  (void)info;
  (void)data;
  running = 0;
}

class ILock {
public:
  virtual void acquire() = 0;
  virtual void release() = 0;
};

class ScopedLock {
public:
  ScopedLock(ILock* lock)
    : _lock(lock, [](ILock* lock) { lock->release(); })
  {
    _lock->acquire();
  }

  ScopedLock(ScopedLock&) = delete;

protected:
  std::unique_ptr<ILock, void (*)(ILock*)> _lock;
};

class ProcessResourceLock : public ILock {
public:
  ProcessResourceLock(const std::string& name)
    : _semaphore(grund::NamedSemaphore::open_create(name, 1))
  {
  }

  void acquire() override
  {
    _semaphore.wait();
  }

  void release() override
  {
    _semaphore.post();
  }

protected:
  grund::NamedSemaphore _semaphore;
};

grund::NamedSemaphore open(const std::string& name, bool create, bool open)
{
  if (open) {
    return grund::NamedSemaphore::open(name);
  }
  if (create) {
    return grund::NamedSemaphore::create(name, 0);
  }
  return grund::NamedSemaphore::open_create(name, 0);
}

int main(int argc, char* argv[])
{
  using namespace std::literals::chrono_literals;

  static struct option long_options[] = {
    { "create", no_argument, 0, 'c' },
    { "name", required_argument, 0, 'n' },
    { "open", no_argument, 0, 'o' },
    { "unlink", no_argument, 0, 'u' },
    { 0, 0, 0, 0 }
  };
  int option_index = 0;
  bool opt_create = false;
  bool opt_open = false;
  bool opt_unlink = false;
  std::string opt_name;

  while (1) {
    int c = getopt_long(argc, argv, "cn:ou",
      long_options, &option_index);
    if (c == -1) {
      break;
    }
    switch (c) {
    case 'c':
      opt_create = true;
      break;
    case 'n':
      opt_name.assign(optarg);
      break;
    case 'o':
      opt_open = true;
      break;
    case 'u':
      opt_unlink = true;
      break;
    default:
      break;
    }
  }

  if (opt_name.empty()) {
    fprintf(stderr, "Name required\n");
    return EXIT_FAILURE;
  }

  if (opt_unlink) {
    fprintf(stdout, "unlink %s\n", opt_name.c_str());
    grund::NamedSemaphore::unlink(opt_name);
    return EXIT_SUCCESS;
  }

  grund::NamedSemaphore ns = open(opt_name, opt_create, opt_open);

  struct termios terminal_settings { };
  tcgetattr(STDIN_FILENO, &terminal_settings);
  struct termios original_terminal_settings {
    terminal_settings
  };
  terminal_settings.c_lflag &= (~ICANON);
  terminal_settings.c_lflag &= (~ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &terminal_settings);

  struct sigaction sig { };
  std::memset(&sig, 0, sizeof(sig));
  sig.sa_sigaction = signal_handler;
  sigfillset(&sig.sa_mask);
  sig.sa_flags = SA_SIGINFO;
  sigaction(SIGINT, &sig, nullptr);

  grund::FileDescriptor in(STDIN_FILENO);

  grund::epoll::EventLoop event_loop;

  grund::epoll::Event input_event(event_loop, in, grund::epoll::Event::READ, [&ns](int fd, uint32_t events) {
    if (events != grund::epoll::Event::READ) {
      return;
    }
    char c;
    read(fd, &c, 1);
    std::string topic;
    switch (c) {
    case 'p':
      ns.post();
      fprintf(stdout, "post\n");
      break;
    case 'v':
      fprintf(stdout, "value %d\n", ns.value());
      break;
    case 'w':
      if (!ns.wait(10s)) {
        fprintf(stdout, "Timeout\n");
      }
      else {
        int value = ns.value();
        fprintf(stdout, "done %d\n", value);
      }
      break;
    default:
      break;
    }
  });

  while (running) {
    event_loop.run_once();
  }

  tcsetattr(STDIN_FILENO, TCSANOW, &original_terminal_settings);

  return EXIT_SUCCESS;
}
