#include <chrono>
#include <cinttypes>
#include <grund/iouring/executor.hpp>
#include <grund/iouring/signal_fd.hpp>
#include <grund/iouring/timer_fd.hpp>
#include <grund/system.hpp>

using namespace grund::iouring;

void block_signal(int signal)
{
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, signal);
  sigprocmask(SIG_BLOCK, &mask, nullptr);
}

class ExitSignal : public SignalFd {
protected:
  bool exit;

public:
  ExitSignal(Executor& executor, int signal_identifier)
    : SignalFd(executor, signal_identifier)
    , exit(false)
  {
  }

  void signal(int signal_identifier) override
  {
    printf("Signal %d\r\n", signal_identifier);
    if (signal_identifier == SIGINT) {
      exit = true;
    }
  }

  bool do_run()
  {
    return !exit;
  }
};

class MyTimer : public TimerFd {
public:
  void timer(uint64_t expirations) override
  {
    auto timestamp = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch());
    printf("Timer %" PRIu64 " %" PRIu64 "\r\n", timestamp.count(), expirations);
  }
};

int main(int argc, char* argv[])
{
  using namespace std::literals::chrono_literals;
  (void)argc;
  (void)argv;
  auto [major, minor, fix] = grund::System::get_kernel_version();
  fprintf(stdout, "Kernel %u.%u.%u\r\n", major, minor, fix);
  if (major >= 5 && minor >= 11) {
    block_signal(SIGINT);
  }
  Executor executor;
  ExitSignal exit_signal(executor, SIGINT);
  MyTimer timer;
  timer.arm(executor, 250ms, true);
  executor.submit();
  int result = 0;
  while (result >= 0) {
    result = executor.run_one();
    if (!exit_signal.do_run()) {
      break;
    }
  }
  return 0;
}
