#include <array>
#include <getopt.h>
#include <grund/convert.hpp>
#include <grund/dev_mem.hpp>
#include <grund/system_error.hpp>
#include <span>

int main(int argc, char* argv[])
{
  static struct option long_options[] = {
    { "address", required_argument, 0, 'a' },
    { "offset", required_argument, 0, 'o' },
    { "size", required_argument, 0, 's' },
    { 0, 0, 0, 0 }
  };
  int option_index = 0;
  std::size_t address = 0;
  std::size_t offset = 0;
  std::size_t size = 4096;

  while (1) {
    int c = getopt_long(argc, argv, "a:o:s:",
      long_options, &option_index);
    if (c == -1) {
      break;
    }
    switch (c) {
    case 'a':
      address = grund::from_string<std::size_t>(optarg);
      break;
    case 'o':
      offset = grund::from_string<std::size_t>(optarg);
      break;
    case 's':
      size = grund::from_string<std::size_t>(optarg);
      break;
    default:
      break;
    }
  }

  if (address == 0) {
    fprintf(stderr, "Address required\n");
    return EXIT_FAILURE;
  }

  try {
    grund::MemoryMap devmem = grund::DevMem::to_memory_map(address, size);
    auto value = devmem.get<uint32_t>(offset);
    fprintf(stdout, "%08x\n", value);
  }
  catch (grund::SystemError& e) {
    fprintf(stderr, "Mapping failed %s\n", e.what());
  }

  return EXIT_SUCCESS;
}
