#include <getopt.h>
#include <grund/convert.hpp>
#include <grund/ip_address.hpp>
#include <grund/socket_address.hpp>
#include <grund/udp_multicast.hpp>

int main(int argc, char* argv[])
{
  static struct option long_options[] = {
    { "address", required_argument, 0, 'a' },
    { "port", required_argument, 0, 'p' },
    { 0, 0, 0, 0 }
  };
  int option_index = 0;
  char remote_address[260] = "";
  char remote_port[260] = "";

  while (1) {
    int c = getopt_long(argc, argv, "a:p:",
      long_options, &option_index);
    if (c == -1) {
      break;
    }
    switch (c) {
    case 'a':
      strcpy(remote_address, optarg);
      break;
    case 'p':
      strcpy(remote_port, optarg);
      break;
    default:
      break;
    }
  }

  grund::IpAddress multicast_address(224, 0, 0, 101);
  if (strlen(remote_address) > 0) {
    multicast_address = grund::IpAddress(remote_address);
  }

  fprintf(stdout, "Create socket\r\n");

  auto socket = grund::UdpMulticast(multicast_address.family(), false);
  socket.set_option(SOL_SOCKET, SO_REUSEADDR, true);
  socket.set_option(SOL_SOCKET, SO_REUSEPORT, true);
  fprintf(stdout, "Bind socket\r\n");

  // socket.bind(grund::IpAddress::ipv4_any(), multicast_port);

  fprintf(stdout, "Join socket\r\n");
  socket.join(multicast_address);

  fprintf(stdout, "Receive socket\r\n");
  std::uint8_t buffer[1024] = { 0 };
  auto [bytes, address] = socket.receive_from(buffer, 1024);

  fprintf(stdout, "Received %zu\r\n", bytes);

  return 0;
}
