#include <csignal>
#include <cstdio>
#include <cstring>
#include <grund/epoll/eventloop.hpp>
#include <grund/epoll/tcp_server.hpp>
#include <grund/epoll/tcp_session.hpp>
#include <unordered_map>

class EventServer : public grund::epoll::TcpServer {
public:
  static const uint16_t PORT = 37357;

public:
  EventServer(grund::epoll::EventLoop& event_loop)
    : grund::epoll::TcpServer(event_loop)
  {
    listen(grund::IpAddress::ipv6_loopback(), PORT, [this](grund::Socket socket, grund::SocketAddress& address) {
      session_accept(std::move(socket), address);
    });
  }

protected:
  std::unordered_map<int, std::shared_ptr<grund::epoll::TcpSession>> _sessions;

  void session_accept(grund::Socket socket, grund::SocketAddress& address)
  {
    fprintf(stdout, "Accept %d %s, %d\n", _socket.fd(), address.as_string().c_str(), socket.fd());
    int identifier = socket.fd();

    const std::string_view hello { "Hello" };
    ssize_t length = ::write(identifier, hello.data(), hello.size());
    (void)length;
    auto session = std::make_shared<grund::epoll::TcpSession>(_event_loop, std::move(socket), [this](grund::Socket& socket) {
      session_read(socket);
    });
    _sessions.insert(std::make_pair(identifier, session));
  }

  void session_read(grund::Socket& socket)
  {
    uint8_t buffer[8];
    int r = ::read(socket.fd(), buffer, 8);
    if (r < 0) {
      fprintf(stdout, "Read %d %s\n", socket.fd(), strerror(errno));
    }
    else if (r == 0) {
      // Disconnected
      fprintf(stdout, "Disconnect %d\n", socket.fd());
      auto session = _sessions.extract(socket.fd());
    }
    else {
      fprintf(stdout, "Read %d %d\n", socket.fd(), r);
    }
  }
};

static volatile sig_atomic_t running = 1;

static void signal_handler(int signal, siginfo_t* info, void* data)
{
  (void)signal;
  (void)info;
  (void)data;
  running = 0;
}

int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;
  struct sigaction sig;
  std::memset(&sig, 0, sizeof(sig));
  sig.sa_sigaction = signal_handler;
  sigfillset(&sig.sa_mask);
  sig.sa_flags = SA_SIGINFO;
  sigaction(SIGINT, &sig, nullptr);

  grund::epoll::EventLoop event_loop;

  EventServer server(event_loop);

  do {
    event_loop.run_once(500);
  } while (running);
}