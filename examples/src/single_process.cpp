#include <csignal>
#include <filesystem>
#include <fmt/core.h>
#include <grund/convert.hpp>
#include <grund/process_identifier_file.hpp>
#include <grund/process_information.hpp>
#include <grund/system_error.hpp>

static volatile sig_atomic_t running = 1;

static void signal_handler(int signal, siginfo_t* info, void* data)
{
  (void)signal;
  (void)info;
  (void)data;
  running = 0;
}

int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  struct sigaction sig;
  std::memset(&sig, 0, sizeof(sig));
  sig.sa_sigaction = signal_handler;
  sigfillset(&sig.sa_mask);
  sig.sa_flags = SA_SIGINFO;
  sigaction(SIGINT, &sig, nullptr);

  auto pid_file_path = std::filesystem::path("/tmp/single_process.pid");
  grund::ProcessIdentifierFile pid_file(pid_file_path);

  if (pid_file.occupied()) {
    fmt::print("Process is already running with PID {}\n", pid_file.pid());
    return EXIT_SUCCESS;
  }

  grund::ProcessInformation info;

  fmt::print("{} {}\n", info.name(), info.pid());

  pid_file.write();

  do {
  } while (running);

  return EXIT_SUCCESS;
}
