#include <array>
#include <getopt.h>
#include <grund/convert.hpp>
#include <grund/spidev.hpp>
#include <span>

int main(int argc, char* argv[])
{
  static struct option long_options[] = {
    { "bus", required_argument, 0, 'b' },
    { "device", required_argument, 0, 'd' },
    { 0, 0, 0, 0 }
  };
  int option_index = 0;
  uint16_t spi_bus = 0;
  uint16_t spi_device = 0;

  while (1) {
    int c = getopt_long(argc, argv, "b:d:",
      long_options, &option_index);
    if (c == -1) {
      break;
    }
    switch (c) {
    case 'b':
      spi_bus = grund::from_string<uint16_t>(optarg);
      break;
    case 'd':
      spi_device = grund::from_string<uint16_t>(optarg);
      break;
    default:
      break;
    }
  }

  grund::SpiDev spi(spi_bus, spi_device);
  spi.set_mode(grund::SpiDev::MODE_0);
  spi.set_bits_per_word(8);
  spi.set_frequency(5'000'000);

  uint32_t spi_frequency = spi.get_frequency();
  uint8_t spi_bits_per_word = spi.get_bits_per_word();
  grund::SpiDev::MODE spi_mode = spi.get_mode();

  fprintf(stdout, "SPI %u.%u %u Hz %u-bit %u\r\n", spi_bus, spi_device, spi_frequency, spi_bits_per_word, static_cast<uint32_t>(spi_mode));

  std::array<uint8_t, 1> reg { 0xaa };

  static constexpr uint8_t SPI_READ = 0x80;
  static constexpr uint8_t ADDR_IDENTITY = 0x50;
  static constexpr uint8_t ADDR_MEASUREMENT_CONTROL = 0x74;
  static constexpr uint8_t ADDR_STATUS = 0x73;
  static constexpr uint8_t ADDR_PRESSURE_MSB = 0x77;

  spi.send_receive({ SPI_READ | ADDR_IDENTITY }, reg);

  fprintf(stdout, "Received %02x\r\n", reg[0]);

  spi.send({ ADDR_MEASUREMENT_CONTROL, 0x01 });

  do {
    spi.send_receive({ SPI_READ | ADDR_STATUS }, reg);
  } while ((reg[0] & 0x08) == 0x08);

  std::array<uint8_t, 8> measurement { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  spi.send_receive({ SPI_READ | ADDR_PRESSURE_MSB }, measurement);

  for (const uint8_t b : measurement) {
    fprintf(stdout, " %02x", b);
  }
  fprintf(stdout, "\r\n");

  int32_t temperature = static_cast<int32_t>(measurement[3]) << 12 | static_cast<int32_t>(measurement[4]) << 4 | static_cast<int32_t>(measurement[5]) >> 4;

  fprintf(stdout, "Temperature %d\r\n", temperature);

  return 0;
}
