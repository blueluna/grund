#include <atomic>
#include <csignal>
#include <cstdlib>
#include <cstring>
#include <grund/epoll/eventloop.hpp>
#include <grund/file_descriptor.hpp>
#include <grund/system_resource_lock.hpp>
extern "C" {
#include <getopt.h>
#include <termios.h>
#include <unistd.h>
}

static volatile sig_atomic_t running = 1;

static void signal_handler(int signal, siginfo_t* info, void* data)
{
  (void)signal;
  (void)info;
  (void)data;
  running = 0;
}

int main(int argc, char* argv[])
{
  using namespace std::literals::chrono_literals;

  static struct option long_options[] = {
    { "name", required_argument, 0, 'n' },
    { 0, 0, 0, 0 }
  };
  int option_index = 0;
  std::string opt_name;

  while (1) {
    int c = getopt_long(argc, argv, "n:",
      long_options, &option_index);
    if (c == -1) {
      break;
    }
    switch (c) {
    case 'n':
      opt_name.assign(optarg);
      break;
    default:
      break;
    }
  }

  if (opt_name.empty()) {
    fprintf(stderr, "Name required\n");
    return EXIT_FAILURE;
  }

  grund::SystemResourceLock lock(opt_name);

  struct termios terminal_settings { };
  tcgetattr(STDIN_FILENO, &terminal_settings);
  struct termios original_terminal_settings {
    terminal_settings
  };
  terminal_settings.c_lflag &= (~ICANON);
  terminal_settings.c_lflag &= (~ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &terminal_settings);

  struct sigaction sig { };
  std::memset(&sig, 0, sizeof(sig));
  sig.sa_sigaction = signal_handler;
  sigfillset(&sig.sa_mask);
  sig.sa_flags = SA_SIGINFO;
  sigaction(SIGINT, &sig, nullptr);

  grund::FileDescriptor in(STDIN_FILENO);

  grund::epoll::EventLoop event_loop;

  grund::epoll::Event input_event(event_loop, in, grund::epoll::Event::READ, [&lock](int fd, uint32_t events) {
    if (events != grund::epoll::Event::READ) {
      return;
    }
    char c;
    read(fd, &c, 1);
    std::string topic;
    switch (c) {
    case 'r':
      lock.release();
      fprintf(stdout, "released\n");
      break;
    case 'a':
      if (!lock.acquire(10s)) {
        fprintf(stdout, "timeout\n");
      }
      else {
        fprintf(stdout, "locked\n");
      }
      break;
    default:
      break;
    }
  });

  while (running) {
    event_loop.run_once();
  }

  tcsetattr(STDIN_FILENO, TCSANOW, &original_terminal_settings);

  return EXIT_SUCCESS;
}
