#include <csignal>
#include <cstdio>
#include <cstring>
#include <grund/epoll/eventloop.hpp>
#include <grund/epoll/tcp_client.hpp>
#include <grund/epoll/tcp_server.hpp>

static volatile sig_atomic_t running = 1;

static void signal_handler(int signal, siginfo_t* info, void* data)
{
  (void)signal;
  (void)info;
  (void)data;
  running = 0;
}

void on_connect(grund::epoll::TcpClient& client)
{
  (void)client;
  fprintf(stdout, "Connect\n");
}

void on_read(grund::epoll::TcpClient& client)
{
  uint8_t buffer[8];
  int r = ::read(client.file_descriptor(), buffer, 8);
  if (r < 0) {
    fprintf(stdout, "Read %d %s\n", client.file_descriptor(), strerror(errno));
  }
  else if (r == 0) {
    running = 0;
  }
  else {
    fprintf(stdout, "Read %d \"%.*s\"\n", client.file_descriptor(), r, buffer);
  }
}

int main(int argc, char* argv[])
{
  static const uint16_t PORT = 37357;
  (void)argc;
  (void)argv;
  struct sigaction sig;
  std::memset(&sig, 0, sizeof(sig));
  sig.sa_sigaction = signal_handler;
  sigfillset(&sig.sa_mask);
  sig.sa_flags = SA_SIGINFO;
  sigaction(SIGINT, &sig, nullptr);

  grund::epoll::EventLoop event_loop;

  grund::epoll::TcpClient client(event_loop);
  client.connect(grund::SocketAddress(grund::IpAddress::ipv6_loopback(), PORT), on_connect, on_read);

  do {
    event_loop.run_once(500);
  } while (running);

  client.close();
}