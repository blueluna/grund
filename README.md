# Grund

C++ library for embedded Linux experiments.

Experiments with `liburing`, `clang`, `llvm`, `cmake`, `ninja`, ...

## Libraries

### Grund

Classes components like file, time, networking, ...

### Grund-epoll

Epoll-based classes.

### Grund-iouring

io-uring-based classes.

See [grund-iouring](grund-iouring/README.md) for more about IO-uring.

### Plogik

Classes for Xilinx ZynqMP PS.

## Build environment

The CMake toolchain files look for headers and libraries located at `${GRUND_ENV}/<target triple>/`. Headers in `${GRUND_ENV}/<target triple>/include`, libraries in `${GRUND_ENV}/<target triple>/lib`, pkgconfig in `${GRUND_ENV}/<target triple>/lib/pkgconfig`, ...

Export a environment variable pointing to the build environment,
```shell
$ export GRUND_ENV=${HOME}/grund_env
```

The target triple is formatted as <CPU architecture>-linux-gnu. For example x86_64-linux-gnu.

Architectures that has been tested are,
 * x86_64
 * aarch64
 * armhf

## Prerequisites

This library requires header only fmt. To build grund-iouring, liburing is required. 

### System packages

Install git and git lfs

```shell
$ sudo apt install git git-lfs
```

Install clang, lld and llvm

```shell
$ sudo apt install llvm lld clang
```

Install aarch64 toolchain

```shell
$ sudo apt install gcc-aarch64-linux-gnu g++-aarch64-linux-gnu
```

Install armhf toolchain

```shell
$ sudo apt install gcc-arm-linux-gnueabihf g++-arm-linux-gnueabihf
```

Install ninja build,
```shell
$ sudo apt install ninja-build
```

Configure cmake to use Ninja as default build generator. Add following to
your `${HOME}/.profile`.
```shell
# Configure default cmake generator
export CMAKE_GENERATOR=Ninja
```

Source your `.profile`
```shell
$ . ${HOME}/.profile
```

Alternately select build system generator on the command line,

```shell
$ cmake -G Ninja ..
```

#### PopOs not finding stdc++

Clang is using the non-standard gcc installation? Check with,

```
clang++ --verbose
```

Install libstdc++ for the selected gcc toolchain.

```
$ apt-get install libstdc++-12-dev
```

## Building prerequisite libraries

Build and install the required libraries into the `${GRUND_ENV}` environment.

### fmt

Download fmt-8 or later from https://fmt.dev/, unpack.

Build using cmake,
```shell
$ cmake -B build -S . -GNinja
$ cmake --build build
```

Install into the environment,
```shell
$ cmake --install build --prefix=${GRUND_ENV}/x86_64-linux-gnu/
```

To cross-compile use the toolchain file in the grund project,

```shell
$ cmake -B build-aarch64 -S . -GNinja -DCMAKE_TOOLCHAIN_FILE=<path to grund>/toolchain-aarch64-clang.cmake
$ cmake --build build-aarch64
```

Install into the environment,
```shell
$ cmake --install build-aarch64 --prefix=${GRUND_ENV}/aarch64-linux-gnu/
```

### liburing

Download liburing-2.0 or later from https://github.com/axboe/liburing/tags, unpack.

Build and install,
```shell
$ ./configure --prefix=${GRUND_ENV}/x86_64-linux-gnu
$ make
$ make install
```

To cross-compile supply toolchain overrides to configure,

```shell
$ CC=aarch64-linux-gnu-gcc CXX=aarch64-linux-gnu-g++ ./configure --prefix=${GRUND_ENV}/aarch64-linux-gnu
$ make
$ make install
```

## Build

```shell
$ cmake  -B build -S .
$ cmake --build build
```

To clean. Issue,
```shell
$ cmake --build build --target clean
```

### Build for aarch64

```shell
$ cmake -S . -B build-aarch64 -DCMAKE_TOOLCHAIN_FILE=toolchain-aarch64-clang.cmake
$ cmake --build build-aarch64
```

### Build for armhf

```shell
$ cmake -S . -B build-armhf -DCMAKE_TOOLCHAIN_FILE=toolchain-armhf-clang.cmake
$ cmake --build build-armhf
```

### Check with clang-format

```shell
$ ./scripts/format-check
```

### Format with clang-format

```shell
$ ./scripts/format
```

### Check with clang-tidy

Ensure that a compilation database has been created in the build directory.

```shell
$ run-clang-tidy -p build
```
