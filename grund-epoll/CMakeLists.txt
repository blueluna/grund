set(lib_name ${PROJECT_NAME}-epoll)
set(inc_dir include/${PROJECT_NAME}/epoll)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)

set(sources
    src/event.cpp
    src/eventloop.cpp
    src/tcp_server.cpp
    src/event_fd.cpp
    src/event_timer.cpp
    src/tcp_client.cpp
    src/tcp_session.cpp
    src/udp_multicast.cpp
    ${inc_dir}/event.hpp
    ${inc_dir}/eventloop.hpp
    ${inc_dir}/tcp_server.hpp
    ${inc_dir}/event_fd.hpp
    ${inc_dir}/event_timer.hpp
    ${inc_dir}/tcp_client.hpp
    ${inc_dir}/tcp_session.hpp
    ${inc_dir}/udp_multicast.hpp
    )

add_library(${lib_name} SHARED ${sources})
set_property(TARGET ${lib_name} PROPERTY CXX_STANDARD 20)
set_property(TARGET ${lib_name} PROPERTY VERSION ${PROJECT_VERSION})
set_property(TARGET ${lib_name} PROPERTY SOVERSION ${PROJECT_VERSION_MAJOR})
target_include_directories(${lib_name} PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:include>)
target_link_libraries(${lib_name} PUBLIC grund)

set(lib_name_static ${lib_name}-static)
add_library(${lib_name_static} STATIC ${sources})
set_property(TARGET ${lib_name_static} PROPERTY CXX_STANDARD 20)
target_include_directories(${lib_name_static} PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:include>)
target_link_libraries(${lib_name_static} PUBLIC grund-static)

install(TARGETS ${lib_name}
        EXPORT ${lib_name}_targets
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib
        )

install(TARGETS ${lib_name_static}
        EXPORT ${lib_name}_targets
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib
        )

install(DIRECTORY include/ DESTINATION include)

set(package_config_location ${CMAKE_INSTALL_LIBDIR}/${lib_name}/cmake)
set(package_targets_name ${lib_name}-targets.cmake)
set(package_targets ${CMAKE_CURRENT_BINARY_DIR}/${package_targets_name})
set(package_config ${CMAKE_CURRENT_BINARY_DIR}/${lib_name}-config.cmake)
set(package_version ${CMAKE_CURRENT_BINARY_DIR}/${lib_name}-config-version.cmake)

export(EXPORT ${lib_name}_targets
        FILE ${package_targets}
        )

install(EXPORT ${lib_name}_targets
        FILE ${package_targets_name}
        DESTINATION ${package_config_location}
        )

configure_package_config_file(
        ${lib_name}-config.cmake.in
        ${package_config}
        INSTALL_DESTINATION ${package_config_location}
        PATH_VARS lib_name
)

write_basic_package_version_file(${package_version} COMPATIBILITY AnyNewerVersion)

install(FILES
        ${package_config}
        ${package_version}
        DESTINATION ${package_config_location}
        )