// TCP server session

#include <grund/epoll/eventloop.hpp>
#include <grund/socket.hpp>
#include <grund/socket_address.hpp>

#ifndef GRUND_EPOLL_TCP_SESSION_HPP
#define GRUND_EPOLL_TCP_SESSION_HPP

namespace grund::epoll {

class TcpSession : public Event {
public:
  typedef std::function<void(Socket& socket)> ReadCallback;

  explicit TcpSession(EventLoop& event_loop, grund::Socket socket, ReadCallback read_callback);

  TcpSession(TcpSession&& other) = default;

  ~TcpSession();

  TcpSession(TcpSession& other) = delete;

protected:
  Socket _socket;
  ReadCallback _read_callback;

  void callback(int fd, uint32_t events);
};

} // namespace grund

#endif // GRUND_EPOLL_TCP_SESSION_HPP
