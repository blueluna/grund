#include <cstdint>
#include <functional>
#include <grund/file_descriptor.hpp>
extern "C" {
#include <sys/epoll.h>
}

#ifndef GRUND_EPOLL_EVENT_HPP
#define GRUND_EPOLL_EVENT_HPP

namespace grund::epoll {

class EventLoop;

typedef std::function<void(int fd, uint32_t events)> EventCallback;

class Event {
public:
  static const uint32_t ERROR = EPOLLERR;
  static const uint32_t HANGUP = EPOLLHUP;
  static const uint32_t ONESHOT = EPOLLONESHOT;
  static const uint32_t READ = EPOLLIN;
  static const uint32_t WRITE = EPOLLOUT;

protected:
  EventLoop& _event_loop;

public:
  explicit Event(EventLoop& event_loop);
  explicit Event(EventLoop& event_loop, int fd, uint32_t events, EventCallback callback);
  explicit Event(EventLoop& event_loop, FileDescriptor& fd, uint32_t events, EventCallback callback);
  virtual ~Event() = default;

  void register_event(int fd, uint32_t events, EventCallback callback);
  void unregister_event(int fd);
};

} // namespace grund

#endif // GRUND_EPOLL_EVENT_HPP
