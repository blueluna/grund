// TCP client

#include <grund/epoll/eventloop.hpp>
#include <grund/socket.hpp>
#include <grund/socket_address.hpp>

#ifndef GRUND_EPOLL_TCP_CLIENT_HPP
#define GRUND_EPOLL_TCP_CLIENT_HPP

namespace grund::epoll {

class TcpClient : public Event {
protected:
  enum class State {
    Disconnected,
    Connecting,
    Connected,
  };
  State _state;

public:
  typedef std::function<void(TcpClient& client)> ConnectCallback;
  typedef std::function<void(TcpClient& client)> ReadCallback;

  explicit TcpClient(EventLoop& event_loop);
  ~TcpClient();

  void connect(const SocketAddress& address, ConnectCallback connect_callback, ReadCallback read_callback);
  void close();

  int file_descriptor()
  {
    return _socket.fd();
  }

protected:
  grund::Socket _socket;
};

} // namespace grund

#endif // GRUND_EPOLL_TCP_CLIENT_HPP
