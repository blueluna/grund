// Timer events using timerfd

#include <grund/epoll/event.hpp>
#include <grund/epoll/eventloop.hpp>
#include <grund/timer_fd.hpp>

#ifndef GRUND_EPOLL_EVENT_TIMER_HPP
#define GRUND_EPOLL_EVENT_TIMER_HPP

namespace grund::epoll {

class EventTimer : public Event, public TimerFd {
public:
  typedef std::function<void()> Callback;

  explicit EventTimer(EventLoop& event_loop, const EventTimer::Callback& callback);
  ~EventTimer();
};

} // namespace grund

#endif // GRUND_EPOLL_EVENT_TIMER_HPP
