// TCP server

#include <grund/epoll/event_fd.hpp>
#include <grund/socket.hpp>
#include <grund/socket_address.hpp>

#ifndef GRUND_EPOLL_TCP_SERVER_HPP
#define GRUND_EPOLL_TCP_SERVER_HPP

namespace grund::epoll {

class TcpServer : public Event {
protected:
  const int _LISTEN_BACKLOG = 50;

public:
  typedef std::function<void(Socket socket, SocketAddress& address)> AcceptCallback;

  explicit TcpServer(EventLoop& event_loop);
  ~TcpServer();

  void listen(const IpAddress& address, uint16_t port, AcceptCallback callback);
  void listen(uint16_t port, AcceptCallback callback);

protected:
  grund::Socket _socket;
};

} // namespace grund

#endif // GRUND_EPOLL_TCP_SERVER_HPP
