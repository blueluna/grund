
#include <grund/epoll/eventloop.hpp>
#include <grund/socket_address.hpp>
#include <grund/udp_multicast.hpp>

#ifndef GRUND_EPOLL_UDP_MULTICAST_HPP
#define GRUND_EPOLL_UDP_MULTICAST_HPP

namespace grund::epoll {

class UdpMulticast : public Event, public grund::UdpMulticast {
public:
  typedef std::function<void(grund::UdpMulticast& fd, uint32_t events)> Callback;

  explicit UdpMulticast(EventLoop& event_loop, sa_family_t family, UdpMulticast::Callback);
  ~UdpMulticast();
};

}

#endif // GRUND_EPOLL_UDP_MULTICAST_HPP
