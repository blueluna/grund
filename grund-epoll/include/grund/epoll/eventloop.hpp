// Bare bones event loop handler using epoll

#ifndef GRUND_EPOLL_EVENTLOOP_HPP
#define GRUND_EPOLL_EVENTLOOP_HPP

extern "C" {
#include <sys/epoll.h>
#include <unistd.h>
}
#include <memory>
#include <unordered_map>

#include <grund/epoll/event.hpp>

namespace grund::epoll {

/// EventLoop for epoll event handling
class EventLoop {
protected:
  /// epoll handle
  int _epoll_fd;
  /// epoll event buffer
  std::unique_ptr<struct epoll_event[]> _poll_buffer;
  /// registered event callbacks
  std::unordered_map<int, EventCallback> _entries;

  bool _running;

public:
  explicit EventLoop();
  ~EventLoop();

  void add(int fd, uint32_t events, EventCallback callback);
  void remove(int fd);
  bool modify(int fd, uint32_t events) const;

  void add(const FileDescriptor fd, uint32_t events, EventCallback callback)
  {
    add(fd.fd(), events, callback);
  }

  void remove(const FileDescriptor& fd)
  {
    remove(fd.fd());
  }

  bool modify(const FileDescriptor& fd, uint32_t events) const
  {
    return modify(fd.fd(), events);
  }

  bool run_once(int poll_timeout = -1);
  void run(int poll_timeout = 1000);

  int stop();
};

} // namespace grund

#endif // GRUND_EPOLL_EVENTLOOP_HPP
