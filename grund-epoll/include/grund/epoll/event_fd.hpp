// Base class for file descriptor events

#include <cstdint>

#include <grund/epoll/event.hpp>
#include <grund/event_fd.hpp>

#ifndef GRUND_EPOLL_EVENTFD_HPP
#define GRUND_EPOLL_EVENTFD_HPP

namespace grund::epoll {

class EventFd : public Event, public grund::EventFdSemaphore {
public:
  typedef std::function<void(uint64_t)> Callback;

  explicit EventFd(EventLoop& event_loop, const EventFd::Callback& callback);
  ~EventFd();
};

} // namespace grund

#endif // GRUND_EPOLL_EVENTFD_HPP
