#include <grund/epoll/event.hpp>
#include <grund/epoll/eventloop.hpp>
#include <grund/system_error.hpp>
#include <utility>

using namespace grund::epoll;

Event::Event(EventLoop& event_loop)
  : _event_loop(event_loop)
{
}

Event::Event(EventLoop& event_loop, int fd, const uint32_t events, EventCallback callback)
  : _event_loop(event_loop)
{
  register_event(fd, events, std::move(callback));
}

Event::Event(EventLoop& event_loop, FileDescriptor& fd, const uint32_t events, EventCallback callback)
  : Event(event_loop, fd.fd(), events, std::move(callback))
{
}

void Event::register_event(int fd, const uint32_t events, EventCallback callback)
{
  if (FileDescriptor::is_valid(fd)) {
    _event_loop.add(fd, events, std::move(callback));
  }
  else {
    throw SystemError(EBADFD);
  }
}

void Event::unregister_event(int fd)
{
  if (FileDescriptor::is_valid(fd)) {
    _event_loop.remove(fd);
  }
}
