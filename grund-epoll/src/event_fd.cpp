#include <grund/epoll/event_fd.hpp>
#include <grund/epoll/eventloop.hpp>

namespace grund::epoll {

EventFd::EventFd(EventLoop& event_loop, const EventFd::Callback& callback)
  : Event(event_loop)
{
  register_event(fd(), Event::READ,
    [this, callback](int fd, uint32_t events) {
      (void)events;
      if (fd == this->fd()) {
        uint64_t counter = read_event();
        callback(counter);
      }
    });
}

EventFd::~EventFd()
{
  unregister_event(_fd);
}

}