// TCP client

#include <grund/epoll/tcp_client.hpp>

namespace grund::epoll {

TcpClient::TcpClient(EventLoop& eventloop)
  : Event(eventloop)
  , _state(State::Disconnected)
{
}
TcpClient::~TcpClient()
{
  unregister_event(_socket.fd());
}

void TcpClient::connect(const SocketAddress& address, ConnectCallback connect_callback, ReadCallback read_callback)
{
  _state = State::Disconnected;
  _socket = Socket::TcpSocket(address.family(), true);
  bool in_progress = _socket.connect(address);
  auto func = [this, connect_callback, read_callback](int fd, uint32_t events) {
    (void)fd;
    if ((events & Event::WRITE) == Event::WRITE && _state == State::Connecting) {
      _state = State::Connected;
      connect_callback(*this);
      _event_loop.modify(_socket, Event::READ | Event::HANGUP | Event::ERROR);
    }
    if ((events & Event::READ) == Event::READ) {
      if (_state == State::Connected) {
        read_callback(*this);
      }
    }
  };
  if (in_progress) {
    _state = State::Connecting;
    register_event(_socket.fd(), Event::WRITE | Event::HANGUP | Event::ERROR, func);
  }
  else {
    _state = State::Connected;
    connect_callback(*this);
    register_event(_socket.fd(), Event::READ | Event::HANGUP | Event::ERROR, func);
  }
}

void TcpClient::close()
{
  if (_socket.is_open()) {
    _event_loop.remove(_socket);
    _socket.close();
  }
}

} // namespace grund
