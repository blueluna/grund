extern "C" {
#include <unistd.h>
}
#include <cstring>

#include <grund/epoll/eventloop.hpp>
#include <grund/epoll/tcp_server.hpp>

namespace grund::epoll {

TcpServer::TcpServer(EventLoop& event_loop)
  : Event(event_loop)
{
}

TcpServer::~TcpServer()
{
  unregister_event(_socket.fd());
}

void TcpServer::listen(const IpAddress& address, const uint16_t port, AcceptCallback callback)
{
  _socket = Socket::TcpSocket(address.family(), true);
  _socket.set_option(SOL_SOCKET, SO_REUSEADDR, true);
  _socket.bind(address, port);
  _socket.listen(_LISTEN_BACKLOG);
  register_event(_socket.fd(), Event::READ, [this, callback](int fd, uint32_t events) {
    (void)fd;
    (void)events;
    Socket socket;
    SocketAddress address;
    std::tie(socket, address) = _socket.accept_with_address();
    callback(std::move(socket), address);
  });
}

void TcpServer::listen(const uint16_t port, AcceptCallback callback)
{
  IpAddress address = IpAddress::ipv4_any();
  listen(address, port, callback);
}

} // namespace grund
