#include <grund/epoll/eventloop.hpp>
#include <grund/system_error.hpp>

namespace grund::epoll {

const size_t POLL_BUFFER_COUNT = 16;

inline int create_epoll_handle()
{
  int fd = epoll_create1(EPOLL_CLOEXEC);
  if (fd == FileDescriptor::INVALID) {
    throw SystemError();
  }
  return fd;
}

EventLoop::EventLoop()
  : _epoll_fd(create_epoll_handle())
  , _poll_buffer(std::make_unique<struct epoll_event[]>(POLL_BUFFER_COUNT))
  , _running(false)
{
}

EventLoop::~EventLoop()
{
  if (_epoll_fd == FileDescriptor::INVALID) {
    return;
  }
  _entries.clear();
  close(_epoll_fd);
}

void EventLoop::add(int fd, const uint32_t events, EventCallback callback)
{
  // Check if the entry already has been added
  auto existing_entry = _entries.find(fd);
  if (existing_entry != _entries.end()) {
    return;
  }
  struct epoll_event ev_data = {};
  ev_data.events = events;
  ev_data.data.fd = fd;
  if (epoll_ctl(_epoll_fd, EPOLL_CTL_ADD, fd, &ev_data) == -1) {
    throw SystemError();
  }
  _entries[fd] = callback;
}

void EventLoop::remove(int fd)
{
  if (epoll_ctl(_epoll_fd, EPOLL_CTL_DEL, fd, nullptr) == -1) {
    throw SystemError();
  }
  _entries.erase(fd);
}

bool EventLoop::modify(int fd, const uint32_t events) const
{
  struct epoll_event ev_data = {};
  ev_data.events = events;
  ev_data.data.fd = fd;
  if (epoll_ctl(_epoll_fd, EPOLL_CTL_MOD, fd, &ev_data) == -1) {
    int error = errno;
    if (error == ENOENT) {
      return false;
    }
    throw SystemError(error);
  }
  return true;
}

bool EventLoop::run_once(const int poll_timeout)
{
  int count = epoll_wait(_epoll_fd, _poll_buffer.get(), POLL_BUFFER_COUNT, poll_timeout);
  if (count == -1) {
    int error = errno;
    if (error != EINTR) {
      throw SystemError(error);
    }
    return false;
  }
  for (int n = 0; n < count; n++) {
    int fd = _poll_buffer[n].data.fd;
    auto entry = _entries.find(fd);
    if (entry == _entries.end()) {
      continue;
    }
    entry->second(fd, _poll_buffer[n].events);
  }
  return true;
}

void EventLoop::run(const int poll_timeout)
{
  _running = true;
  while (_running) {
    if (!run_once(poll_timeout)) {
      break;
    }
  }
}

int EventLoop::stop()
{
  _running = false;
  return 0;
}

} // namespace grund
