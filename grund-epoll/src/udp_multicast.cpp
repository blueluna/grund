#include <cstring>

#include "grund/epoll/udp_multicast.hpp"
#include "grund/system_error.hpp"

namespace grund::epoll {

UdpMulticast::UdpMulticast(EventLoop& event_loop, sa_family_t family, UdpMulticast::Callback callback)
  : Event(event_loop)
  , grund::UdpMulticast(family, true)
{
  register_event(fd(), Event::READ | Event::WRITE,
    [this, &callback](int fd, uint32_t events) {
      if (fd == this->fd()) {
        callback(*this, events);
      }
    });
}

UdpMulticast::~UdpMulticast()
{
  unregister_event(_fd);
}

}