#include <grund/epoll/event_timer.hpp>
#include <grund/epoll/eventloop.hpp>

namespace grund::epoll {

EventTimer::EventTimer(EventLoop& event_loop, const EventTimer::Callback& callback)
  : Event(event_loop)
  , grund::TimerFd(true)
{
  register_event(fd(), Event::READ,
    [this, callback](int fd, uint32_t events) {
      (void)events;
      if (fd == this->fd()) {
        callback();
        clear();
      }
    });
}

EventTimer::~EventTimer()
{
  unregister_event(_fd);
}

} // namespace grund
