#include <cstdio>
#include <cstring>
#include <grund/epoll/tcp_session.hpp>

namespace grund::epoll {

TcpSession::TcpSession(EventLoop& event_loop, grund::Socket socket, ReadCallback read_callback)
  : Event(event_loop, socket.fd(), Event::READ, [this](int fd, uint32_t events) { callback(fd, events); })
  , _socket(std::move(socket))
  , _read_callback(read_callback)
{
}

TcpSession::~TcpSession()
{
  unregister_event(_socket.fd());
}

void TcpSession::callback(int fd, uint32_t events)
{
  (void)fd;
  if ((events & Event::READ) == Event::READ) {
    if (_read_callback) {
      _read_callback(_socket);
    }
  }
  else if ((events & Event::ERROR) == Event::ERROR) {
    fprintf(stdout, "TcpSession error %d \n", _socket.fd());
  }
  else if ((events & Event::HANGUP) == Event::HANGUP) {
    fprintf(stdout, "TcpSession hangup %d \n", _socket.fd());
  }
}

} // namespace grund
